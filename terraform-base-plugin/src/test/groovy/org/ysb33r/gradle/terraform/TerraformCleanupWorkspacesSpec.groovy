/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.terraform

import org.gradle.testkit.runner.BuildResult
import org.gradle.testkit.runner.GradleRunner
import org.ysb33r.gradle.terraform.internal.TerraformModel
import org.ysb33r.gradle.terraform.testfixtures.IntegrationSpecification

import static org.gradle.testkit.runner.TaskOutcome.FAILED
import static org.gradle.testkit.runner.TaskOutcome.SUCCESS
import static org.gradle.testkit.runner.TaskOutcome.UP_TO_DATE

class TerraformCleanupWorkspacesSpec extends IntegrationSpecification {

    public static final List<String> TEST_WORKSPACES = ['alpha', 'beta']
    public static final String WITH_WORKSPACES = """workspaces '${TEST_WORKSPACES.join("','")}'"""
    public static final String WITHOUT_WORKSPACES = ''
    public static final String PREFIX = TerraformModel.PREFIX

    void setup() {
        writeBuildFile(WITH_WORKSPACES)
        createTF()
    }

    void cleanup() {
        cleanupPluginTree()
    }

    void 'Run terraform apply on different workspaces'() {
        when: 'running with default workspace'
        BuildResult defaultResult = getGradleRunner("${PREFIX}Apply").build()

        then:
        defaultResult.task(":${PREFIX}Init").outcome == SUCCESS
        defaultResult.task(":${PREFIX}Apply").outcome == SUCCESS

        when: 'running with first workspace'
        final alphaResult = getGradleRunner("${PREFIX}ApplyAlpha").build()

        then:
        alphaResult.task(":${PREFIX}Init").outcome == UP_TO_DATE
        alphaResult.task(":${PREFIX}ApplyAlpha").outcome == SUCCESS

        when: 'running with second workspace'
        final betaResult = getGradleRunner("${PREFIX}ApplyBeta").build()

        then:
        betaResult.task(":${PREFIX}Init").outcome == UP_TO_DATE
        betaResult.task(":${PREFIX}ApplyBeta").outcome == SUCCESS

        and: 'tfPlan should not have executed'
        betaResult.task(":${PREFIX}Plan") == null

        and: 'tfInitBeta should not exist as a task'
        betaResult.task(":${PREFIX}InitBeta") == null

        when: 'removing the workspaces'
        writeBuildFile(WITHOUT_WORKSPACES)
        final noWorkspaceResult = getGradleRunner("${PREFIX}Apply").build()

        then:
        noWorkspaceResult.task(":${PREFIX}Init").outcome == UP_TO_DATE

        when: 'running a task for a workspace that should no longer exists'
        final noWorkspaceResult2 = getGradleRunner("${PREFIX}ApplyAlpha").buildAndFail()

        then:
        noWorkspaceResult2.output.contains("Task '${PREFIX}ApplyAlpha' not found")

        when: 'cleaning up old workspaces without approval'
        final cleaningResult = getGradleRunner("${PREFIX}CleanupWorkspaces").buildAndFail()

        then: 'fails due to dangling workspaces'
        cleaningResult.task(":${PREFIX}CleanupWorkspaces").outcome == FAILED
        cleaningResult.output.contains('Workspace is not empty')
        cleaningResult.output.contains('Workspace "alpha" is currently tracking')

        when: 'cleaning up old workspaces with force'
        final cleaningResult2 = getGradleRunner("${PREFIX}CleanupWorkspaces", '--force').build()

        then: 'removes local workspaces'
        cleaningResult2.task(":${PREFIX}CleanupWorkspaces").outcome == SUCCESS
    }

    void 'Run apply for multiple workspaces in one command-line'() {
        when:
        final result = getGradleRunner("${PREFIX}Apply", "${PREFIX}ApplyAlpha", "${PREFIX}ApplyBeta").build()

        then:
        result.task(":${PREFIX}Apply").outcome == SUCCESS
        result.task(":${PREFIX}ApplyAlpha").outcome == SUCCESS
        result.task(":${PREFIX}ApplyBeta").outcome == SUCCESS
    }

    void writeBuildFile(String workspaces) {
        writeBasicBuildFile()
        buildFile << """
        terraform {
            backends {
                local(LocalBackend) {
                    path = 'build/tfstate/project.tfstate'
                }
            }
            sourceSets {
                main {
                    ${workspaces}
                }
            }
        }
        """.stripIndent()
    }

    void createTF() {
        writeHashiRandomProvidersFile()
        new File(srcDir, 'main.tf').text = '''
        resource "random_uuid" "test" {
        }
        '''.stripIndent()
    }

    GradleRunner getGradleRunner(CharSequence... args) {
        getGradleRunner(IS_GROOVY_DSL, args.toList()*.toString())
    }
}