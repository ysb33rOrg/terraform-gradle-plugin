/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.terraform

import org.ysb33r.gradle.terraform.testfixtures.IntegrationSpecification

import static org.gradle.testkit.runner.TaskOutcome.SUCCESS

class TerraformImportSpec extends IntegrationSpecification {

    String taskName = 'tfImport'

    void setup() {
        writeBasicBuildFile()
        writeHashiRandomProvidersFile()
    }

    void 'Can import a resource'() {
        setup:
        new File(srcDir, 'main.tf').text = '''
        resource "random_uuid" "test" {
        }
        '''.stripIndent()

        when:
        final result = getGradleRunner(IS_GROOVY_DSL, [
            '-i',
            'tfInit', taskName, '--path', 'random_uuid.test', '--id', 'aabbccdd-eeff-0011-2233-445566778899'
        ]).build()

        then:
        result.task(":${taskName}").outcome == SUCCESS
    }

    void 'Can import a resource for a specific workspace'() {
        setup:
        final workspace = 'staging'
        buildFile << """
        terraform {
            sourceSets {
                main {
                    workspaces '${workspace}'
                }
            }
        }
        """.stripIndent()
        new File(srcDir, 'main.tf').text = '''
        resource "random_uuid" "test" {
        }
        '''.stripIndent()

        when:
        final result = getGradleRunner(IS_GROOVY_DSL, [
            '-i',
            'tfInit', "${taskName}${workspace.capitalize()}".toString(),
            '--path', 'random_uuid.test', '--id', 'aabbccdd-eeff-0011-2233-445566778899'
        ]).build()

        then:
        result.task(":${taskName}${workspace.capitalize()}").outcome == SUCCESS
    }
}