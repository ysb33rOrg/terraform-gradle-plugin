/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.terraform

import org.gradle.testkit.runner.BuildResult
import org.ysb33r.gradle.terraform.internal.TerraformModel
import org.ysb33r.gradle.terraform.testfixtures.IntegrationSpecification

import static org.gradle.testkit.runner.TaskOutcome.SUCCESS

class TerraformTaintSpec extends IntegrationSpecification {

    public static final String RESOURCE_NAME = 'test'
    public static final String PREFIX = TerraformModel.PREFIX

    void setup() {
        writeBasicBuildFile()
        writeHashiRandomProvidersFile()
        writeMainTf()
        applyInit()
    }

    void 'Can taint and untaint a resource'() {
        setup: 'Start with a applied setup'
        final taintTask = "${PREFIX}Taint"
        final untaintTask = "${PREFIX}Untaint"
        final applyTask = "${PREFIX}Apply"

        when: 'the resource is tainted'
        final result1 = runTaintOrUntaint(taintTask)
        final result2 = applyAgain()

        then: 'apply will run again'
        result1.task(":${taintTask}").outcome == SUCCESS
        result2.task(":${applyTask}").outcome == SUCCESS

        when: 'the resource is tainted and then untainted'
        final result3 = runTaintOrUntaint(taintTask)
        final result4 = runTaintOrUntaint(untaintTask)
        final result5 = applyAgain()

        then: 'apply and plan will still run'
        result3.task(":${taintTask}").outcome == SUCCESS
        result4.task(":${untaintTask}").outcome == SUCCESS
        result5.task(":${PREFIX}Plan").outcome == SUCCESS
        result5.task(":${applyTask}").outcome == SUCCESS
    }

    private BuildResult runTaintOrUntaint(String taskName) {
        getGradleRunner(IS_GROOVY_DSL, [
            taskName,
            '--path', "random_uuid.${RESOURCE_NAME}"
        ]*.toString()).build()
    }

    private BuildResult applyAgain() {
        getGradleRunner(IS_GROOVY_DSL, [
            '-q',
            "${PREFIX}Apply",
        ]*.toString()).build()
    }

    private void applyInit() {
        getGradleRunner(IS_GROOVY_DSL, ["${PREFIX}Apply", '-q']*.toString()).build()
    }

    private void writeMainTf(String resourceName = RESOURCE_NAME) {
        new File(srcDir, 'main.tf').text = """
        resource "random_uuid" "${resourceName}" {
        }
        """.stripIndent()
    }
}