/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.terraform

import org.ysb33r.gradle.terraform.backends.GenericBackend
import org.ysb33r.gradle.terraform.backends.GitlabBackend
import org.ysb33r.gradle.terraform.backends.LocalBackend
import org.ysb33r.gradle.terraform.backends.TerraformRemoteBackend
import org.ysb33r.gradle.terraform.extensions.TerraformExtension
import org.ysb33r.gradle.terraform.plugins.TerraformBasePlugin
import org.ysb33r.gradle.terraform.testfixtures.UnitTestSpecification

class BackendSpec extends UnitTestSpecification {

    TerraformExtension toolExt

    void setup() {
        project.pluginManager.apply(TerraformBasePlugin)
        toolExt = project.extensions.getByType(TerraformExtension)
    }

    void 'Local backend can set path'() {
        setup:
        final local = toolExt.backends.create('local', LocalBackend)
        local.path = '../some/definedPath'

        when:
        final values = local.tokenProvider.get().values()*.value

        then:
        values.find { it.endsWith('definedPath"') }
    }

    void 'Set tokens of different types'() {
        setup:
        final backend = toolExt.backends.create('test', GenericBackend)

        when:
        backend.token('foo', 'us-east-1')
        final values = backend.tokenProvider.get().values()*.value

        then:
        values.containsAll(['"us-east-1"'])
    }

    void 'The legacy Terraform Cloud backend can be configured'() {
        setup:
        final backend = toolExt.backends.create('test', TerraformRemoteBackend)

        when:
        backend.authToken = 'us-east-1'
        backend.organization = 'foo'

        final values = backend.tokenProvider.get().values()*.value
        final secrets = backend.secretVariables.get().keySet()

        then:
        values.containsAll(['"foo"'])
        secrets.contains('TF_TOKEN_app_terraform_io')
    }

    void 'The Gitlab backend can be configured'() {
        setup:
        final backend = toolExt.backends.create('test', GitlabBackend)
        final user = 'username1'
        final pass = UUID.randomUUID().toString()
        final addr = 'https://gitlab.com/api/v4/projects/123456/terraform/state/my-project'
        final max1 = 5
        final minT = 20
        final maxT = 45

        when:
        backend.address = addr
        backend.username = user
        backend.accessToken = pass
        backend.retryMax = max1
        backend.retryWaitMin = minT
        backend.retryWaitMax = maxT

        final values = backend.tokenProvider.get().collectEntries { k, v -> [k, v.value] }
        final secrets = backend.secretVariables.get().keySet()

        then:
        secrets.containsAll(['TF_USERNAME', 'TF_PASSWORD'])
        values.address == "\"${addr}\""
        values.lock_address == "\"${addr}/lock\""
        values.unlock_address == "\"${addr}/lock\""
        values.lock_method == '"POST"'
        values.unlock_method == '"DELETE"'
        values.retry_wait_min == minT.toString()
        values.retry_wait_max == maxT.toString()
        values.retry_max == max1.toString()
    }
}