/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.terraform

import org.ysb33r.gradle.terraform.internal.TerraformModel
import org.ysb33r.gradle.terraform.plugins.TerraformWrapperPlugin
import org.ysb33r.gradle.terraform.tasks.TerraformCacheBinary
import org.ysb33r.gradle.terraform.testfixtures.IntegrationSpecification

import static org.gradle.testkit.runner.TaskOutcome.SUCCESS
import static org.ysb33r.gradle.terraform.internal.plugins.TerraformGlobalConfigForSingleProjectPlugin.RC_GENERATOR

class TerraformWrapperPluginSpec extends IntegrationSpecification {

    String wrapperExtensionName = TerraformWrapperPlugin.WRAPPER_EXTENSION_NAME
    String wrapperTaskName = TerraformWrapperPlugin.WRAPPER_TASK_NAME
    String cacheTaskName = TerraformWrapperPlugin.CACHE_BINARY_TASK_NAME
    String toolName = TerraformModel.EXE_BASE_NAME

    void 'Can cache a binary on behalf of a wrapper'() {
        setup:
        final propsFile = new File(projectDir, ".gradle/${TerraformCacheBinary.LOCATION_PROPERTIES_DEFAULT}")
        writeRootBuildFile()

        when:
        final result = getGradleRunner(IS_GROOVY_DSL, [cacheTaskName]).build()

        then:
        result.task(":${cacheTaskName}").outcome == SUCCESS
        result.task(":${RC_GENERATOR}").outcome == SUCCESS
        propsFile.exists()
    }

    void 'Can generate wrappers'() {
        setup:
        writeRootBuildFile()

        when:
        final result = getGradleRunner(IS_GROOVY_DSL, [wrapperTaskName]).build()

        then:
        result.task(":${wrapperTaskName}").outcome == SUCCESS
        result.task(":${RC_GENERATOR}").outcome == SUCCESS

        ["${toolName}w", "${toolName}w.bat"].collect {
            new File(projectDir, it)
        }*.exists()
    }

    private void writeRootBuildFile() {
        buildFile.text = """
        plugins {
            id 'org.ysb33r.${EXTENSION_NAME}.wrapper'
        }
        
        ${wrapperExtensionName} {
        }
        
        """.stripIndent()
    }
}