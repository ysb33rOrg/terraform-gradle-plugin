/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.terraform

import org.gradle.testkit.runner.BuildResult
import org.gradle.testkit.runner.GradleRunner
import org.ysb33r.gradle.terraform.internal.TerraformModel
import org.ysb33r.gradle.terraform.testfixtures.IntegrationSpecification
import spock.lang.Issue
import spock.lang.Unroll

import static org.gradle.testkit.runner.TaskOutcome.SUCCESS
import static org.gradle.testkit.runner.TaskOutcome.UP_TO_DATE
import static org.ysb33r.gradle.iac.base.internal.tf.TaskUtils.VARIABLE_CACHE_FILE_NAME

class TerraformPlanApplyAndDestroySpec extends IntegrationSpecification {

    public static final String PREFIX = TerraformModel.PREFIX
    public static final String initTask = "${PREFIX}Init"
    public static final String RESOURCE_NAME = 'random_uuid.test'

    File mainTF

    void setup() {
        mainTF = new File(srcDir, 'main.tf')
        writeBasicBuildFile()
        writeHashiRandomProvidersFile()
        createTF()
    }

    @Unroll
    void 'Run plan on a local resource (#format)'() {
        setup:
        File planFile = new File(buildDir, 'tf/main/main.tf.plan')
        File textFile = new File(buildDir, "reports/tf/main/main.tf.plan.${json ? 'json' : 'txt'}")
        def cmdLine = json ? ['--json'] : []

        when:
        BuildResult result = getGradleRunner(IS_GROOVY_DSL, ["${PREFIX}Plan"]*.toString() + cmdLine).build()

        then:
        result.task(":${initTask}").outcome == SUCCESS
        planFile.exists()
        textFile.exists()

        where:
        format | json
        'text' | false
        'json' | true
    }

    void 'Run apply on a local resource'() {
        when:
        BuildResult result = getGradleRunner(IS_GROOVY_DSL, ["${PREFIX}Apply"]*.toString()).build()

        then:
        result.task(":${initTask}").outcome == SUCCESS
        result.task(":${PREFIX}Plan").outcome == SUCCESS
        result.task(":${PREFIX}Apply").outcome == SUCCESS
    }

    @Unroll
    void 'Create destroy plan (#format)'() {
        setup:
        File planFile = new File(buildDir, 'tf/main/main.tf.destroy.plan')
        File textFile = new File(buildDir, "reports/tf/main/main.tf.destroy.plan.${json ? 'json' : 'txt'}")
        def cmdLine = json ? ['--json'] : []

        when:
        BuildResult result = getGradleRunner(
            IS_GROOVY_DSL,
            ["${PREFIX}Apply", "${PREFIX}DestroyPlan"]*.toString() + cmdLine
        ).build()

        then:
        result.task(":${PREFIX}Apply").outcome == SUCCESS
        result.task(":${PREFIX}DestroyPlan").outcome == SUCCESS
        planFile.exists()
        textFile.exists()

        where:
        format | json
        'text' | false
        'json' | true
    }

    void 'Run terraform state pull after apply'() {
        given:
        File stateFile = new File(srcDir, 'foo.tfstate')

        when:
        BuildResult result = getGradleRunner(
            IS_GROOVY_DSL,
            ["${PREFIX}Apply", "${PREFIX}StatePull", '--state-file', 'foo.tfstate', '-i']*.toString()
        ).build()

        then:
        result.task(":${PREFIX}StatePull").outcome == SUCCESS
        stateFile.exists()
    }

    void 'tfShowState should not run tfApply'() {
        setup:
        getGradleRunner(IS_GROOVY_DSL, ["${PREFIX}Apply"]*.toString()).build()

        when:
        final result = getGradleRunner(IS_GROOVY_DSL, ["${PREFIX}ShowState"]*.toString()).build()

        then:
        result.task(":${PREFIX}ShowState").outcome == SUCCESS
        result.task(":${PREFIX}Apply") == null

        when:
        final result2 = getGradleRunner(IS_GROOVY_DSL, ["${PREFIX}ShowState"]*.toString()).build()

        then:
        result2.task(":${PREFIX}ShowState").outcome == SUCCESS
    }

    @Unroll
    void 'Plan should not run twice if nothing changed, but should run if source changes (#cc)'() {
        when:
        final result = getGradleRunner(ccMode, "${PREFIX}Plan").build()

        then:
        result.task(":${PREFIX}Plan").outcome == SUCCESS

        when:
        final result2 = getGradleRunner(ccMode, "${PREFIX}Apply").build()

        then:
        result2.task(":${PREFIX}Init").outcome == UP_TO_DATE
        result2.task(":${PREFIX}Plan").outcome == UP_TO_DATE
        result2.task(":${PREFIX}Apply").outcome == SUCCESS

        when:
        final result3 = getGradleRunner(ccMode, "${PREFIX}Apply").build()

        then:
        result3.task(":${PREFIX}Init").outcome == UP_TO_DATE
        result3.task(":${PREFIX}Plan").outcome == UP_TO_DATE
        result3.task(":${PREFIX}Apply").outcome == UP_TO_DATE

        when:
        mainTF << '''
        resource "random_uuid" "test2" {
        }
        '''.stripIndent()
        final result4 = getGradleRunner(ccMode, "${PREFIX}Apply").build()

        then:
        result4.task(":${PREFIX}Init").outcome == SUCCESS
        result4.task(":${PREFIX}Plan").outcome == SUCCESS
        result4.task(":${PREFIX}Apply").outcome == SUCCESS

        where:
        cc              | ccMode
        'without cache' | false
        'with case'     | true
    }

    void 'Run destroy on a local resource'() {
        when:
        final result = getGradleRunner("${PREFIX}Apply", "${PREFIX}Destroy", '--approve').build()

        then:
        result.task(":${PREFIX}Apply").outcome == SUCCESS
        result.task(":${PREFIX}DestroyPlan").outcome == SUCCESS
        result.task(":${PREFIX}Destroy").outcome == SUCCESS
    }

    void 'tfApply should not run again when tfDestroy is called'() {
        when: 'Infrastructure is applied'
        getGradleRunner("${PREFIX}Apply").build()

        and: 'Sources are modified'
        mainTF << '\n\n\n'

        and: 'tfDestroy is called without tfApply preceding it'
        final result = getGradleRunner("${PREFIX}Destroy", '--approve').build()

        then: 'Only tfDestroy should be executed'
        result.task(":${PREFIX}Destroy").outcome == SUCCESS
        result.task(":${PREFIX}Apply") == null
        result.task(":${PREFIX}Plan") == null
    }

    @Issue('https://gitlab.com/ysb33rOrg/terraform-gradle-plugin/-/issues/47')
    void 'tfApply should pass target and replace parameters to tfPlan'() {
        when: 'Infrastructure is applied'
        BuildResult result1 = getGradleRunner("${PREFIX}Apply", '--target', RESOURCE_NAME, '-i').build()

        then:
        result1.task(":${PREFIX}Plan").outcome == SUCCESS
        result1.task(":${PREFIX}Apply").outcome == SUCCESS
        result1.output.contains("-target=${RESOURCE_NAME}")

        when:
        final result2 = getGradleRunner("${PREFIX}Apply", '--replace', RESOURCE_NAME, '-i').build()

        then:
        result2.task(":${PREFIX}Plan").outcome == SUCCESS
        result2.task(":${PREFIX}Apply").outcome == SUCCESS
        result2.output.contains("-replace=${RESOURCE_NAME}")
    }

    void 'When there is a relationship between two source sets, the tasks are sequenced'() {
        setup:
        buildFile << """
        terraform {
            sourceSets {
                main {
                  workspaces 'one'
                }
                other {
                  mustRunAfter ('main')
                  workspaces 'two'
                  useBackend('local')
               }
            }
        }
        """.stripIndent()

        final otherSrcDir = new File(mainTF.parentFile.parentFile, 'other')
        otherSrcDir.mkdirs()
        new File(otherSrcDir, 'providers.tf').text = """
        terraform {
            backend "local" {
            }
            required_providers {
                random = {
                  source = "hashicorp/random"
                  version = "3.6.3"
                }
            }
        }

        provider "random" {
        }
        """.stripIndent()
        new File(otherSrcDir, 'main.tf').text = '''
        resource "random_uuid" "test" {
        }
        '''.stripIndent()

        when:
        final result = getGradleRunner(
            "${PREFIX}OtherApplyTwo",
            "${PREFIX}OtherApply",
            "${PREFIX}ApplyOne",
            "${PREFIX}Apply"
        ).build()
        List<String> executed = result.output.lines().findAll { it.startsWith('> Task') }*.replaceFirst('> Task :', '')*.toString()
            .findAll { it.startsWith("${PREFIX}") }

        then:
        executed == [
            "${PREFIX}Init",
            "${PREFIX}Plan",
            "${PREFIX}Apply",
            "${PREFIX}PlanOne",
            "${PREFIX}ApplyOne",
            "${PREFIX}OtherInit",
            "${PREFIX}OtherPlanTwo",
            "${PREFIX}OtherApplyTwo",
            "${PREFIX}OtherPlan",
            "${PREFIX}OtherApply"
        ]
    }

    @Issue('https://gitlab.com/ysb33rOrg/terraform-gradle-plugin/-/issues/63')
    void 'tfPlan should add remote_state to tfvars if remoteStateMap.injectVar is set'() {
        setup:
        mainTF << '''
        variable remote_state {
            type = map(string)
        }
        '''

        buildFile << '''
        terraform {
            sourceSets {
                main {
                    variables {
                       remoteStateMap {
                         injectVar = true
                       }
                    }
                }
            }
        }
        '''

        when: 'Infrastructure is applied'
        final result1 = getGradleRunner("${PREFIX}Plan").build()

        then:
        result1.task(":${PREFIX}Plan").outcome == SUCCESS
        new File(buildDir, "tf/main/${VARIABLE_CACHE_FILE_NAME}").text.contains('remote_state')
    }

    void createTF() {
        mainTF.text = '''
        resource "random_uuid" "test" {
        }
        '''.stripIndent()
    }

    GradleRunner getGradleRunner(boolean withCache, String... args) {
        if (withCache) {
            getGradleRunnerConfigCache(IS_GROOVY_DSL, args.toList()*.toString())
        } else {
            getGradleRunner(IS_GROOVY_DSL, args.toList()*.toString())
        }
    }

    GradleRunner getGradleRunner(String... args) {
        getGradleRunner(IS_GROOVY_DSL, args.toList()*.toString())
    }
}