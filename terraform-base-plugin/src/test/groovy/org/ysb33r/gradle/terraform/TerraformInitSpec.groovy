/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.terraform

import org.gradle.testkit.runner.BuildResult
import org.gradle.testkit.runner.GradleRunner
import org.ysb33r.gradle.terraform.internal.TerraformUtils
import org.ysb33r.gradle.terraform.testfixtures.IntegrationSpecification
import spock.lang.Unroll

import static org.gradle.testkit.runner.TaskOutcome.NO_SOURCE
import static org.gradle.testkit.runner.TaskOutcome.SUCCESS
import static org.gradle.testkit.runner.TaskOutcome.UP_TO_DATE

class TerraformInitSpec extends IntegrationSpecification {

    String taskName = 'tfInit'
    String providerVersion = '2.70.0'

    void 'Run init on a clean project directory'() {
        setup:
        writeBuildFile()

        when:
        final result = getGradleRunner(IS_GROOVY_DSL, [taskName]).build()

        then:
        result.task(":${taskName}").outcome == NO_SOURCE
    }

    void 'Run init on a project with a single plugin'() {
        setup:
        File pluginDir = new File(
            testKitDir,
            "caches/terraform.d/registry.terraform.io/hashicorp/aws/${providerVersion}/${TerraformUtils.osArch()}"
        )
        writeBuildFile()
        writeTfInit()

        when:
        final result = getGradleRunner(IS_GROOVY_DSL, [taskName]).build()

        then:
        result.task(":${taskName}").outcome == SUCCESS
        new File(testKitDir, 'caches/terraform.d').exists()
        pluginDir.exists()
        pluginDir.listFiles().find { it.name.startsWith('terraform-') }
    }

    void 'Run init on a project with a single plugin and a custom plugin cache directory'() {
        setup:
        File pluginDir = new File(
            projectDir,
            ".gradle/.${PREFIX}.d/test-project.main/registry.terraform.io/hashicorp/aws/${providerVersion}/${TerraformUtils.osArch()}"
        )
        writeBuildFile()
        writeTfInit()

        buildFile << """
        ${EXTENSION_NAME} {
          sourceSets {
            main {
              useCustomPluginCache()
            }
          }
        }
        """.stripIndent()

        when:
        final result = getGradleRunner(IS_GROOVY_DSL, [taskName]).build()

        then:
        result.task(":${taskName}").outcome == SUCCESS
        pluginDir.exists()
        pluginDir.listFiles().find { it.name.startsWith('terraform-') }
    }

    @Unroll
    void '--reconfigure will cause out of date condition (#cc)'() {
        setup:
        final gradleRunner = ccMode ?
            getGradleRunnerConfigCache(IS_GROOVY_DSL, [taskName, '-i']) :
            getGradleRunner(IS_GROOVY_DSL, [taskName, '-i'])
        writeTfInit()
        writeBuildFile()

        when: 'tfInit is run the first time'
        final result1 = gradleRunner.build()

        then: 'the task is executed'
        result1.task(":${taskName}").outcome == SUCCESS

        when: 'tfInit is run the second time'
        final result2 = gradleRunner.build()

        then: 'the task is not executed'
        result2.task(":${taskName}").outcome == UP_TO_DATE

        when: 'tfInit is run with --reconfigure'
        BuildResult result3 = nextRunner('--reconfigure').build()

        then: 'the task is executed'
        result3.task(":${taskName}").outcome == SUCCESS

        when: 'tfInit is run with --force-copy'
        BuildResult result4 = nextRunner('--force-copy').build()

        then: 'the task is executed'
        result4.task(":${taskName}").outcome == SUCCESS

        when: 'tfInit is run with --upgrade'
        BuildResult result5 = nextRunner('--upgrade').build()

        then: 'the task is executed'
        result5.task(":${taskName}").outcome == SUCCESS

        when: 'tfInit is then run again without parameters'
        BuildResult result6 = nextRunner().build()

        then: 'the task is not executed'
        result6.task(":${taskName}").outcome == UP_TO_DATE

        where:
        cc                                | ccMode
        'without any configuration cache' | false
        'with configuration cache'        | true
    }

    void 'Can initialise a subproject'() {
        setup:
        final subprojectName = 'infra'
        final subproject = new File(projectDir, subprojectName)
        subproject.mkdirs()
        writeBuildFile(subproject)
        writeTfInit(subproject)

        when:
        final result = getGradleRunner(IS_GROOVY_DSL, [taskName]).build()

        then:
        result.task(":${subprojectName}:${taskName}").outcome == SUCCESS
        new File(testKitDir, 'caches/terraform.d').exists()
    }

    GradleRunner nextRunner(String... args) {
        List<String> args1 = [
            taskName,
            '-s', '-i'
        ]
        args1.addAll(args)

        getGradleRunner(IS_GROOVY_DSL, args1)
    }

    void writeBuildFile(File subProjectDir = null) {
        final target = subProjectDir ? new File(subProjectDir, 'build.gradle') : buildFile
        target.text = '''
        import org.ysb33r.gradle.terraform.backends.LocalBackend
        plugins {
            id 'org.ysb33r.terraform.base'
        }
        
        terraform {
          backends {
            local(LocalBackend) {
                path = '../terraform.tfstate'
            }
          }
          sourceSets {
            main {
              useBackend('local')
            }
          }
        }
        '''.stripIndent()

        if (subProjectDir) {
            buildFile.text = '''
            plugins {
                id 'org.ysb33r.terraform.rc'
                id 'org.ysb33r.terraform.base' apply false
            }
            '''.stripIndent()

            settingsFile << """
            include '${subProjectDir.name}'
            """.stripIndent()
        }
    }

    void writeTfInit(File subProjectDir = null) {
        final target = subProjectDir ? new File(subProjectDir, 'src/tf/main/init.tf') : new File(srcDir, 'init.tf')
        target.parentFile.mkdirs()
        target.text = """
        terraform {
            backend "local" {
            }
            required_providers {
                aws = {
                  version = "= ${providerVersion}"
                  source = "hashicorp/aws"
                }
            }
        }

        provider "aws" {
          region  = "us-east-1"
        }
        """.stripIndent()
    }
}