/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.terraform

import org.ysb33r.gradle.terraform.extensions.TerraformExtension
import org.ysb33r.gradle.terraform.plugins.TerraformBasePlugin
import org.ysb33r.gradle.terraform.testfixtures.UnitTestSpecification

class TerraformToolchainSpec extends UnitTestSpecification {

    TerraformExtension terraform

    void setup() {
        project.pluginManager.apply(TerraformBasePlugin)

        terraform = project.extensions.getByType(TerraformExtension)
    }

    void 'When the plugin is applied a default toolchain will be added'() {
        expect:
        terraform.toolchains.getByName('standard')
    }

    void 'Can add a 2nd plugin'() {
        setup:
        terraform.toolchains {
            extra {
                executableByVersion(TERRAFORM_TEST_VERSIONS[0])
            }
        }

        expect:
        terraform.toolchains.getByName('extra').runExecutableAndReturnVersion() == TERRAFORM_TEST_VERSIONS[0]
        true
    }
}