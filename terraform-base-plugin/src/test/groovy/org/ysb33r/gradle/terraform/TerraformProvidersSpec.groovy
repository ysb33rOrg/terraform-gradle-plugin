/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.terraform

import org.gradle.testkit.runner.GradleRunner
import org.ysb33r.gradle.terraform.internal.TerraformModel
import org.ysb33r.gradle.terraform.testfixtures.IntegrationSpecification

import static org.gradle.testkit.runner.TaskOutcome.SUCCESS

class TerraformProvidersSpec extends IntegrationSpecification {

    File reportsDir

    void setup() {
        writeBasicBuildFile()
        createTF()
        reportsDir = new File(buildDir, 'reports/tf/main')
    }

    void 'tfProvidersShow describes the providers on the console'() {
        when:
        final result1 = nextCacheRunner('tfProvidersShow').build()

        then:
        result1.task(':tfProvidersShow').outcome == SUCCESS
        result1.output.contains('provider[registry.terraform.io/hashicorp/random]')
    }

    void 'tfProvidersLock will lock more than the current platform'() {
        when: 'tfProvidersLock is run the first time'
        final result1 = nextCacheRunner('tfProvidersLock').build()
        final lockFile = new File(srcDir, TerraformModel.PROVIDER_LOCK_FILENAME)

        then: 'the task is executed'
        result1.task(':tfProvidersLock').outcome == SUCCESS
        result1.output.contains('darwin_arm64')
        result1.output.contains('windows_386')
        lockFile.exists()
    }

    void 'tfProvidersSchema puts schema in an output file'() {
        when:
        final result1 = nextCacheRunner('tfProvidersSchema').build()
        final schemaFile = new File(reportsDir, 'main.schema.json')

        then:
        result1.task(':tfInit').outcome == SUCCESS
        result1.task(':tfProvidersSchema').outcome == SUCCESS
        schemaFile.exists()
    }

    GradleRunner nextCacheRunner(String taskName) {
        getGradleRunnerConfigCache(IS_GROOVY_DSL, ['tfInit', taskName, '-i'])
    }

    void createTF() {
        writeHashiRandomProvidersFile()

        buildFile << '''
        terraform {
            platforms allPlatforms
        }
        '''
    }
}