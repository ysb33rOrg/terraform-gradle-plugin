/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.terraform

import org.testcontainers.containers.localstack.LocalStackContainer
import org.testcontainers.spock.Testcontainers
import org.testcontainers.utility.DockerImageName
import org.ysb33r.gradle.terraform.internal.TerraformModel
import org.ysb33r.gradle.terraform.testfixtures.IntegrationSpecification
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials
import software.amazon.awssdk.auth.credentials.AwsCredentials
import software.amazon.awssdk.auth.credentials.AwsCredentialsProvider
import software.amazon.awssdk.regions.Region
import software.amazon.awssdk.services.s3.S3Client
import software.amazon.awssdk.services.s3.model.CreateBucketRequest
import software.amazon.awssdk.services.s3.model.HeadBucketRequest
import spock.lang.Shared

import static org.gradle.testkit.runner.TaskOutcome.SUCCESS
import static org.testcontainers.containers.localstack.LocalStackContainer.Service.IAM
import static org.testcontainers.containers.localstack.LocalStackContainer.Service.S3
import static org.testcontainers.containers.localstack.LocalStackContainer.Service.STS

@Testcontainers
class TerraformStateIntegrationSpec extends IntegrationSpecification {

    public static final String RESOURCE_NAME = 'test'
    public static final String PREFIX = TerraformModel.PREFIX
    public static final String BUCKET = 'bucketeer'

    @Shared
    LocalStackContainer localstack = new LocalStackContainer(DockerImageName.parse(
        'localstack/localstack:4.1.1'
    )).withServices(S3, STS)

    void setupSpec() {
        createBucket()
    }

    void setup() {
        localstack.endpoint
        localstack.accessKey
        localstack.secretKey
        writeBasicBuildFile()
        writeHashiRandomProvidersFile('s3')
        writeMainTf()
        updateBuildToUseS3()
    }

    void 'Can pull and push a state file'() {
        final pullTask = "${PREFIX}StatePull"
        final pushTask = "${PREFIX}StatePush"
        final stateFile = '../backup.tfstate'
        getGradleRunner(IS_GROOVY_DSL, ["${PREFIX}Init"]*.toString()).build()

        when:
        getGradleRunner(IS_GROOVY_DSL, ["${PREFIX}Apply"]*.toString()).build()
        final resultPull = getGradleRunner(IS_GROOVY_DSL, [
            '-i',
            pullTask,
            '--state-file', stateFile
        ]*.toString()).build()

        then:
        resultPull.task(":${pullTask}").outcome == SUCCESS
        resultPull.task(":${PREFIX}Init")?.outcome == null

        when:
        new File(buildDir, 'tf').deleteDir()
        getGradleRunner(IS_GROOVY_DSL, ["${PREFIX}Init".toString()]).build()

        final resultPush = getGradleRunner(IS_GROOVY_DSL, [
            '-i',
            pushTask,
            '--force',
            '--state-file', stateFile
        ]*.toString()).build()

        then:
        resultPush.task(":${PREFIX}Init")?.outcome == null
        resultPush.task(":${pushTask}").outcome == SUCCESS
    }

    private void writeMainTf(String resourceName = RESOURCE_NAME) {
        new File(srcDir, 'main.tf').text = """
        resource "random_uuid" "${resourceName}" {
        }
        """.stripIndent()
    }

    private void updateBuildToUseS3() {
        buildFile << """
        terraform {
            backends {
                s3(S3Backend) {
                    region = '${localstack.region}'
                    bucket = '${BUCKET}'
                    key = '${this.class.canonicalName}.tfstate'
                    s3Endpoint = '${localstack.getEndpointOverride(S3)}'
                    stsEndpoint = '${localstack.getEndpointOverride(STS)}'
                    iamEndpoint = '${localstack.getEndpointOverride(IAM)}'
                    accessKey = '${localstack.accessKey}'
                    secretKey = '${localstack.secretKey}'
                    forcePathStyle = true
                }
            }
            sourceSets {
              main {
                useBackend('s3')
                
                executionOptions {
                    lock {
                        enabled = false
                    }
                }
              }
            }
        }
        """.stripIndent()
    }

    private void createBucket() {
        final client = S3Client.builder()
            .region(Region.of(localstack.region))
            .credentialsProvider(new CredentialsProvider(localstack))
            .endpointOverride(localstack.endpoint)
            .build()

        final req = CreateBucketRequest.builder().bucket(BUCKET).build()
        final waitReq = HeadBucketRequest.builder()
            .bucket(BUCKET)
            .build()

        final waiter = client.waiter()
        client.createBucket(req)
        waiter.waitUntilBucketExists(waitReq)
    }

    private static class CredentialsProvider implements AwsCredentialsProvider {
        final AwsBasicCredentials creds

        CredentialsProvider(LocalStackContainer owner) {
            this.creds = AwsBasicCredentials.create(
                new String(owner.accessKey),
                new String(owner.secretKey)
            )
        }

        @Override
        AwsCredentials resolveCredentials() {
            this.creds
        }
    }
}