/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.terraform.testfixtures

import org.gradle.testkit.runner.GradleRunner
import org.ysb33r.grolifant5.api.core.OperatingSystem
import spock.lang.Specification
import spock.lang.TempDir

import java.nio.file.FileVisitResult
import java.nio.file.FileVisitor
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.attribute.BasicFileAttributes

import static java.nio.file.FileVisitResult.CONTINUE

@SuppressWarnings(['LineLength'])
class IntegrationSpecification extends Specification {
    public static final boolean IS_KOTLIN_DSL = false
    public static final boolean IS_GROOVY_DSL = true
    public static final OperatingSystem OS = OperatingSystem.current()
    public static final List<String> TERRAFORM_TEST_VERSIONS = System.getProperty('TERRAFORM_TEST_VERSIONS')
        .split(',').toList()

    @TempDir
    File testProjectDir

    File projectDir
    File buildDir
    File projectCacheDir
    File buildFile
    File settingsFile
    File testKitDir
    File srcDir

    void setup() {
        projectDir = new File(testProjectDir, 'test-project')
        projectDir.mkdirs()
        buildDir = new File(projectDir, 'build')
        projectCacheDir = new File(projectDir, '.gradle')
        buildFile = new File(projectDir, 'build.gradle')
        settingsFile = new File(projectDir, 'settings.gradle')
        settingsFile.text = ''

        testKitDir = new File(testProjectDir, '.testkit')
        testKitDir.mkdirs()

        srcDir = new File(projectDir, 'src/tf/main')
        srcDir.mkdirs()
    }

    GradleRunner getGradleRunner(
        boolean groovyDsl,
        String taskName
    ) {
        getGradleRunner(groovyDsl, [taskName])
    }

    GradleRunner getGradleRunner(
        boolean groovyDsl,
        List<String> args
    ) {
        GradleRunner.create()
            .withProjectDir(projectDir)
            .withArguments([
                "-Dorg.ysb33r.gradle.terraform.releases.uri=${System.getProperty('org.ysb33r.gradle.terraform.uri')}".toString(),
                '-s'
            ] + args)
            .forwardOutput()
            .withDebug(groovyDsl)
            .withPluginClasspath()
            .withTestKitDir(testKitDir)
    }

    GradleRunner getGradleRunnerConfigCache(
        boolean groovyDsl,
        List<String> args
    ) {
        getGradleRunner(groovyDsl, ['--configuration-cache', '--configuration-cache-problems=fail'] + args)
            .withGradleVersion('8.9')
            .withDebug(false)
    }

    void writeBasicBuildFile(String plugin = 'org.ysb33r.terraform.base') {
        buildFile.text = """
        import org.ysb33r.gradle.terraform.backends.S3Backend
        plugins {
            id '${plugin}'
        }
        
        terraform {
          sourceSets {
            main {
              executionOptions {
                stateOptions {
                  maxParallel = 1
                }
              }
            }
          }
        }
        """.stripIndent()
    }

    void cleanupPluginTree() {
        Files.walkFileTree(
            new File(projectDir, 'build/tf/main/plugins').toPath(),
            new FileVisitor<Path>() {
                @Override
                FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
                    CONTINUE
                }

                @Override
                FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                    if (attrs.symbolicLink) {
                        println "Deleting: ${file}"
                        Files.delete(file)
                    }
                    CONTINUE
                }

                @Override
                FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
                    println "Failed to visit: ${file}, because ${exc.message}"
                    CONTINUE
                }

                @Override
                FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                    CONTINUE
                }
            }
        )
    }

    void writeHashiRandomProvidersFile(String backend) {
        new File(srcDir, 'providers.tf').text = """
        terraform {
            backend "${backend}" {
            }
            required_providers {
                random = {
                  source = "hashicorp/random"
                  version = "3.6.3"
                }
            }
        }

        provider "random" {
        }
        """.stripIndent()
    }

    static String getEscapedEnvPathString() {
        if (OS.windows) {
            System.getenv(OS.pathVar).replace(BACKSLASH, DOUBLE_BACKSLASH)
        } else {
            System.getenv(OS.pathVar)
        }
    }

    static String getEscapedPathString(String path) {
        if (OS.windows) {
            path.replace('/', BACKSLASH)
        } else {
            path
        }
    }

    static private final String BACKSLASH = '\\'
    static private final String DOUBLE_BACKSLASH = BACKSLASH * 2
}