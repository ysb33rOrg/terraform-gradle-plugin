/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.terraform.internal

import groovy.transform.CompileStatic
import org.gradle.api.Project
import org.ysb33r.gradle.iac.base.tf.config.multilevel.ExecutionOptions
import org.ysb33r.gradle.terraform.TerraformSourceSet
import org.ysb33r.gradle.terraform.extensions.TerraformExtension

/**
 * Uses for configuring newly created tasks.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class TaskConfiguration {
    final TerraformSourceSet sourceSet
    final String taskName
    final String workspaceName
    final TerraformExtension ext
    final Project project
    final Iterable<Class<? extends ExecutionOptions>> exeOpts

    @SuppressWarnings('ParameterCount')
    TaskConfiguration(
        TerraformSourceSet sourceSet,
        String taskName,
        String workspaceName,
        TerraformExtension ext,
        Project project,
        Iterable<Class<? extends ExecutionOptions>> exeOpts
    ) {
        this.sourceSet = sourceSet
        this.taskName = taskName
        this.project = project
        this.exeOpts = exeOpts
        this.ext = ext
        this.workspaceName = workspaceName
    }
}
