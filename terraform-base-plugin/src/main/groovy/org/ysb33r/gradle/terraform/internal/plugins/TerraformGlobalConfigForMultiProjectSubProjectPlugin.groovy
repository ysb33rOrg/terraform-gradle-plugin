/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.terraform.internal.plugins

import groovy.transform.CompileStatic
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.ysb33r.gradle.iac.base.tf.tasks.GlobalConfigDetailsConsumerBase
import org.ysb33r.gradle.iac.base.tf.tasks.PluginCacheDirGenerator
import org.ysb33r.gradle.terraform.internal.TerraformModel
import org.ysb33r.gradle.terraform.internal.TerraformTasks
import org.ysb33r.gradle.terraform.tasks.TerraformCustomFmtApply
import org.ysb33r.grolifant5.api.core.ProjectOperations

import static org.ysb33r.gradle.terraform.internal.plugins.TerraformGlobalConfigForMultiProjectRootPlugin.DETAILS_FILENAME
import static org.ysb33r.gradle.terraform.internal.plugins.TerraformGlobalConfigForMultiProjectRootPlugin.DETAILS_SUBDIR
import static org.ysb33r.gradle.terraform.internal.plugins.TerraformGlobalConfigForMultiProjectRootPlugin.GLOBAL_CONFIG_INCOMING
import static org.ysb33r.gradle.terraform.internal.plugins.TerraformGlobalConfigForMultiProjectRootPlugin.GLOBAL_CONFIG_OUTGOING
import static org.ysb33r.gradle.terraform.internal.plugins.TerraformGlobalConfigForMultiProjectRootPlugin.GLOBAL_CONFIG_RESOLVE

/**
 * Used internally for single projects.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class TerraformGlobalConfigForMultiProjectSubProjectPlugin implements Plugin<Project> {
    public static final String SYNC_TASK = "${TerraformModel.PREFIX}SyncGlobalConfig"

    @Override
    void apply(Project project) {
        createIncomingRepoDetailsConfigurations(project)
    }

    private void createIncomingRepoDetailsConfigurations(Project project) {
        ProjectOperations.find(project)
            .configurations.createLocalRoleFocusedConfiguration(
            GLOBAL_CONFIG_INCOMING,
            GLOBAL_CONFIG_RESOLVE,
            false
        )

        project.dependencies.add(
            GLOBAL_CONFIG_INCOMING,
            project.dependencies.project(path: ':', configuration: GLOBAL_CONFIG_OUTGOING)
        )

        final sync = project.tasks.register(SYNC_TASK, GlobalConfigConsumer)

        project.tasks.withType(TerraformTasks.INIT.type).configureEach {
            it.dependsOn(sync)
        }
        project.tasks.withType(TerraformCustomFmtApply).configureEach {
            it.dependsOn(sync)
        }
        project.tasks.withType(PluginCacheDirGenerator).configureEach {
            it.dependsOn(sync)
        }
    }

    static class GlobalConfigConsumer extends GlobalConfigDetailsConsumerBase {
        GlobalConfigConsumer() {
            super(
                DETAILS_SUBDIR,
                DETAILS_FILENAME,
                GLOBAL_CONFIG_RESOLVE
            )
        }
    }
}
