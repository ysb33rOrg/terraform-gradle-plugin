/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.terraform.extensions

import groovy.transform.CompileStatic
import org.gradle.api.Action
import org.gradle.api.ExtensiblePolymorphicDomainObjectContainer
import org.gradle.api.NamedDomainObjectContainer
import org.gradle.api.Project
import org.gradle.api.model.ObjectFactory
import org.gradle.api.provider.Provider
import org.ysb33r.gradle.iac.base.internal.tf.DefaultPlatform
import org.ysb33r.gradle.iac.base.internal.tf.SecretUtils
import org.ysb33r.gradle.iac.base.internal.tf.SubprojectConfigDetails
import org.ysb33r.gradle.iac.base.secrets.IacSecrets
import org.ysb33r.gradle.iac.base.tf.IacPlatforms
import org.ysb33r.gradle.iac.base.tf.OtherSources
import org.ysb33r.gradle.iac.base.tf.PluginCache
import org.ysb33r.gradle.iac.base.tf.TfConfig
import org.ysb33r.gradle.terraform.TerraformSourceSet
import org.ysb33r.gradle.terraform.TerraformToolchain
import org.ysb33r.gradle.terraform.backends.GenericBackend
import org.ysb33r.gradle.terraform.backends.GitlabBackend
import org.ysb33r.gradle.terraform.backends.LocalBackend
import org.ysb33r.gradle.terraform.backends.S3Backend
import org.ysb33r.gradle.terraform.backends.TerraformBackend
import org.ysb33r.gradle.terraform.backends.TerraformRemoteBackend
import org.ysb33r.gradle.terraform.internal.SourceSetFactory
import org.ysb33r.gradle.terraform.internal.TerraformModel
import org.ysb33r.gradle.terraform.internal.ToolchainFactory
import org.ysb33r.gradle.terraform.internal.plugins.TerraformGlobalConfigForMultiProjectSubProjectPlugin
import org.ysb33r.gradle.terraform.plugins.TerraformBasePlugin
import org.ysb33r.grolifant5.api.core.ClosureUtils
import org.ysb33r.grolifant5.api.core.ConfigCacheSafeOperations

import static org.ysb33r.gradle.terraform.internal.Downloader.downloadSupported
import static org.ysb33r.gradle.terraform.internal.TerraformModel.DEFAULT_WORKSPACE

/**
 * Terraform extension.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0 (completely rewritten from 1.x)
 */
@CompileStatic
class TerraformExtension implements IacPlatforms {
    public static final String DEFAULT_TOOLCHAIN = 'standard'
    public static final String NAME = 'terraform'
    public static final String EXECUTABLE_BASE_NAME = TerraformModel.EXE_BASE_NAME

    final NamedDomainObjectContainer<TerraformSourceSet> sourceSets
    final NamedDomainObjectContainer<TerraformToolchain> toolchains
    final ExtensiblePolymorphicDomainObjectContainer<TerraformBackend> backends
    final ExtensiblePolymorphicDomainObjectContainer<IacSecrets> secrets
    final Provider<File> pluginCacheDir
    final Provider<Integer> pluginCacheDirTimeout
    final Provider<File> globalConfigFile

    TerraformExtension(Project tempProjectReference) {
        final ccso = ConfigCacheSafeOperations.from(tempProjectReference)
        final versions = ccso.fsOperations().loadPropertiesFromResource(
            TerraformBasePlugin.VERSION_RESOURCE_PATH,
            this.class.classLoader
        )
        this.otherSources = tempProjectReference.objects.newInstance(OtherSources)
        this.platforms = new DefaultPlatform(TerraformModel.PLATFORMS, tempProjectReference.objects)
        this.sourceSets = tempProjectReference.objects.domainObjectContainer(
            TerraformSourceSet,
            new SourceSetFactory(this, tempProjectReference)
        )
        this.toolchains = tempProjectReference.objects.domainObjectContainer(
            TerraformToolchain,
            new ToolchainFactory(this, tempProjectReference)
        )

        this.backends = tempProjectReference.objects.polymorphicDomainObjectContainer(TerraformBackend)
        registerBackendFactoriesAndBackends(tempProjectReference.objects)

        this.secrets = tempProjectReference.objects.polymorphicDomainObjectContainer(IacSecrets)
        registerSecretFactories(tempProjectReference.objects)

        final tc = this.toolchains.create(DEFAULT_TOOLCHAIN)

        if (downloadSupported) {
            tc.executableByVersion(versions['terraform-version'])
        } else {
            tc.executableBySearchPath(EXECUTABLE_BASE_NAME)
        }

        this.sourceSets.whenObjectAdded { TerraformSourceSet ss ->
            TerraformModel.createTasksFromModel(ss, DEFAULT_WORKSPACE, tempProjectReference)
        }

        final pc = getPluginCache(tempProjectReference)
        this.pluginCacheDir = pc.pluginCacheDir
        this.pluginCacheDirTimeout = pc.useLockWithTimeout
        this.globalConfigFile = getTfConfig(tempProjectReference).configFile
    }

    OtherSources getOtherSources() {
        this.otherSources
    }

    void otherSources(Action<OtherSources> configurator) {
        configurator.execute(this.otherSources)
    }

    void otherSources(@DelegatesTo(OtherSources) Closure<?> configurator) {
        ClosureUtils.configureItem(this.otherSources, configurator)
    }

    private static PluginCache getPluginCache(Project tempProjectReference) {
        final ccso = ConfigCacheSafeOperations.from(tempProjectReference)

        if (ccso.projectTools().multiProject) {
            if (ccso.projectTools().rootProject) {
                tempProjectReference.extensions.getByType(TerraformRCExtension)
            } else {
                new SubprojectConfigDetails(tempProjectReference.tasks.named(
                    TerraformGlobalConfigForMultiProjectSubProjectPlugin.SYNC_TASK,
                    TerraformGlobalConfigForMultiProjectSubProjectPlugin.GlobalConfigConsumer
                ).flatMap { it.detailsLocation })
            }
        } else {
            tempProjectReference.extensions.getByType(TerraformRCExtension)
        }
    }

    private static TfConfig getTfConfig(Project tempProjectReference) {
        final ccso = ConfigCacheSafeOperations.from(tempProjectReference)

        if (ccso.projectTools().multiProject) {
            if (ccso.projectTools().rootProject) {
                tempProjectReference.extensions.getByType(TerraformRCExtension)
            } else {
                new SubprojectConfigDetails(tempProjectReference.tasks.named(
                    TerraformGlobalConfigForMultiProjectSubProjectPlugin.SYNC_TASK,
                    TerraformGlobalConfigForMultiProjectSubProjectPlugin.GlobalConfigConsumer
                ).flatMap { it.detailsLocation })
            }
        } else {
            tempProjectReference.extensions.getByType(TerraformRCExtension)
        }
    }

    // tag::registerBackendFactoriesAndBackends[]
    private void registerBackendFactoriesAndBackends(ObjectFactory objects) {
        backends.registerFactory(GenericBackend) {
            objects.newInstance(GenericBackend, it, owner)
        }
        backends.registerFactory(LocalBackend) {
            objects.newInstance(LocalBackend, it, owner)
        }
        backends.registerFactory(S3Backend) {
            objects.newInstance(S3Backend, it, owner)
        }
        // end::registerBackendFactoriesAndBackends[]
        backends.registerFactory(TerraformRemoteBackend) {
            objects.newInstance(TerraformRemoteBackend, it, owner)
        }
        backends.registerFactory(GitlabBackend) {
            objects.newInstance(GitlabBackend, it, owner)
        }
        // tag::registerBackendFactoriesAndBackends[]
    }
    // end::registerBackendFactoriesAndBackends[]

    private void registerSecretFactories(ObjectFactory objects) {
        SecretUtils.registerIacSecretFactories(secrets, objects)
    }

    @Delegate
    private final IacPlatforms platforms

    private final OtherSources otherSources
}
