/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.terraform.plugins

import groovy.transform.CompileStatic
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.ysb33r.gradle.iac.base.IacBasePlugin
import org.ysb33r.gradle.iac.base.errors.IacConfigurationException
import org.ysb33r.gradle.iac.base.internal.tf.TaskUtils
import org.ysb33r.gradle.terraform.extensions.TerraformRCExtension
import org.ysb33r.gradle.terraform.extensions.TerraformWrapperExtension
import org.ysb33r.gradle.terraform.tasks.TerraformCacheBinary
import org.ysb33r.gradle.terraform.tasks.TerraformWrapper
import org.ysb33r.grolifant5.api.core.ProjectOperations

import static org.ysb33r.gradle.terraform.internal.TerraformModel.TOOL_NAME
import static org.ysb33r.gradle.terraform.internal.plugins.TerraformGlobalConfigForSingleProjectPlugin.RC_GENERATOR

/**
 * Plugin for creating terraform wrapper scripts.
 *
 * @author Schalk W. Cronjé
 */
@CompileStatic
class TerraformWrapperPlugin implements Plugin<Project> {
    public static final String WRAPPER_TASK_NAME = "${TOOL_NAME.uncapitalize()}Wrapper"
    public static final String WRAPPER_EXTENSION_NAME = "${TOOL_NAME.uncapitalize()}w"
    public static final String CACHE_BINARY_TASK_NAME = "cache${TOOL_NAME.capitalize()}Binary"

    @Override
    void apply(Project project) {
        project.pluginManager.apply(IacBasePlugin)

        if (!ProjectOperations.find(project).projectTools.rootProject) {
            throw new IacConfigurationException(
                "The ${TOOL_NAME.capitalize()} wrapper plugin can only be applied to the root project"
            )
        }

        project.pluginManager.tap {
            apply(TerraformRCPlugin)
        }

        final rcExt = project.extensions.getByType(TerraformRCExtension)
        final wExt = project.extensions.create(
            TerraformWrapperExtension.NAME,
            TerraformWrapperExtension,
            project
        )

        final cacheBinary = project.tasks.register(CACHE_BINARY_TASK_NAME, TerraformCacheBinary) {
            it.dependsOn(RC_GENERATOR)
        }
        TaskUtils.configureWrapperCacheBinaryTask(cacheBinary, rcExt, wExt, TOOL_NAME)

        final wrapper = project.tasks.register(WRAPPER_TASK_NAME, TerraformWrapper) {
            it.dependsOn(RC_GENERATOR)
        }
        TaskUtils.configureWrapperTask(wrapper, cacheBinary, TOOL_NAME)
    }
}
