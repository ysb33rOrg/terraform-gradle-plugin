/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.terraform.tasks

import groovy.transform.CompileStatic
import org.ysb33r.gradle.iac.base.tf.ToolFeatures
import org.ysb33r.gradle.iac.base.tf.tasks.AbstractPlanTask
import org.ysb33r.gradle.terraform.TerraformExecSpec
import org.ysb33r.gradle.terraform.internal.TerraformFeatures

import javax.inject.Inject

import static org.ysb33r.gradle.terraform.internal.TerraformModel.DEFAULT_WORKSPACE

/**
 * Equivalent of {@code terraform plan -destroy}.
 *
 * @since 0.1
 */
@CompileStatic
class TerraformDestroyPlan extends AbstractPlanTask {
    @Inject
    TerraformDestroyPlan(String workspaceName) {
        super(workspaceName, DEFAULT_WORKSPACE, TerraformExecSpec, true)
    }

    /**
     * Loads features for the tool
     *
     * @param ver Tool version
     * @return Tool features
     */
    @Override
    protected ToolFeatures loadToolFeatures(String ver) {
        new TerraformFeatures(ver)
    }
}
