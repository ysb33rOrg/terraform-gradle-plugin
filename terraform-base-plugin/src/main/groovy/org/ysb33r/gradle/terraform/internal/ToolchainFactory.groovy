/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.terraform.internal

import groovy.transform.CompileStatic
import org.gradle.api.NamedDomainObjectFactory
import org.gradle.api.Project
import org.gradle.api.model.ObjectFactory
import org.ysb33r.gradle.terraform.TerraformToolchain
import org.ysb33r.gradle.terraform.extensions.TerraformExtension

/**
 * OpenTofu Toolchain factory
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class ToolchainFactory implements NamedDomainObjectFactory<TerraformToolchain> {
    ToolchainFactory(TerraformExtension parent, Project project) {
        this.objectFactory = project.objects
        this.parent = parent
    }

    @Override
    TerraformToolchain create(String name) {
        objectFactory.newInstance(TerraformToolchain, name)
    }

    private final TerraformExtension parent
    private final ObjectFactory objectFactory
}
