/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.terraform.internal

import groovy.transform.CompileStatic
import org.gradle.api.Project
import org.ysb33r.gradle.iac.base.internal.tf.InitRelationship
import org.ysb33r.gradle.iac.base.tf.config.multilevel.CodeFormatting
import org.ysb33r.gradle.iac.base.tf.config.multilevel.ExecutionConfiguration
import org.ysb33r.gradle.iac.base.tf.config.multilevel.Lock
import org.ysb33r.gradle.iac.base.tf.config.multilevel.StateOptionsConcurrency
import org.ysb33r.gradle.iac.base.tf.config.multilevel.StateOptionsFull
import org.ysb33r.gradle.terraform.TerraformSourceSet
import org.ysb33r.gradle.terraform.extensions.TerraformExtension
import org.ysb33r.gradle.terraform.tasks.TerraformApply
import org.ysb33r.gradle.terraform.tasks.TerraformCleanupWorkspaces
import org.ysb33r.gradle.terraform.tasks.TerraformDestroy
import org.ysb33r.gradle.terraform.tasks.TerraformDestroyPlan
import org.ysb33r.gradle.terraform.tasks.TerraformFmtApply
import org.ysb33r.gradle.terraform.tasks.TerraformFmtCheck
import org.ysb33r.gradle.terraform.tasks.TerraformImport
import org.ysb33r.gradle.terraform.tasks.TerraformInit
import org.ysb33r.gradle.terraform.tasks.TerraformOutput
import org.ysb33r.gradle.terraform.tasks.TerraformOutputJson
import org.ysb33r.gradle.terraform.tasks.TerraformPlan
import org.ysb33r.gradle.terraform.tasks.TerraformProvidersLock
import org.ysb33r.gradle.terraform.tasks.TerraformProvidersSchema
import org.ysb33r.gradle.terraform.tasks.TerraformProvidersShow
import org.ysb33r.gradle.terraform.tasks.TerraformShowState
import org.ysb33r.gradle.terraform.tasks.TerraformStateMv
import org.ysb33r.gradle.terraform.tasks.TerraformStatePull
import org.ysb33r.gradle.terraform.tasks.TerraformStatePush
import org.ysb33r.gradle.terraform.tasks.TerraformStateRm
import org.ysb33r.gradle.terraform.tasks.TerraformTaint
import org.ysb33r.gradle.terraform.tasks.TerraformUntaint
import org.ysb33r.gradle.terraform.tasks.TerraformValidate

import static org.ysb33r.gradle.iac.base.internal.tf.InitRelationship.DEPENDS_ON
import static org.ysb33r.gradle.iac.base.internal.tf.InitRelationship.MUST_RUN_AFTER
import static org.ysb33r.gradle.iac.base.internal.tf.InitRelationship.NONE

/**
 * Maps Terraform tasks to conventions.
 *
 * @author Schalk W. Cronjé
 */
@CompileStatic
@SuppressWarnings(['LineLength', 'DuplicateListLiteral'])
enum TerraformTasks {
    INIT(
        1, 'init', TerraformInit, 'Initialises Terraform',
        [],
        NONE, true
    ),
    IMPORT(
        2, 'import', TerraformImport, 'Imports a resource',
        [Lock, StateOptionsConcurrency],
        MUST_RUN_AFTER, false, ['STATE_RM', 'STATE_PUSH']
    ),
    SHOW(
        3, 'showState', TerraformShowState, 'Generates a report on the current state',
        [],
        MUST_RUN_AFTER, false, ['APPLY', 'DESTROY', 'STATE_RM', 'IMPORT', 'STATE_MV', 'STATE_PUSH', 'TAINT', 'UNTAINT']
    ),
    OUTPUT(
        4, 'output', TerraformOutput, 'Generates a file of output variables',
        [],
        MUST_RUN_AFTER, false, ['APPLY', 'DESTROY']
    ),
    PLAN(
        10, 'plan', TerraformPlan, 'Generates Terraform execution plan',
        [Lock, StateOptionsFull],
        DEPENDS_ON, false, ['STATE_MV', 'STATE_RM', 'IMPORT', 'TAINT', 'UNTAINT', 'CLEANUP_WORKSPACES', 'STATE_PUSH']
    ),
    APPLY(
        11, 'apply', TerraformApply, 'Builds or changes infrastructure',
        [Lock, StateOptionsFull],
        DEPENDS_ON, false, ['STATE_MV', 'STATE_RM', 'IMPORT', 'TAINT', 'UNTAINT', 'CLEANUP_WORKSPACES', 'STATE_PUSH'], PLAN
    ),
    OUTPUT_JSON(
        12, 'cacheOutputVariables', TerraformOutputJson, 'Caches the output variables',
        [],
        MUST_RUN_AFTER, false, ['DESTROY']
    ),
    DESTROY_PLAN(
        14, 'destroyPlan', TerraformDestroyPlan, 'Generates Terraform destruction plan',
        [Lock, StateOptionsFull],
        DEPENDS_ON, false,
    ),
    DESTROY(
        15, 'destroy', TerraformDestroy, 'Destroys infrastructure',
        [Lock, StateOptionsFull],
        DEPENDS_ON, false, ['STATE_MV', 'STATE_RM', 'IMPORT', 'TAINT', 'UNTAINT', 'STATE_PUSH'], DESTROY_PLAN
    ),
    VALIDATE(
        20, 'validate', TerraformValidate, 'Validates the Terraform configuration',
        [],
        NONE
    ),
    STATE_MV(
        30, 'stateMv', TerraformStateMv, 'Moves a resource from one area to another',
        [Lock],
        MUST_RUN_AFTER, false, ['STATE_PUSH', 'IMPORT', 'STATE_RM']
    ),
    STATE_PUSH(
        31, 'statePush', TerraformStatePush, 'Pushes local state file to remote',
        [Lock]
    ),
    STATE_PULL(
        32, 'statePull', TerraformStatePull, 'Pulls remote state local to local file',
        []
    ),
    STATE_RM(
        33, 'stateRm', TerraformStateRm, 'Removes a resource from state',
        [Lock],
        MUST_RUN_AFTER, false, ['STATE_PUSH']
    ),
    TAINT(
        34, 'taint', TerraformTaint, 'Taint the status of a resource',
        [Lock],
        MUST_RUN_AFTER, false, ['STATE_PUSH']
    ),
    UNTAINT(
        35, 'untaint', TerraformUntaint, 'Remove tainted status from resource',
        [Lock],
        MUST_RUN_AFTER, false, ['STATE_PUSH']
    ),
    FMT_CHECK(
        50,
        'formatCheck',
        TerraformFmtCheck,
        'Checks whether files are correctly formatted',
        [CodeFormatting],
        NONE, true
    ),
    FMT_APPLY(
        51, 'formatApply', TerraformFmtApply, 'Formats source files in source set',
        [CodeFormatting],
        NONE, true
    ),
    CLEANUP_WORKSPACES(
        60, 'cleanupWorkspaces', TerraformCleanupWorkspaces, 'Deletes any dangling workspaces',
        [],
        NONE, true
    ),
    PROVIDER(
        70, 'providersShow', TerraformProvidersShow, 'Show provider information',
        [],
        MUST_RUN_AFTER, true
    ),
    PROVIDER_LOCK(
        71, 'providersLock', TerraformProvidersLock, 'Lock provider package versions',
        [],
        MUST_RUN_AFTER, true
    ),
    PROVIDER_SCHEMA(
        72, 'providersSchema', TerraformProvidersSchema, 'Write detailed provider schemas to a file',
        [],
        MUST_RUN_AFTER, true
    )

    static List<TerraformTasks> ordered() {
        TerraformTasks.values().sort { a, b -> a.order <=> b.order } as List
    }

    /**
     * Find instance by command name
     *
     * @param cmd Command
     * @return Task metadata
     * @throw {@link IllegalArgumentException} is no match
     */
    static TerraformTasks byCommand(String cmd) {
        def task = values().find { cmd == it.command }
        if (!task) {
            throw new IllegalArgumentException("${cmd} is not a valid command alias for a Terraform task")
        }
        task
    }

    final int order
    final String command
    final Class type
    final String description
    final List<Class<? extends ExecutionConfiguration>> executionConfigurations
    final InitRelationship initRelationship
    final boolean workspaceAgnostic
    final TerraformTasks dependsOnProvider
    final List<String> mustRunAfter

    void configure(TerraformSourceSet sourceSet, String taskName, String workspaceName, Project project) {
        TerraformModel.invokeMethod(
            "configure${command.capitalize()}",
            [new TaskConfiguration(
                sourceSet,
                taskName,
                workspaceName,
                project.extensions.getByType(TerraformExtension),
                project,
                executionConfigurations
            )] as Object[]
        )
    }

    @SuppressWarnings('ParameterCount')
    private TerraformTasks(
        int order,
        String name,
        Class type,
        String description,
        Iterable<Class<? extends ExecutionConfiguration>> configurations,
        InitRelationship init = InitRelationship.MUST_RUN_AFTER,
        boolean workspaceAgnostic = false,
        Iterable<String> mustRunAfter = [],
        TerraformTasks dependsOn = null
    ) {
        this.order = order
        this.command = name
        this.type = type
        this.description = description
        this.workspaceAgnostic = workspaceAgnostic
        this.dependsOnProvider = dependsOn
        this.executionConfigurations = configurations.toList().asImmutable()
        this.initRelationship = init
        this.mustRunAfter = mustRunAfter.toList().asImmutable()
    }
}