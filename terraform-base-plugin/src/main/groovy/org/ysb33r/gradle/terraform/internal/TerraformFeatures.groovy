/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.terraform.internal

import groovy.transform.CompileStatic
import org.ysb33r.gradle.iac.base.tf.ToolFeatures
import org.ysb33r.grolifant5.api.core.Version

/**
 * Easy way to determine which features a version of Terraform will have.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class TerraformFeatures implements ToolFeatures {
    TerraformFeatures(String ver) {
        this.version = Version.of(ver)
    }

    /**
     * {@code plan} supports {-detailed-exitcode}
     *
     * @return {@code true} for feature.
     */
    @Override
    boolean planHasDetailedExitCode() {
        version >= VER_1_0
    }

    /**
     * {@code plan} supports {-generate-config-out}
     *
     * @return {@code true} for feature.
     */
    @Override
    boolean planHasGenerateConfigOut() {
        version >= VER_1_10
    }

    private final Version version

    private static final Version VER_1_0 = Version.of('1.0.0')
    private static final Version VER_1_10 = Version.of('1.10.0')
}
