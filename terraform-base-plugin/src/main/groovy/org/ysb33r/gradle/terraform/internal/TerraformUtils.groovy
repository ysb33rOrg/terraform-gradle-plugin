/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.terraform.internal

import groovy.transform.CompileStatic
import org.ysb33r.grolifant5.api.core.OperatingSystem

import static org.ysb33r.grolifant5.api.core.OperatingSystem.Arch.ARM64
import static org.ysb33r.grolifant5.api.core.OperatingSystem.Arch.X86
import static org.ysb33r.grolifant5.api.core.OperatingSystem.Arch.X86_64

/** General utilities for Terraform.
 *
 * @since 0.2
 */
@CompileStatic
class TerraformUtils {
    public static final OperatingSystem OS = OperatingSystem.current()
    public static final OperatingSystem.Arch ARCH = OS.arch

    static String getExeName() {
        OS.windows ? 'terraform.exe' : 'terraform'
    }

    static String osArch() {
        String variant
        String osname
        if (OS.windows) {
            osname = 'windows'
            variant = (OS.arch == X86) ? '386' : 'amd64'
        } else if (OS.linux) {
            osname = 'linux'
            switch (ARCH) {
                case X86_64:
                    variant = VARIANT_64BIT
                    break
                case X86:
                    variant = VARIANT_32BIT
                    break
                case ARM64:
                    variant = VARIANT_ARM64
                    break
            }
        } else if (OS.macOsX) {
            osname = 'darwin'
            switch (ARCH) {
                case X86_64:
                    variant = VARIANT_64BIT
                    break
                case ARM64:
                    variant = VARIANT_ARM64
                    break
            }
        } else if (OS.solaris) {
            osname = 'solaris'
            variant = VARIANT_64BIT
        } else if (OS.freeBSD) {
            osname = 'freebsd'
            switch (ARCH) {
                case X86_64:
                    variant = VARIANT_64BIT
                    break
                case X86:
                    variant = VARIANT_32BIT
                    break
            }
        }
        variant ? "${osname}_${variant}" : null
    }

//    /** Converts a file path to a format suitable for interpretation by Terraform on the appropriate
//     * platform.
//     *
//     * @param ccso Project context.
//     * @param file Object that can be converted using {@code project.file}.
//     * @return String version adapted on a per-platform basis
//     */
//    static String terraformPath(ConfigCacheSafeOperations ccso, Object file) {
//        String path = ccso.fsOperations().file(file).absolutePath
//        OperatingSystem.current().windows ? path.replaceAll(~/\x5C/, FORWARD_SLASH) : path
//    }
//
//    /** Obtain the required terraform execution environmental variables
//     *
//     * @param terraformrc {@link TerraformRCExtension}.
//     * @param name Name of the task
//     * @param dataDir Data directory provider
//     * @param logDir Log directory provider
//     * @param logLevel Level of logging. Can be {@code null}.
//     * @return Map of environmental variables
//     *
//     * @since 0.10.0
//     */
//    static Map terraformEnvironment(
//        TerraformRCExtension terraformrc,
//        String name,
//        Provider<File> dataDir,
//        Provider<File> logDir,
//        String logLevel
//    ) {
//        [
//            TF_DATA_DIR         : dataDir.get().absolutePath,
//            TF_CLI_CONFIG_FILE  : TerraformConfigUtils.locateTerraformConfigFile(terraformrc).absolutePath,
//            TF_LOG_PATH         : terraformLogFile(name, logDir).absolutePath,
//            TF_LOG              : logLevel ?: '',
//            TF_APPEND_USER_AGENT: "terraform-gradle-plugin/${PLUGIN_VERSION}"
//        ]
//    }
//
//    /**
//     * Resolves the location of the log file.
//     *
//     * @param name Task name
//     * @param logDir Log dir provider
//     * @return Location of log file
//     *
//     * @since 0.11
//     */
//    static File terraformLogFile(String name, Provider<File> logDir) {
//        new File(logDir.get(), "${name}.log").absoluteFile
//    }

//
//    /**
//     * Escape HCL variables in a form suitable for using in a variables or backend configuration file.
//     *
//     * @param vars Variables map to escape
//     * @param escapeInnerLevel Whether inner level string variables should be escaped.
//     * @return Escaped map.
//     *
//     * @sinec 0.13
//     */
//    static Map<String, String> escapeHclVars(Map<String, Object> vars, boolean escapeInnerLevel) {
//        Map<String, String> hclMap = [:]
//        for (String key in vars.keySet()) {
//            hclMap[key] = escapeOneItem(vars[key], escapeInnerLevel)
//        }
//        hclMap
//    }

//    /**
//     * Takes a list and creates a HCL-list with appropriate escaping.
//     *
//     * @param items List items
//     * @return Escaped string
//     *
//     * @since 0.12
//     */
//    static String escapedList(Iterable<Object> items, boolean escapeInnerLevel) {
//        String joinedList = Transform.toList(items as Collection) { Object it ->
//            escapeOneItem(it, escapeInnerLevel)
//        }.join(COMMA_SEPARATED)
//        "[${joinedList}]"
//    }

//    /**
//     * Takes a map and creates a HCL-map with appropriate escaping.
//     *
//     * @param items Map items
//     * @return Escaped string
//     *
//     * @since 0.12
//     */
//    static String escapedMap(Map<String, ?> items, boolean escapeInnerLevel) {
//        String joinedMap = Transform.toList(items) { Map.Entry<String, ?> entry ->
//            "\"${entry.key}\" = ${escapeOneItem(entry.value, escapeInnerLevel)}".toString()
//        }.join(COMMA_SEPARATED)
//        "{${joinedMap}}".toString()
//    }

//    /**
//     * Escaped a single item.
//     *
//     * @param item Item to escape
//     * @param innerLevel Whether the escaped item is actually nested.
//     * @return Escaped item
//     *
//     * @since 0.12
//     */
//    static String escapeOneItem(Object item, boolean innerLevel) {
//        switch (item) {
//            case Provider:
//                return escapeOneItem(((Provider) item).get(), innerLevel)
//            case Map:
//                return escapedMap((Map) item, innerLevel)
//            case Iterable:
//                return escapedList((Iterable) item, innerLevel)
//            case Number:
//            case Boolean:
//                return StringUtils.stringize(item)
//            default:
//                return innerLevel ?
//                    "\"${escapeQuotesInString(StringUtils.stringize(item))}\"".toString() :
//                    escapeQuotesInString(StringUtils.stringize(item))
//        }
//    }

//    /**
//     * Escapes any Terraform string quotes.
//     *
//     * @param item String to escape
//     * @return Escape string.
//     */
//    static String escapeQuotesInString(String item) {
//        item.replaceAll(~/"/, '\\\\"')
//    }

//    /**
//     * Converts item to string if not null
//     *
//     * @param thingy Item that needs to be converted to a string
//     * @return Stringized item or {@code null}.
//     *
//     * @since 1.0
//     */
//    static String stringizeOrNull(Object thingy) {
//        thingy != null ? StringUtils.stringize(thingy) : null
//    }

//    private static final String FORWARD_SLASH = '/'
//    private static final String COMMA_SEPARATED = ', '

    private final static String VARIANT_32BIT = '386'
    private final static String VARIANT_64BIT = 'amd64'
    private final static String VARIANT_ARM64 = 'arm64'
}
