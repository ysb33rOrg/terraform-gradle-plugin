/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.terraform.plugins

import groovy.transform.CompileStatic
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.language.base.plugins.LifecycleBasePlugin
import org.ysb33r.gradle.iac.base.IacBasePlugin
import org.ysb33r.gradle.iac.base.internal.tf.TaskUtils
import org.ysb33r.gradle.terraform.extensions.TerraformExtension
import org.ysb33r.gradle.terraform.internal.TerraformModel

/**
 * Standard plugin for OpenTofu
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class TerraformBasePlugin implements Plugin<Project> {
    public static final String VERSION_RESOURCE_PATH = 'terraform-gradle-templates/terraform.properties'
    public static final String STATE_FILE_NAME = 'terraform.tfstate'

    @Override
    void apply(Project project) {
        project.pluginManager.tap {
            apply(LifecycleBasePlugin)
            apply(IacBasePlugin)
            apply(TerraformRCPlugin)
        }
        final terraform = project.extensions.create(TerraformExtension.NAME, TerraformExtension, project)
        terraform.sourceSets.create(TerraformModel.DEFAULT_SOURCESET_NAME)

        TaskUtils.registerPluginCacheDirTask(
            TerraformModel.pluginCacheDirTaskName(),
            TerraformModel.TASK_GROUP,
            terraform.pluginCacheDir,
            project
        )
    }
}
