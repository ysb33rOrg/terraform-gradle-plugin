/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.terraform.backends

import groovy.transform.CompileStatic
import org.gradle.api.Project
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.ysb33r.gradle.terraform.extensions.TerraformExtension

import javax.inject.Inject

/**
 * Supports remote state storage in Gitlab.
 *
 * @since 2.0
 *
 * @author Schalk W. Cronjé
 */
@CompileStatic
class GitlabBackend extends TerraformBackend {

    @Inject
    GitlabBackend(String name, TerraformExtension parent, Project tempProjectRef) {
        super(name, parent, tempProjectRef)
        this.address = tempProjectRef.objects.property(URI)
        this.lockAddress = this.address.map { (it.toString() + '/lock').toURI() }

        token('address', this.address)
        token('lock_address', this.lockAddress)
        token('unlock_address', this.lockAddress)
        token('lock_method', 'POST')
        token('unlock_method', 'DELETE')
    }

    /**
     * Sets the Gitlab address for storing state.
     *
     * @param address Anything convertible to URI and in the form of
     * {@code https://<GITLAB_HOST>/api/v4/projects/<PROJECT_ID>/terraform/state/<STATE-NAME>}
     */
    void setAddress(Object address) {
        this.address.set(ccso.stringTools().provideUri(address))
    }

    /**
     * Sets the username.
     *
     * <p>This is evaluated immediately and encrypted in memory.</p>
     *
     * @param tok User name.
     *
     */
    void setUsername(Object tok) {
        secretVariable('TF_USERNAME', tok)
    }

    /**
     * Set authentication token as a secret.
     *
     * <p>This is evaluated immediately and encrypted in memory.</p>
     *
     * @param tok Auth token.
     */
    void setAccessToken(Object tok) {
        secretVariable('TF_PASSWORD', tok)
    }

    /**
     * The number of HTTP request retries.
     *
     * @param value Number of retries.
     */
    void setRetryMax(Integer value) {
        token('retry_max', value)
    }

    /**
     * The minimum time in seconds to wait between HTTP request attempts.
     *
     * @param value Time in seconds.
     */
    void setRetryWaitMin(Integer value) {
        token('retry_wait_min', value)
    }

    /**
     * The maximum time in seconds to wait between HTTP request attempts.
     *
     * @param value Time in seconds.
     */
    void setRetryWaitMax(Integer value) {
        token('retry_wait_max', value)
    }

    private final Property<URI> address
    private final Provider<URI> lockAddress
}
