/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.terraform.backends

import groovy.transform.CompileStatic
import org.gradle.api.Project
import org.ysb33r.gradle.terraform.extensions.TerraformExtension

import javax.inject.Inject

/**
 * Support the legacy Terraform Cloud backend a.k.a {@code remote}.
 *
 * @since 0.13 (but reworked for 2.0)
 *
 * @author Schalk W. Cronjé
 */
@CompileStatic
class TerraformRemoteBackend extends TerraformBackend {

    @Inject
    TerraformRemoteBackend(String name, TerraformExtension parent, Project tempProjectRef) {
        super(name, parent, tempProjectRef)
    }

    /** Sets a hostname if other than {@code app.terraform.io}.
     *
     * @param hostname
     */
    void setHostname(Object hostname) {
        token('hostname', hostname)
    }

    /**
     * Set organization name.
     *
     * @param org Organization name.
     */
    void setOrganization(Object org) {
        token('organization', org)
    }

    /**
     * Set authentication token as a secret.
     *
     * As an alternative the {@link org.ysb33r.gradle.terraform.extensions.TerraformRCExtension#credentials} method
     * can be used to set credentials in the configuration file.
     *
     * @param tok Auth token.
     */
    void setAuthToken(Object tok) {
        secretVariable('TF_TOKEN_app_terraform_io', tok)
    }

    /**
     * The full name of one remote workspace.
     *
     * Overrides {@link #setWorkspacePrefix}.
     *
     * @param wsName Workspace name.
     */
    void setWorkspaceName(Object wsName) {
        token(WORKSPACES, [name: wsName])
    }

    /**
     * A prefix used in the names of one or more remote workspaces, all of which can be used with this configuration.
     *
     * Overrides {@link #setWorkspaceName}.
     *
     * @param prefix Workspace prefix
     */
    void setWorkspacePrefix(Object prefix) {
        token(WORKSPACES, [prefix: prefix])
    }

    private static final String WORKSPACES = 'workspaces'
}
