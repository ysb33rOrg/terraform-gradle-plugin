/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.iac.base.tf

import groovy.json.JsonSlurper
import groovy.transform.CompileStatic
import org.gradle.api.Action
import org.gradle.api.Named
import org.gradle.api.NamedDomainObjectContainer
import org.gradle.api.Project
import org.gradle.api.file.FileTree
import org.gradle.api.file.FileTreeElement
import org.gradle.api.model.ObjectFactory
import org.gradle.api.provider.ListProperty
import org.gradle.api.provider.MapProperty
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.gradle.api.specs.Spec
import org.gradle.api.tasks.TaskProvider
import org.gradle.api.tasks.util.PatternFilterable
import org.gradle.api.tasks.util.PatternSet
import org.ysb33r.gradle.iac.base.internal.tf.DefaultSecretVariables
import org.ysb33r.gradle.iac.base.tf.backends.AbstractIacBackend
import org.ysb33r.gradle.iac.base.tf.backends.TokenValue
import org.ysb33r.gradle.iac.base.tf.config.multilevel.ExecutionConfiguration
import org.ysb33r.gradle.iac.base.tf.config.multilevel.ExecutionOptions
import org.ysb33r.gradle.iac.base.tf.config.multilevel.ResolvedExecutionsOptions
import org.ysb33r.gradle.iac.base.tf.config.multilevel.Variables
import org.ysb33r.gradle.iac.base.tf.config.multilevel.VariablesSpec
import org.ysb33r.gradle.iac.base.tf.tasks.AbstractOutputJsonTask
import org.ysb33r.grolifant5.api.core.ClosureUtils
import org.ysb33r.grolifant5.api.core.ConfigCacheSafeOperations
import org.ysb33r.grolifant5.api.core.ProjectOperations
import org.ysb33r.grolifant5.api.core.SimpleSecureString
import org.ysb33r.grolifant5.api.core.TaskTools

import java.util.concurrent.ConcurrentHashMap

import static org.ysb33r.gradle.iac.base.internal.tf.TaskUtils.VARIABLE_CACHE_FILE_NAME

/**
 * A base class for OpenTofu & Terraform source sets.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
@SuppressWarnings(['MethodCount'])
abstract class SourceSetBase implements Named, PatternFilterable, SecretVariables,
    SourceSetSources, SourceSetExecutionEnvironment {

    /**
     * Source set name
     */
    final String name

    /**
     * Source tree
     */
    final FileTree asFileTree

    /**
     * Provide for the root directory of the source set,
     *
     * @return File provider.
     */
    @Override
    Provider<File> getSrcDir() {
        this.sourceDir
    }

    /**
     * Sets the source directory.
     *
     * @param dir Directory can be anything convertible using {@link org.gradle.api.Project#file}.
     * @return {@code this}.
     */
    void setSrcDir(Object dir) {
        ccso.fsOperations().updateFileProperty(this.sourceDir, dir)
    }

    /** Data directory provider.
     *
     * @return File provider.
     */
    @Override
    Provider<File> getDataDir() {
        this.dataDir
    }

    /** Sets the Terraform data directory.
     *
     * @param dir Directory can be anything convertible using {@link org.gradle.api.Project#file}.
     * @return {@code this}.
     */
    void setDataDir(Object dir) {
        ccso.fsOperations().updateFileProperty(this.dataDir, dir)
    }

    /** Log directory provider.
     *
     * @return File provider.
     */
    @Override
    Provider<File> getLogDir() {
        this.logDir
    }

    /** Sets the log directory.
     *
     * @param dir Directory can be anything convertible using {@link org.gradle.api.Project#file}.
     * @return {@code this}.
     */
    void setLogDir(Object dir) {
        ccso.fsOperations().updateFileProperty(this.logDir, dir)
    }

    /**
     * Reports directory.
     *
     * @return File provider.
     */
    @Override
    Provider<File> getReportsDir() {
        this.reportsDir
    }

    /**
     * The file where DSL-defined variables are cached to.
     *
     * @return Location provider
     */
    Provider<File> getVariablesFile() {
        this.variablesFile
    }

    /**
     * The file defined by {@Link #getVariablesFile} as well as any files declared in the {@link VariablesSpec}
     *
     * @return Location of all variables files.
     */
    Provider<List<File>> getAllVariablesFiles() {
        this.allVariablesFiles
    }

    /**
     * All declared variables correctly escaped.
     *
     * @return Provider to escaped variables.
     */
    Provider<List<String>> getEscapedVariables() {
        this.vars.tfVars
    }

    /**
     * Sets variables that are applicable to this source set.
     *
     * @param cfg Closure that configures a {@link VariablesSpec}.
     *
     */
    void variables(@DelegatesTo(VariablesSpec) Closure cfg) {
        ClosureUtils.configureItem(this.vars, cfg)
    }

    /**
     * Sets variables that are applicable to this source set.
     *
     * @param cfg Configurating action.
     *
     */
    void variables(Action<VariablesSpec> cfg) {
        cfg.execute(this.vars)
    }

    /**
     * Get all variables applicable to this source set.
     *
     * @param cfg
     *
     */
    VariablesSpec getVariables() {
        this.vars
    }

    /**
     * Sets the reports directory.
     *
     * @param dir Directory can be anything convertible using {@link org.gradle.api.Project#file}.
     * @return {@code this}.
     */
    void setReportsDir(Object dir) {
        ccso.fsOperations().updateFileProperty(this.reportsDir, dir)
    }

    /**
     * Location where backend directory is located.
     *
     * @return Directory location.
     */
    Provider<File> getBackendConfigurationDir() {
        this.backendDir
    }

    /**
     * Additional sources that affects infrastructure.
     *
     * @param files Anything convertible to a file.
     */
    void secondarySources(Object... files) {
        final tempRef = this.secondarySources
        files.each {
            tempRef.add(ccso.fsOperations().provideFile(it))
        }
    }

    /**
     * Additional sources that affects infrastructure.
     *
     * @param files Anything convertible to a file.
     */
    void secondarySources(Iterable<Object> files) {
        final tempRef = this.secondarySources
        files.each {
            tempRef.add(ccso.fsOperations().provideFile(it))
        }
    }

    /**
     * Provides a list of secondary sources.
     *
     * @return Provider never returns null, but could return an empty list.
     */
    @Override
    Provider<List<File>> getSecondarySources() {
        this.secondarySources
    }

    @Override
    PatternFilterable exclude(Closure closure) {
        patternSet.exclude(closure)
    }

    @Override
    PatternFilterable exclude(Spec<FileTreeElement> spec) {
        patternSet.exclude(spec)
    }

    @Override
    PatternFilterable exclude(String... strings) {
        patternSet.exclude(strings)
    }

    @Override
    PatternFilterable exclude(Iterable<String> iterable) {
        patternSet.exclude(iterable)
    }

    @Override
    Set<String> getIncludes() {
        patternSet.includes
    }

    @Override
    Set<String> getExcludes() {
        patternSet.excludes
    }

    @Override
    PatternFilterable setIncludes(Iterable<String> iterable) {
        patternSet.includes = iterable
        this
    }

    @Override
    PatternFilterable setExcludes(Iterable<String> iterable) {
        patternSet.excludes = iterable
        this
    }

    @Override
    PatternFilterable include(String... strings) {
        patternSet.include(strings)
    }

    @Override
    PatternFilterable include(Iterable<String> iterable) {
        patternSet.include(iterable)
    }

    @Override
    PatternFilterable include(Spec<FileTreeElement> spec) {
        patternSet.include(spec)
    }

    @Override
    PatternFilterable include(Closure closure) {
        patternSet.include(closure)
    }

    void environment(Map<String, ?> envVars) {
        this.env.putAll(ccso.stringTools().provideValues(envVars))
    }

    void environment(String key, Object value) {
        this.env.put(key, ccso.stringTools().provideString(value))
    }

    /**
     * Adds a provider to environment variables.
     *
     * @param envProvider Provider to a resolved map.
     */
    void addEnvironmentProvider(Provider<Map<String, String>> envProvider) {
        this.env.putAll(envProvider)
    }

    /**
     * Provider to the full running environment.
     *
     * @return Provider to the environment map.
     */
    @Override
    Provider<Map<String, String>> getEnvironment() {
        this.env
    }

    /**
     * The execution options configured on the toolchain.
     *
     * @return Execution options.
     */
    @Override
    Provider<ResolvedExecutionsOptions> executionOptionsFor(Iterable<Class<? extends ExecutionConfiguration>> types) {
        executionOptions.getResolvedOptionsFor(types)
    }

    ExecutionOptions getExecutionOptions() {
        this.executionOptions
    }

    void executionOptions(Action<ExecutionOptions> configurator) {
        configurator.execute(this.executionOptions)
    }

    void executionOptions(@DelegatesTo(ExecutionOptions) Closure<?> configurator) {
        ClosureUtils.configureItem(this.executionOptions, configurator)
    }

    /**
     * Adds one or more workspaces.
     *
     * @since 0.10.0
     */
    void workspaces(String... workspaceNames) {
        for (String ws : workspaceNames) {
            this.workspaces.create(ws)
            this.addRelationshipTasks(ws)
        }
    }

    /**
     * List of additional workspaces other than the the default.
     */
    final Provider<List<String>> workspaceNames

    /**
     * Flags whether workspaces have been defined.
     *
     * @return {@code true} is one or more workspaces have been defined
     */
    Provider<Boolean> hasWorkspaces() {
        this.hasWorkspacesProvider
    }

    /**
     * Indicate that if the certain tasks of this source set is in the task graph with equivalent tasks from the other
     * source sets, then these tasks must only run when those tasks have completed.
     *
     * <p>
     *     This includes commands like `apply` and `plan` and is intended for cases where infrastructure from the one
     *     source set must be in place before the other source set should be used.
     * </p>
     *
     * @param otherSourceSet Names of other source sets
     */
    void mustRunAfter(String... otherSourceSets) {
        otherSourceSets.each { name ->
            final list = sourceSetContainer.named(name).flatMap { SourceSetBase ss -> ss.sourceSetRelationshipTasks }
            getRelationshipTaskNames(defaultWorkspaceName).each {
                taskTools.whenNamed(it) { t ->
                    t.mustRunAfter(list)
                }
            }
            workspaces.whenObjectAdded { Workspace ws ->
                getRelationshipTaskNames(ws.name).each {
                    taskTools.whenNamed(it) { t ->
                        t.mustRunAfter(list)
                    }
                }
            }
        }
    }

    /**
     * The important tasks that must be sequenced when there are relationships between source sets.
     *
     * @return List of task objects. This is anything that can be passed to {@link org.gradle.api.Task#dependsOn} or
     * {@link org.gradle.api.Task#mustRunAfter}
     */
    Provider<List<Object>> getSourceSetRelationshipTasks() {
        this.sourceSetRelationshipTaskObjects
    }

    // this.sourceSetRelationshipTasks = project.object.listProperty(Object)
    /**
     * The tokens associated with the backend for this sourceset.
     *
     * @return Map provider.
     */
    Provider<Map<String, TokenValue>> getBackendTokenProvider() {
        this.backendTokens
    }

    /**
     * Returns a provider which can be used to access output variables from the default source set.
     *
     * Invoking the provider return by this call will invoke state output command the first time, but thereafter
     * values will be cached for the remainder of the build. If you want updates values, ensure the relevent
     * infrastructure apply task is run before invoking the provider.
     *
     * @return Provider for reading output values. THe values are returned in a raw format which basically is just the
     *   JSON output parsed into some form of map.
     */
    Provider<Map<String, Object>> rawOutputVariables() {
        rawOutputVariables(defaultWorkspaceName)
    }

    /**
     * Returns a provider which can be used to access output variables from a source set.
     *
     * Invoking the provider return by this call will invoke state output command the first time, but thereafter
     * values will be cached for the remainder of the build. If you want updates values, ensure the relevent
     * infrastructure apply task is run before invoking the provider.
     *
     * @param workspaceName which workspace this is for. Defaults to {@code default} workspace if not supplied.
     * @return Provider for reading output values. The values are returned in a raw format which basically is just the
     *   JSON output parsed into some form of map.
     */
    Provider<Map<String, Object>> rawOutputVariables(String workspaceName) {
        final output = getOutputTask(workspaceName)

        outputVariablesProviderMap.computeIfAbsent(workspaceName) {
            final parser = output.map {
                final json = new JsonSlurper()
                json.parse(it.statusReportOutputFile.get().asFile) as Map<String, ?>
            }
            final props = objectFactory.mapProperty(String, Object)
            props.set(parser)
            props.finalizeValueOnRead()
            props.disallowUnsafeRead()

            props
        }
    }

    /**
     * Returns a provider to a specific output variable in the default workspace.
     *
     * Invoking the provider return by this call will invoke state output command the first time, but thereafter
     * values will be cached for the remainder of the build. If you want updates values, ensure the relevant
     * infrastructure apply task is run before invoking the provider.
     *
     * @param varName Name of specific variable.
     * @return Provider for value of specific output variable. If the variable is not a primitive type, it is up to
     * the caller to do further processing,
     */
    Provider<Object> rawOutputVariable(String varName) {
        rawOutputVariable(varName, defaultWorkspaceName)
    }

    /**
     * Returns a provider to a specific output variable.
     *
     * Invoking the provider return by this call will invoke state output command the first time, but thereafter
     * values will be cached for the remainder of the build. If you want updates values, ensure the relevent
     * infrastructure apply task is run before invoking the provider.
     *
     * @param varName Name of specific variable.
     * @param workspaceName which workspace this is for. Defaults to {@code default} workspace if not supplied.
     * @return Provider for value of specific output variable. If the variable is not a primitive type, it is up to
     * the caller to do further processing,
     */
    Provider<Object> rawOutputVariable(String varName, String workspaceName) {
        rawOutputVariables(workspaceName).map {
            it[varName] ? it[varName]['value'] : null
        }
    }

    /**
     * Returns a provider to a set of secret variables.
     *
     * @return A provider to a map of secret property strings.
     */
    @Override
    Provider<Map<String, SimpleSecureString>> getSecretVariables() {
        this.allSecretVars
    }

    /**
     * Cache directory for plugins / providers.
     *
     * @return Location of plugin directory.
     */
    abstract Provider<File> getPluginDir()

    /**
     * Local timeout for the cache directory for plugins / providers.
     *
     * @return Timeout in milliseconds. If less than 1, then no lock will be performed
     */
    abstract Provider<Integer> getPluginDirTimeout()

    /**
     * Use the plugin cache directory that is configured in the global configuration of the root project.
     *
     */
    abstract void useConfiguredPluginCache()

    /**
     * Use a custom plugin cache directory for this source set.
     *
     * <p>
     *     The plugin dir will be place underneath the project cache directory and is guaranteed to have a
     *     unique name with the context of the whole project hierarchy.
     * </p>
     *
     */
    void useCustomPluginCache() {
        useThisCustomCacheDir(
            proposeCustomCacheDir(),
            ccso.providerTools().provider { -> 0 }
        )
    }

    /**
     * Use a custom plugin cache by specifying both a directory and a lock timeout.
     *
     * @param dir Directory where cache should be located.
     *   Anything convertible to a file.
     *
     * @param timeout Lock timeout in milliseconds.
     *   If this directory to the source set, pass zero to avoid locking mechanisms.
     *   However, if this directory is still shared, provide a timeout for safety purposes.
     */
    void useCustomPluginCache(Object dir, Integer timeout) {
        useThisCustomCacheDir(
            ccso.fsOperations().provideFile(dir),
            ccso.providerTools().provider { -> timeout }
        )
    }
    /**
     * Looks up a backend by name and then set this sourceset to use that backend.
     *
     * @param name Name of backend.
     */
    abstract void useBackend(String name)

    /**
     * Which toolchain to use.
     * If not called, it will always used a default toolchain.
     *
     * @param name Name of toolchain.
     */
    abstract void useToolchain(String name)

    /**
     * Holds a list of tasks in this source set that needs to be recognised by other source sets, if they have
     * a dependency on this one.
     */
    protected final ListProperty<Object> sourceSetRelationshipTaskObjects

    /**
     * Access to the task container
     */
    protected final TaskTools taskTools

    protected SourceSetBase(
        String name,
        String defaultWorkspaceName,
        String prefix,
        Project tempProjectRef
    ) {
        this.ccso = ConfigCacheSafeOperations.from(tempProjectRef)
        this.name = name
        this.toolPrefix = prefix
        this.taskTools = ProjectOperations.find(tempProjectRef).tasks
        this.objectFactory = tempProjectRef.objects
        this.patternSet = new PatternSet()
        this.defaultWorkspaceName = defaultWorkspaceName
        this.sourceDir = ccso.providerTools().property(File)
        this.dataDir = ccso.providerTools().property(File)
        this.logDir = ccso.providerTools().property(File)
        this.reportsDir = ccso.providerTools().property(File)
        this.sourceSetRelationshipTaskObjects = ccso.providerTools().listProperty(Object)
        this.asFileTree = ccso.fsOperations().fileTree(sourceDir).matching(this.patternSet)
        this.secondarySources = ccso.providerTools().listProperty(File)
        this.workspaces = objectFactory.domainObjectContainer(Workspace)
        this.backend = ccso.providerTools().property(AbstractIacBackend)
        this.backendTokens = this.backend.flatMap { it.tokenProvider }
        this.vars = objectFactory.newInstance(Variables, this.sourceDir, this.backendTokens)
        this.secrets = objectFactory.newInstance(DefaultSecretVariables)
        this.env = ccso.providerTools().mapProperty(String, String)
        this.executionOptions = objectFactory.newInstance(ExecutionOptions)
        this.outputVariablesProviderMap = new ConcurrentHashMap<>()
        this.allSecretVars = this.backend.flatMap { it.secretVariables }
            .zip(this.secrets.secretVariables) { x, y -> x + y }
        this.variablesFile = this.dataDir.map {
            new File(it, VARIABLE_CACHE_FILE_NAME)
        }
        this.allVariablesFiles = this.variablesFile.zip(this.vars.declaredFiles) { one, more ->
            more + [one]
        }

        this.workspaceNames = ccso.providerTools().provider { ->
            workspaces*.name
        }

        final backendMidPiece = name == 'main' ? '' : name.capitalize()
        this.backendDir = ccso.fsOperations().buildDirDescendant(
            "${prefix}RemoteState/${prefix}${backendMidPiece}BackendConfiguration"
        )

        ccso.fsOperations().updateFileProperty(
            sourceDir,
            new File(ccso.fsOperations().projectDir, "src/tf/${name}")
        )

        ccso.fsOperations().updateFileProperty(
            dataDir,
            ccso.fsOperations().buildDirDescendant("tf/${name}")
        )

        ccso.fsOperations().updateFileProperty(
            logDir,
            ccso.fsOperations().buildDirDescendant("tf/${name}/logs")
        )

        ccso.fsOperations().updateFileProperty(
            reportsDir,
            ccso.fsOperations().buildDirDescendant("reports/tf/${name}")
        )

        this.patternSet.include('**/*.tf', '**/*.tfvars')
        this.patternSet.exclude('**/*.tfstate')
        this.workspaces.whenObjectAdded { Workspace ws -> createWorkspaceTasks(ws.name) }
        this.hasWorkspacesProvider = ccso.providerTools().provider { -> final chk = !workspaces.empty; chk }
    }

    protected final ConfigCacheSafeOperations ccso
    protected final NamedDomainObjectContainer<Workspace> workspaces
    protected final ObjectFactory objectFactory
    protected final String toolPrefix

    /**
     * The name of the output task that will generate JSON output variables
     * @param workspaceName Name of workspace.
     * @return Task that will generate output.
     */
    protected abstract TaskProvider<AbstractOutputJsonTask> getOutputTask(String workspaceName)

    /**
     * Invoked when a workspace is added.
     *
     * @param workspaceName Name of workspace,
     */
    protected abstract void createWorkspaceTasks(String workspaceName)

    /**
     * Names of tasks that are important in inter-source set relationships.
     *
     * @param ws Name of workspace.
     *
     * @return List of task names.
     */
    protected abstract List<String> getRelationshipTaskNames(String ws)

    /**
     * Get access to the container this source set belongs too.
     *
     * @return Container. Cannot be {@code null}.
     */
    protected abstract NamedDomainObjectContainer<? extends SourceSetBase> getSourceSetContainer()

    /**
     * Add task naming objects
     *
     * @param ws Workspace.
     */
    protected void addRelationshipTasks(String ws) {
        this.sourceSetRelationshipTaskObjects.addAll(getRelationshipTaskNames(ws))
    }

    /**
     * Use this custom cache directory.
     *
     * @param dir Location of cache.
     * @param timeout Timeout in milliseconds
     */
    protected abstract void useThisCustomCacheDir(Provider<File> dir, Provider<Integer> timeout)

    /**
     * Sets a reference to an existing backend.
     *
     * @param backend Backend instance.
     */
    protected void backendByInstance(AbstractIacBackend backend) {
        this.backend.set(backend)
    }

    /**
     * Sets a reference to an existing backend.
     *
     * @param backend Backend instance.
     */
    protected void backendByInstance(Provider<? extends AbstractIacBackend> backend) {
        this.backend.set(backend)
    }

    private Provider<File> proposeCustomCacheDir() {
        final path = "${ccso.projectTools().fullProjectPath}.${name}"
        ccso.fsOperations().provideProjectCacheDirDescendant(
            ".${toolPrefix}.d/${ccso.fsOperations().toSafeFileName(path)}"
        )
    }

    private final String defaultWorkspaceName
    private final ListProperty<File> secondarySources
    private final Property<File> sourceDir
    private final Property<File> dataDir
    private final Property<File> logDir
    private final Property<File> reportsDir
    private final Provider<File> backendDir
    private final Provider<File> variablesFile
    private final Provider<List<File>> allVariablesFiles
    private final PatternSet patternSet
    private final Variables vars
    private final ConcurrentHashMap<String, MapProperty<String, Object>> outputVariablesProviderMap
    private final ExecutionOptions executionOptions
    private final Property<AbstractIacBackend> backend
    private final Provider<Map<String, TokenValue>> backendTokens
    private final Provider<Map<String, SimpleSecureString>> allSecretVars
    private final MapProperty<String, String> env
    private final Provider<Boolean> hasWorkspacesProvider

    @Delegate(interfaces = true, excludes = ['getSecretVariables'])
    private final SecretVariables secrets
}
