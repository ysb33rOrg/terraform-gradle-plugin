/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.iac.base.tf.tasks

import groovy.transform.CompileStatic
import org.gradle.api.file.RegularFileProperty
import org.gradle.api.provider.Property
import org.gradle.api.tasks.Internal
import org.ysb33r.gradle.iac.base.tf.UsesSourceSetName
import org.ysb33r.gradle.iac.base.tf.execspec.DefaultArguments
import org.ysb33r.gradle.iac.base.tf.execspec.IacTfExecSpec

/**
 * Equivalent of {@code output -json}, but primarily used for when output variables need to provided to another task.
 *
 * @since 2.0
 */
@CompileStatic
class AbstractOutputJsonTask extends AbstractTfStandardTask implements UsesSourceSetName {

    /**
     * Sets the source set name
     * @param ssName Source set name.
     */
    @Override
    void setSourceSetName(String ssName) {
        this.sourceSetName.set(ssName)
    }

    /**
     * Get the location where the report file needs to be generated.
     *
     * @return File provider
     */
    @Internal
    final RegularFileProperty statusReportOutputFile

    protected AbstractOutputJsonTask(
        String workspaceName,
        String defaultWorkspaceName,
        Class<? extends IacTfExecSpec> execSpecClass
    ) {
        super(
            'output',
            workspaceName,
            execSpecClass,
            DefaultArguments.builder().supportsColor()
        )

        this.sourceSetName = project.objects.property(String)
        final String ws = workspaceName == defaultWorkspaceName ? '' : ".${workspaceName}"
        this.statusReportOutputFile = project.objects.fileProperty()

        this.statusReportOutputFile.convention(
            project.layout.file(
                sourceSetName.zip(dataDir) { baseName, dir ->
                    new File(dir, "${baseName}${ws}.output-variable-cache.json")
                }
            )
        )

        captureStdOutTo(statusReportOutputFile.map { it.asFile })

        execSpec.cmd {
            args '-json'
        }
    }

    private final Property<String> sourceSetName
}
