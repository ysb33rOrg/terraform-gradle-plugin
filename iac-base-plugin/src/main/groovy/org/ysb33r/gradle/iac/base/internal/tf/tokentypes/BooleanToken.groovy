/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.iac.base.internal.tf.tokentypes

import groovy.transform.CompileStatic
import org.ysb33r.gradle.iac.base.tf.backends.TokenValue

/**
 * Backend token that is a boolean.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class BooleanToken implements TokenValue {
    final String value

    BooleanToken(Boolean value) {
        this.value = value.toString()
    }

    @Override
    String toString() {
        value
    }
}
