/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.iac.base.hcl

import groovy.transform.CompileStatic
import org.gradle.api.provider.Provider
import org.ysb33r.grolifant5.api.core.ConfigCacheSafeOperations
import org.ysb33r.grolifant5.api.core.FileSystemOperations
import org.ysb33r.grolifant5.api.core.OperatingSystem
import org.ysb33r.grolifant5.api.core.ProviderTools
import org.ysb33r.grolifant5.api.core.StringTools
import org.ysb33r.grolifant5.api.core.Transform

import java.util.concurrent.Callable
import java.util.function.Function
import java.util.function.Supplier
import java.util.regex.Pattern
import java.util.stream.Collectors

/**
 * A transformer for various HCL entities
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class HclTransformer {

    HclTransformer(ConfigCacheSafeOperations ccso) {
        this.stringTools = ccso.stringTools()
        this.providerTools = ccso.providerTools()
        this.fsOperations = ccso.fsOperations()
    }

//    /**
//     * Escape HCL variables in a form suitable for using in a variables or backend configuration file.
//     *
//     * @param vars Variables map to escape
//     * @param escapeInnerLevel Whether inner level string variables should be escaped.
//     * @return Escaped map.
//     */
//    Provider<Map<String, String>> escapeHclVars(Provider<Map<String, ?>> vars, boolean escapeInnerLevel) {
//        final escapees = vars.map {
//            Map<String, Provider<String>> hclMap = [:]
//            for (String key in it.keySet()) {
//                hclMap[key] = escapeOneItem(vars[key], escapeInnerLevel)
//            }
//            hclMap
//        }
//
//        stringTools.provideValues(escapees as Map<String, ?>)
//    }

//    /**
//     * Escape HCL variables in a form suitable for using in a variables or backend configuration file.
//     *
//     * @param vars Variables map to escape
//     * @param escapeInnerLevel Whether inner level string variables should be escaped.
//     * @return Escaped map.
//     */
//    Provider<Map<String, String>> escapeHclVars(Map<String, Object> vars, boolean escapeInnerLevel) {
//        escapeHclVars(providerTools.provider { -> vars }, escapeInnerLevel)
//    }

    /**
     * Takes a list and creates a HCL-list with appropriate escaping.
     *
     * @param items List items
     * @return Escaped string
     */
    Provider<String> escapedList(Iterable<?> items, boolean escapeInnerLevel) {
        escapedList(providerTools.provider { -> items }, escapeInnerLevel)
    }

    /**
     * Takes a list and creates a HCL-list with appropriate escaping.
     *
     * @param items List items
     * @return Escaped string
     */
    Provider<String> escapedList(Provider<Iterable<?>> items, boolean escapeInnerLevel) {
        items.map { list ->
            String joinedList = Transform.toList(list as Collection<Object>) { Object it ->
                escapeOneItem(it, escapeInnerLevel)
            }.join(COMMA_SEPARATED)
            "[${joinedList}]".toString()
        }
    }

    /**
     * Takes a map and creates a HCL-map with appropriate escaping.
     *
     * @param items Map items of unescaped items.
     * @return Escaped string
     */
    Provider<String> escapedMap(Provider<Map<String, ?>> items, boolean escapeInnerLevel) {
        items.map {
            final joinedMap = toList(it) { Map.Entry<String, ?> entry ->
                "\"${entry.key}\" = ${escapeOneItem(entry.value, escapeInnerLevel)}".toString()
            }.join(COMMA_SEPARATED)
            "{${joinedMap}}".toString()
        }
    }

    /**
     * Takes a map and creates a HCL-map with appropriate escaping.
     *
     * @param items Map items
     * @return Escaped string
     */
    Provider<String> escapedMap(Map<String, ?> items, boolean escapeInnerLevel) {
        escapedMap(providerTools.provider { -> items }, escapeInnerLevel)
    }

    /**
     * Escaped a single item.
     *
     * @param item Item to escape
     * @param innerLevel Whether the escaped item is actually nested.
     * @return Provider to escaped item
     */
    Provider<String> escapeOneItem(Provider<?> item, boolean innerLevel) {
        item.map { escapeOneItemImpl(it, innerLevel) }
    }

    /**
     * Escaped a single item.
     *
     * @param item Item to escape
     * @param innerLevel Whether the escaped item is actually nested.
     * @return Provider to escaped item
     */
    Provider<String> escapeOneItem(Object item, boolean innerLevel) {
        stringTools.provideString(item).map {
            escapeOneItemImpl(it, innerLevel)
        }
    }

    /**
     * Escapes any Terraform string quotes.
     *
     * @param item String to escape
     * @return Escape string.
     */
    Provider<String> escapeQuotesInString(String item) {
        escapeQuotesInString(providerTools.provider { -> item })
    }

    /**
     * Escapes any Terraform string quotes.
     *
     * @param item String to escape
     * @return provider to escaped string.
     */
    Provider<String> escapeQuotesInString(Provider<String> item) {
        item.map { escapeDoubleQuotes(it) }
    }

    /**
     * Escapes file paths for safe inclusion in HCL files.
     *
     * @param path File path to escape
     * @return Escape file path as a string.
     */
    String escapeFilePath(File path) {
        final os = OperatingSystem.current()
        os.windows ? path.absolutePath.replaceAll(BACKSLASH, DOUBLE_BACKSLASH) : path.absolutePath
    }

    /**
     * Escapes file paths for safe inclusion in HCL files.
     *
     * @param path File path to escape
     * @return Escape file path as a string.
     */
    String escapeFilePath(Provider<File> path) {
        final os = OperatingSystem.current()
        os.windows ? path.get().absolutePath.replaceAll(BACKSLASH, DOUBLE_BACKSLASH) : path.get().absolutePath
    }

    /**
     * Escapes file paths for safe inclusion in HCL files.
     *
     * @param path File path to escape
     * @return Escape file path as a string.
     */
    String escapeFilePath(Object path) {
        escapeFilePath(fsOperations.provideFile(path))
    }

    /**
     * Escaped a single item.
     *
     * @param item Item to escape
     * @param innerLevel Whether the escaped item is actually nested.
     * @return Escaped item
     */
    private String escapeOneItemImpl(Object item, boolean innerLevel) {
        if (item == null) {
            return 'null'
        }
        switch (item) {
            case Provider:
                return escapeOneItem(((Provider) item).get(), innerLevel)
            case Optional:
                return escapeOneItem(((Optional) item).get(), innerLevel)
            case Supplier:
                return escapeOneItem(((Supplier) item).get(), innerLevel)
            case Callable:
                return escapeOneItem(((Callable) item).call(), innerLevel)
            case Map:
                return escapedMap((Map) item, innerLevel)
            case Iterable:
                return escapedList((Iterable) item, innerLevel)
            case Number:
            case Boolean:
                return stringTools.stringize(item)
            default:
                escapeDoubleQuotes(stringTools.stringize(item))
        }
    }

    private String escapeDoubleQuotes(String s) {
        s.replaceAll(~/"/, '\\\\"')
    }
    private final StringTools stringTools
    private final ProviderTools providerTools
    private final FileSystemOperations fsOperations

    private final static String COMMA_SEPARATED = ', '
    private final static Pattern BACKSLASH = ~/\x5C/
    private final static String DOUBLE_BACKSLASH = '\\\\\\\\'

    private static <I, V, O> List<O> toList(final Map<I, V> collection, Function<Map.Entry<I, V>, O> tx) {
        collection.entrySet().stream().map(tx).collect(Collectors.toList())
    }
}
