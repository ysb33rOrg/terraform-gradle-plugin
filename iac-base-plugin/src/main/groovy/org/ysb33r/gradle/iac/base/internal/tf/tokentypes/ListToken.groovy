/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.iac.base.internal.tf.tokentypes

import groovy.transform.CompileStatic
import org.gradle.api.Project
import org.gradle.api.provider.Provider
import org.ysb33r.gradle.iac.base.tf.backends.TokenValue
import org.ysb33r.grolifant5.api.core.StringTools

import javax.inject.Inject

/**
 * Backend token that is a token of other string.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class ListToken implements TokenValue {

    @Inject
    ListToken(Collection<?> value, Project tempProjectRef) {
        final list = value.collect {
            TokenHelpers.create(it, tempProjectRef.objects)
        }
        valueProvider = tempProjectRef.provider { -> '[' + list*.value.join(StringTools.COMMA_SPACE) + ']' }
    }

    @Override
    String getValue() {
        valueProvider.get()
    }

    @Override
    String toString() {
        value
    }

    private final Provider<String> valueProvider
}
