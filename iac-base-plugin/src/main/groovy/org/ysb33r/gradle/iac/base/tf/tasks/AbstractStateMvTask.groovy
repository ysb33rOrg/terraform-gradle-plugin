/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.iac.base.tf.tasks

import groovy.transform.CompileStatic
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.gradle.api.tasks.options.Option
import org.ysb33r.gradle.iac.base.errors.IacConfigurationException
import org.ysb33r.gradle.iac.base.tf.execspec.IacTfExecSpec

/**
 * The {@code state mv} command.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class AbstractStateMvTask extends AbstractStateTask {
    @Option(option = 'from-path', description = 'Source item (dotted-path) to move')
    void setSourcePath(String id) {
        this.source.set(id)
    }

    @Option(option = 'to-path', description = 'Destination for item (dotted-path)')
    void setDestinationPath(String id) {
        this.destination.set(id)
    }

    @Override
    protected void exec() {
        if (!source.present || !destination.present) {
            throw new IacConfigurationException('Resource paths missing. Use --from-path and --to-path.')
        }
        execSpec.cmd {
            addCommandLineArgumentProviders(finalArgs)
        }
        super.exec()
    }

    protected AbstractStateMvTask(
        String workspaceName,
        Class<? extends IacTfExecSpec> execSpecClass
    ) {
        super('mv', workspaceName, execSpecClass)
        this.source = project.objects.property(String)
        this.destination = project.objects.property(String)
        this.finalArgs = this.source.zip(this.destination) { x, y -> [x, y] }
    }

    private final Property<String> source
    private final Property<String> destination
    private final Provider<List<String>> finalArgs
}
