/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.iac.base.tf.tasks

import groovy.transform.CompileStatic
import org.gradle.api.Task
import org.gradle.api.provider.ListProperty
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.gradle.api.specs.Spec
import org.gradle.api.tasks.Internal
import org.gradle.api.tasks.OutputFile
import org.gradle.api.tasks.options.Option
import org.ysb33r.gradle.iac.base.errors.FeatureNotSupportedException
import org.ysb33r.gradle.iac.base.internal.tf.DefaultUsesVariables
import org.ysb33r.gradle.iac.base.internal.tf.TaskUtils
import org.ysb33r.gradle.iac.base.tf.ResourceFilter
import org.ysb33r.gradle.iac.base.tf.ToolFeatures
import org.ysb33r.gradle.iac.base.tf.UsesSourceSetName
import org.ysb33r.gradle.iac.base.tf.config.multilevel.UsesVariables
import org.ysb33r.gradle.iac.base.tf.execspec.DefaultArguments
import org.ysb33r.gradle.iac.base.tf.execspec.IacTfExecSpec
import org.ysb33r.grolifant5.api.core.runnable.ExecOutput

import java.util.concurrent.Callable

import static java.util.Collections.EMPTY_LIST
import static org.ysb33r.gradle.iac.base.internal.tf.TaskUtils.JSON_FORMAT

/**
 * Equivalent of {@code plan}.
 *
 * @since 2.0
 */
@CompileStatic
abstract class AbstractPlanTask extends AbstractTfStandardTask
    implements UsesVariables, ResourceFilter, UsesSourceSetName {
    /**
     * Adds an environment provider.
     *
     * @param provider Provider that will add additional environments.
     */
    @Override
    void addEnvironmentProvider(Provider<? extends Map<String, String>> provider) {
        super.addEnvironmentProvider(provider)
        this.showExecSpec.entrypoint.addEnvironmentProvider(provider.map { map ->
            map.findAll { it.key != 'TF_LOG' }
        })
    }

    /**
     * Sets the source set name
     * @param ssName Source set name.
     */
    @Override
    void setSourceSetName(String ssName) {
        this.sourceSetName.set(ssName)
    }

    /** Where the plan file will be written to.
     *
     * @return Location of plan file.
     */
    @OutputFile
    Provider<File> getPlanOutputFile() {
        this.planOutputFile
    }

    /**
     * Where the textual representation of the plan will be written to.
     *
     * @return Location of text file.
     */
    @OutputFile
    Provider<File> getPlanReportOutputFile() {
        this.planReportOutputFile
    }

    /**
     * This is the location of an internal tracker file used to keep state between apply & destroy cycles.
     *
     * @return Location of tracker file.
     */
    @Internal
    Provider<File> getInternalTrackerFile() {
        this.internalTrackerFile
    }

    /** This is the location of an variables file used to keep anything provided via the build script.
     *
     * @return Location of variables file.
     *
     * @since 0.13.0
     */
    @Internal
    Provider<File> getVariablesFile() {
        project.provider({ ->
            new File(dataDir.get(), "__.${workspaceName}.tfvars")
        } as Callable<File>)
    }

    /**
     * Select specific resources.
     *
     * @param resourceNames List of resources to target.
     */
    @Option(option = 'target', description = 'List of resources to target')
    @Override
    void setTargets(List<String> resourceNames) {
        this.targets.addAll(resourceNames)
    }

    /**
     * Mark resources to be replaces.
     *
     * @param resourceNames List of resources to target.
     */
    @Option(option = 'replace', description = 'List of resources to replace')
    @Override
    void setReplacements(List<String> resourceNames) {
        this.replacements.addAll(resourceNames)
    }

    // generate-config-out
    @Option(
        option = 'generate-config-out',
        description = 'Create additional resource file if the tool version supports it. Will fail if not supported'
    )
    void setOutputResourceFile(String path) {
        this.generateResourceFile.set(path)
    }

    /**
     * Where to write the report in human-readable or JSON format.
     *
     * @param state Set to {@code true} to output in JSON.
     */
    @Option(option = 'json', description = 'Output readable plan in JSON')
    void setJson(boolean state) {
        this.useJson.set(state)
    }

    @Override
    protected void exec() {
        final features = loadToolFeatures(executableVersion.get())
        createLogDir()
        setSourceDirAsWorkingDir()
        switchWorkspace()

        if (features.planHasDetailedExitCode()) {
            cmdArgs '-detailed-exitcode'
        }

        if (generateResourceFile.present) {
            if (features.planHasGenerateConfigOut()) {
                cmdArgs "-generate-config-out=${generateResourceFile.get()}"
            } else {
                throw new FeatureNotSupportedException(
                    '-generate-config-out is not supported on this version of the tool'
                )
            }
        }

        final runResult = runWithSecrets()
        handleResultCode(runResult, features)

        if (logger.infoEnabled) {
            logger.info '\n\n'
        }

        File planOut = planOutputFile.get()
        File textOut = planReportOutputFile.get()

        final showResult = showExecSpec.submitAsExec { spec ->
            spec.workingDir(sourceDir.get().absolutePath)
        }
        showResult.assertNormalExitValue()

        logger.lifecycle(
            "The plan file has been generated into ${planOut.toURI()}"
        )
        logger.lifecycle("The textual representation of the plan file has been generated into ${textOut.toURI()}")
    }

    protected AbstractPlanTask(
        String workspaceName,
        String defaultWorkspaceName,
        Class<? extends IacTfExecSpec> execSpecClass,
        boolean isDestroy
    ) {
        super(
            'plan',
            workspaceName,
            execSpecClass,
            DefaultArguments.builder().supportsColor().supportsInputs()
        )
        final ws = workspaceName == defaultWorkspaceName ? '' : ".${workspaceName}"
        final planExt = isDestroy ? 'tf.destroy.plan' : PLAN_FILE_EXTENSION
        this.varsFile = new DefaultUsesVariables(execSpec, this)
        this.targets = project.objects.listProperty(String)
        this.replacements = project.objects.listProperty(String)
        this.generateResourceFile = project.objects.property(String)
        this.useJson = project.objects.property(Boolean)
        this.useJson.convention(false)
        this.sourceSetName = project.objects.property(String)
        this.internalTrackerFile = dataDir.map { new File(it, "${ws}.tracker") }
        this.planOutputFile = whatIsPlanOutputFile(this.sourceSetName, workspaceName, defaultWorkspaceName, planExt)

        this.planReportOutputFile = sourceSetName.zip(this.useJson) { ssName, json ->
            "${ssName}${ws}.${planExt}.${json ? 'json' : 'txt'}"
        }.zip(reportsDir) { fName, dir ->
            new File(dir, fName)
        }

        if (isDestroy) {
            cmdArgs('-destroy')
        }

        execSpec.cmd {
            addCommandLineArgumentProviders(
                this.targets.zip(this.replacements) { t, r ->
                    (t.collect { "-target=${it}" } +
                        r.collect { "-replace=${it}" }
                    )*.toString()
                },
                this.planOutputFile.map { ["-out=${it.absolutePath}"]*.toString() },
//                this.useJson.map { it ? [JSON_FORMAT] : EMPTY_LIST }
            )
        }

        final showBuildersArgs = DefaultArguments.builder().supportsColor().build(this)
        this.showExecSpec = project.objects.newInstance(execSpecClass).tap {
            entrypoint {
                executable(executableLocation)
                environment = TaskUtils.defaultEnvironment()
            }
            cmd {
                command = 'show'
                args(showBuildersArgs)
                addCommandLineArgumentProviders(
                    useJson.map { it ? [JSON_FORMAT] : EMPTY_LIST },
                    planOutputFile.map { [it.absolutePath] }
                )
            }
            process {
                output {
                    captureTo(planReportOutputFile)
                }
            }
        }

        inputs.property('targets', this.targets)
        inputs.property('replacements', this.replacements)

        outputs.upToDateWhen(new Spec<Task>() {
            @Override
            boolean isSatisfiedBy(Task task) {
                !hasTaintedResources()
            }
        })
    }

    /**
     * Handle the exit code from the execution.
     *
     * @param exitOutput Result form running executable.
     * @param features Features that a tool might possess.
     */
    @Override
    protected void handleResultCode(ExecOutput exitOutput, ToolFeatures features) {
        if (features.planHasDetailedExitCode()) {
            switch (exitOutput.result.get().exitValue) {
                case 0:
                case 2:
                    return
                default:
                    exitOutput.assertNormalExitValue()
            }
        } else {
            exitOutput.assertNormalExitValue()
        }
    }

    /**
     * Loads features for the tool
     *
     * @param ver Tool version
     * @return Tool features
     */
    abstract protected ToolFeatures loadToolFeatures(String ver)

    private final ListProperty<String> targets
    private final ListProperty<String> replacements
    private final Property<String> generateResourceFile
    private final Property<Boolean> useJson
    private final IacTfExecSpec showExecSpec
    private final Provider<File> planOutputFile
    private final Provider<File> planReportOutputFile
    private final Provider<File> internalTrackerFile
    private final Property<String> sourceSetName

    @Delegate(includes = ['getVariablesFiles', 'setVariablesFiles'])
    private final UsesVariables varsFile
}
