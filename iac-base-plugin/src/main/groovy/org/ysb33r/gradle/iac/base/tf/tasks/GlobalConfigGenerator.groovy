/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.iac.base.tf.tasks

import groovy.transform.CompileStatic
import org.gradle.api.provider.MapProperty
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.gradle.api.tasks.CacheableTask
import org.gradle.api.tasks.OutputFile
import org.gradle.api.tasks.TaskAction
import org.ysb33r.grolifant5.api.core.runnable.GrolifantDefaultTask

/**
 * Generates a global configuration file for OpenTofu/Terraform.
 *
 * <p>Only available in root projects</p>.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
@CacheableTask
class GlobalConfigGenerator extends GrolifantDefaultTask {

    GlobalConfigGenerator() {
        this.configTokens = project.objects.mapProperty(String, String)
        this.credentialTokens = project.objects.mapProperty(String, String)
        this.out = project.objects.property(File)

        this.inputs.property('config', this.configTokens)
        this.inputs.property('auth', this.credentialTokens)
    }

    void setConfigTokens(Provider<Map<String, String>> tokens) {
        this.configTokens.set(tokens)
    }

    void setAuthTokens(Provider<Map<String, String>> tokens) {
        this.credentialTokens.set(tokens)
    }

    void setOutputFile(Provider<File> rc) {
        this.out.set(rc)
    }

    @OutputFile
    Provider<File> getOutputFile() {
        this.out
    }

    @TaskAction
    void exec() {
        outputFile.get().withWriter { writer ->
            configTokens.get().each {
                writer.println "${it.key} = ${it.value}"
            }

            credentialTokens.get().each { key, token ->
                writer.println "credentials \"${key}\" {"
                writer.println "  token = \"${token}\""
                writer.println '}'
            }
        }
    }

    private final Property<File> out
    private final MapProperty<String, String> configTokens
    private final MapProperty<String, String> credentialTokens
}
