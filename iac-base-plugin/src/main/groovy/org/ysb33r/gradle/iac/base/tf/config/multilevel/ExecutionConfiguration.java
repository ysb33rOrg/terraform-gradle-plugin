/**
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.iac.base.tf.config.multilevel;

import groovy.lang.Closure;
import org.gradle.api.provider.Provider;

import java.util.Collections;
import java.util.List;

/**
 * An extension that can be added to a task for representing a specific grouping of Terraform
 * command-line parameters.
 *
 * @author Schalk W. Cronjé
 */
public interface ExecutionConfiguration {
    /**
     * Returns the list of Terraform/OpenTofu command-line arguments.
     *
     * @return List of arguments to be added. Can be empty, but never {@code null}
     */
    Provider<List<String>> getCommandLineArgs();

    /**
     * Returns the list of variables in the form name=value
     *
     * @return Terraform/OpenTofu variables
     */
    Provider<List<String>> getTfVars();

    /**
     * A value that is used as an input property and which can be used to signal that a task for need to be run again.
     *
     * @return Provider to a special property.
     */
    Provider<String> getInputProperty();
}
