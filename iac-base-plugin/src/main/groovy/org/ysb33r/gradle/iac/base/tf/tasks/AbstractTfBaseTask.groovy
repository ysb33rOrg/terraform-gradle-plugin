/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.iac.base.tf.tasks

import groovy.transform.CompileStatic
import org.gradle.api.Action
import org.gradle.api.provider.ListProperty
import org.gradle.api.provider.MapProperty
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.gradle.api.tasks.Internal
import org.gradle.process.ExecSpec
import org.ysb33r.gradle.iac.base.internal.tf.TaskUtils
import org.ysb33r.gradle.iac.base.tf.ToolFeatures
import org.ysb33r.gradle.iac.base.tf.execspec.DefaultArguments
import org.ysb33r.gradle.iac.base.tf.execspec.IacTfExecSpec
import org.ysb33r.grolifant5.api.core.SimpleSecureString
import org.ysb33r.grolifant5.api.core.runnable.AbstractExecWrapperTask
import org.ysb33r.grolifant5.api.core.runnable.ExecOutput

/**
 * A base class for all Terraform and OpenTofu tasks.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
abstract class AbstractTfBaseTask extends AbstractExecWrapperTask<IacTfExecSpec> {

    /**
     * Add additional command-line parameters from external sources.
     *
     * @param opts Provider to options that will be added.
     */
    void addExecutionOptions(Provider<List<String>> opts) {
        this.exeOptions.addAll(opts)
    }

    /**
     * Sets the location of the executable
     *
     * @param file Location of executable.
     */
    void setExecutableLocation(Provider<File> file) {
        this.executable.set(file)
    }

    /**
     * Sets the version of the executable
     *
     * @param file Location of executable.
     *
     * @since 2.0
     */
    void setExecutableVersion(Provider<String> version) {
        this.executableVersion.set(version)
    }

    /**
     * Sets all of the secret variables that will be loaded into the environment.
     *
     * @param secretVars Provider of a set of credentials.
     */
    void setSecretVariables(Provider<Map<String, SimpleSecureString>> vars) {
        this.secretVars.set(vars.map {
            final gsps = grolifantSecurePropertyService.get()
            it.collectEntries { k, v ->
                [k, gsps.unpack(v)]
            } as Map<String, char[]>
        })
    }

    protected AbstractTfBaseTask(
        String tfCmd,
        Class<? extends IacTfExecSpec> execSpecClass,
        DefaultArguments.Builder builder
    ) {
        this.executable = project.objects.property(File)
        this.executableVersion = project.objects.property(String)
        this.exeOptions = project.objects.listProperty(String)
        this.secretVars = project.objects.mapProperty(String, char[])
        final tempExeRef = this.executable
        this.execSpec = project.objects.newInstance(execSpecClass).tap {
            entrypoint {
                executable(tempExeRef)
            }
            cmd {
                command = tfCmd
                args(builder.build(this))
                addCommandLineArgumentProviders(
                    this.exeOptions,
                )
            }
        }
        environment = TaskUtils.defaultEnvironment()
    }

    /**
     * Access to the execution specification.
     *
     * @return This task's execution specification
     */
    @Override
    protected IacTfExecSpec getExecSpec() {
        this.execSpec
    }

    /**
     * Add secret environment variables, execute the specification and then handle the result code.
     */
    @Override
    protected void exec() {
        handleResultCode(runWithSecrets())
    }

    /**
     * Executes the execution specification after loading the secrets.
     *
     * @return Execution result.
     */
    protected ExecOutput runWithSecrets() {
        runWithSecrets { spec -> true }
    }

    /**
     * Executes the execution specification after loading the secrets.
     *
     * @param additionalConfig Action to add some additional configuration to the exec spec
     * @return Execution result.
     */
    protected ExecOutput runWithSecrets(Action<ExecSpec> additionalConfig) {
        final secretEnvVars = this.secretVars.get()
        execSpec.submitAsExec { spec ->
            logger.info "Using environment: ${spec.environment.findAll { it.key.startsWith('TF') }}"
            spec.ignoreExitValue = true
            spec.environment(secretEnvVars)
            additionalConfig.execute(spec)
        }
    }

    /**
     * Handle the exit code from the execution.
     *
     * @param exitOutput Result form running executable.
     */
    protected void handleResultCode(ExecOutput exitOutput) {
        exitOutput.assertNormalExitValue()
    }

    /**
     * Handle the exit code from the execution.
     *
     * @param exitOutput Result form running executable.
     * @param features Features that a tool might possess.
     */
    @SuppressWarnings('UnusedMethodParameter')
    protected void handleResultCode(ExecOutput exitOutput, ToolFeatures features) {
        exitOutput.assertNormalExitValue()
    }

    /**
     * When command is run, capture the standard output
     *
     * @param out Output file
     */
    protected void captureStdOutTo(Provider<File> out) {
        execSpec.process {
            output {
                captureTo(out)
            }
        }
    }

    /**
     * Marks task to always be out of date.
     *
     * Calls this from the constructor of Terraform task types that should always be out of date.
     *
     * @since 0.10.
     */
    protected void alwaysOutOfDate() {
        inputs.property('always-out-of-date', UUID.randomUUID().toString())
    }

    /**
     * Helper methods to add additional command arguments
     *
     * @param arguments One or more arguments
     */
    protected void cmdArgs(Object... arguments) {
        execSpec.cmd {
            args(arguments)
        }
    }

    @Override
    protected Provider<File> getExecutableLocation() {
        this.executable
    }

    @Internal
    protected Provider<String> getExecutableVersion() {
        this.executableVersion
    }

    /**
     * Skip this task if Gradle is offline.
     */
    protected void skipIfOffline() {
        enabled = !projectTools().offlineProvider.get()
    }

    /**
     * Get a reference to the secret variables.
     *
     * @return
     */
    @Internal
    protected Provider<Map<String, char[]>> getSecretVariables() {
        this.secretVars
    }

    private final Property<File> executable
    private final Property<String> executableVersion
    private final IacTfExecSpec execSpec
    private final MapProperty<String, char[]> secretVars
    private final ListProperty<String> exeOptions
}
