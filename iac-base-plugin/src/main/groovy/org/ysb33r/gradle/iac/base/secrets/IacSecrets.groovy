/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.iac.base.secrets

import groovy.transform.CompileStatic
import org.gradle.api.Named
import org.gradle.api.Project
import org.ysb33r.gradle.iac.base.internal.tf.DefaultSecretVariables
import org.ysb33r.gradle.iac.base.tf.SecretVariables
import org.ysb33r.grolifant5.api.core.ConfigCacheSafeOperations

/**
 * Base class for implementing secret maintenance.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class IacSecrets implements SecretVariables, Named {

    final String name

    protected IacSecrets(String name, Project tempProjectRef) {
        this.securityVars = tempProjectRef.objects.newInstance(DefaultSecretVariables)
        this.name = name
        this.ccso = ConfigCacheSafeOperations.from(tempProjectRef)
    }

    protected final ConfigCacheSafeOperations ccso

    @Delegate(interfaces = true)
    private final SecretVariables securityVars
}
