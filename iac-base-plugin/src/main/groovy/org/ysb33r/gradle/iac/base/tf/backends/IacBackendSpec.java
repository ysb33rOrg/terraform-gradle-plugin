/**
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.iac.base.tf.backends;

import org.gradle.api.Named;
import org.gradle.api.provider.Provider;
import org.ysb33r.gradle.iac.base.tf.SecretVariables;

import java.util.Map;

/**
 * Describes the configuration for a backend.
 *
 * @author Schalk W. Cronjé
 *
 * @since 0.12
 */
public interface IacBackendSpec extends Named, IacBackendAttributesSpec, SecretVariables {

    /**
     * Replace all tokens with a new set.
     * <p>
     * Only useful when a custom template is used.
     *
     * @param newTokens New replacement set
     */
    void setTokens(Map<String, ?> newTokens);

    /**
     * Adds more tokens.
     * <p>
     * Only useful when a custom template is used.
     *
     * @param moreTokens Additional tokens for replacement.
     */
    void tokens(Map<String, ?> moreTokens);

    /**
     * Set a single token value.
     *
     * @param key   Token name
     * @param value Lazy-evaluted value. Anything that can resolve to a string.
     */
    void token(String key, Object value);

    /**
     * Set a single token value.
     *
     * @param key   Token name
     * @param value Lazy-evaluted value which is known to be a file. Anything that can resolve to a file.
     */
    void fileToken(String key, Object value);

    /**
     * Adds a provider of tokens.
     * <p>
     * These providers are processed before any of the customisations on the class.
     *
     * @param tokenProvider Addition provider of tokens
     * @since 1.0
     */
    void addTokenProvider(Provider<Map<String, TokenValue>> tokenProvider);
}
