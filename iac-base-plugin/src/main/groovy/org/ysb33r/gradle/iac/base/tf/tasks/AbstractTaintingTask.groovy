/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.iac.base.tf.tasks

import groovy.transform.CompileStatic
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.gradle.api.tasks.options.Option
import org.ysb33r.gradle.iac.base.errors.IacConfigurationException
import org.ysb33r.gradle.iac.base.tf.UsesSourceSetName
import org.ysb33r.gradle.iac.base.tf.execspec.IacTfExecSpec

import static java.util.Collections.EMPTY_LIST

/**
 * Base task for the {@code untaint} and {@code taint} commands.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class AbstractTaintingTask extends AbstractTfStandardTask implements UsesSourceSetName {

    /**
     * Sets the source set name.
     *
     * @param ssName Source set name.
     */
    @Override
    void setSourceSetName(String ssName) {
        this.sourceSetName.set(ssName)
    }

    @Option(option = 'path', description = 'Resource to untaint')
    void setResourcePath(String id) {
        this.resourcePath.set(id)
    }

    @Option(option = 'allow-missing', description = 'Allow task to succeed even if the resource is missing')
    void setAllowMissing(boolean flag) {
        this.allowMissing.set(flag)
    }

    @Option(option = 'ignore-remote-version',
        description = 'Continue if remote and local state versions differ')
    void setIgnoreRemote(boolean flag) {
        this.ignoreRemoteVersion.set(flag)
    }

    protected AbstractTaintingTask(
        String workspaceName,
        String defaultWorkspaceName,
        Class<? extends IacTfExecSpec> execSpecClass,
        boolean isUntaint
    ) {
        super(isUntaint ? 'untaint' : 'taint', workspaceName, execSpecClass)
        alwaysOutOfDate()

        this.isUntaint = isUntaint
        this.resourcePath = project.objects.property(String)
        this.ignoreRemoteVersion = project.objects.property(Boolean).convention(false)
        this.allowMissing = project.objects.property(Boolean).convention(false)
        this.sourceSetName = project.objects.property(String)
        this.planFile = whatIsPlanOutputFile(sourceSetName, workspaceName, defaultWorkspaceName, PLAN_FILE_EXTENSION)

        execSpec.cmd {
            addCommandLineArgumentProviders(
                allowMissing.map { it ? ['-allow-missing'] : EMPTY_LIST },
                ignoreRemoteVersion.map { it ? ['-ignore-remote-version'] : EMPTY_LIST }
            )
        }
    }

    @Override
    protected void exec() {
        if (!resourcePath.present) {
            throw new IacConfigurationException('No resource path specified. Use --path.')
        }

        createLogDir()
        setSourceDirAsWorkingDir()
        switchWorkspace()

        execSpec.cmd {
            addCommandLineArgumentProviders(resourcePath.map { [it] })
        }

        handleResultCode(runWithSecrets())
        isUntaint ? removeTaintedResource(resourcePath.get()) : addTaintedResource(resourcePath.get())

        planFile.get().delete()
        didWork = true
    }

    private final boolean isUntaint
    private final Property<String> resourcePath
    private final Property<Boolean> allowMissing
    private final Property<Boolean> ignoreRemoteVersion
    private final Property<String> sourceSetName
    private final Provider<File> planFile
}
