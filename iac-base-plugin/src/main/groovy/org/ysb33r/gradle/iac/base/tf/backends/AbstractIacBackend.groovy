/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.iac.base.tf.backends

import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import org.gradle.api.Project
import org.gradle.api.model.ObjectFactory
import org.gradle.api.provider.MapProperty
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.ysb33r.gradle.iac.base.hcl.HclTransformer
import org.ysb33r.gradle.iac.base.internal.tf.DefaultSecretVariables
import org.ysb33r.gradle.iac.base.tf.SecretVariables
import org.ysb33r.grolifant5.api.core.ConfigCacheSafeOperations

/**
 * Configuration for an OpenTofu/Terraform backend.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
@Slf4j
class AbstractIacBackend implements IacBackendSpec {

    /**
     * Backend name.
     */
    final String name

    /**
     * Helper to translate objects into HCL formats.
     */
    final HclTransformer hclTransformer

    /**
     * Replace all tokens with a new set.
     *
     * @param newTokens New replacement set
     */
    @Override
    void setTokens(Map<String, ?> newTokens) {
        this.configuredTokens.set([:])
        tokens(newTokens)
    }

    /**
     * Adds more tokens.
     * <p>
     * Only useful when a custom template is used.
     *
     * @param moreTokens Additional tokens for replacement.
     */
    @Override
    void tokens(Map<String, ?> moreTokens) {
        moreTokens.each { k, v ->
            this.configuredTokens.put(k, TokenValue.of(v, objectFactory))
        }
    }

    /**
     * Set a single token value.
     *
     * @param key Token name
     * @param value Lazy-evaluated value. Anything that can resolve to a string.
     */
    @Override
    void token(String key, Object value) {
        this.configuredTokens.put(key, TokenValue.of(value, objectFactory))
    }

    /**
     * Set a single token value.
     *
     * @param key Token name
     * @param value Lazy-evaluted value which is known to be a file. Anything that can resolve to a file.
     */
    @Override
    void fileToken(String key, Object value) {
        this.configuredTokens.put(key,
            TokenValue.of(
                ccso.fsOperations().provideFile(value),
                objectFactory
            )
        )
    }

    /**
     * Adds a provider of tokens.
     *
     * @param tokenProvider Addition provider of tokens
     */
    @Override
    void addTokenProvider(Provider<Map<String, TokenValue>> tokenProvider) {
        this.configuredTokens.putAll(tokenProvider)
    }

    /**
     * A provider for all the tokens that were set.
     *
     * @return Provider to a map of tokens.
     */
    @Override
    Provider<Map<String, TokenValue>> getTokenProvider() {
        this.configuredTokens
    }

    /**
     * Assign the prefix.
     *
     * @param p Object that can be converted to a string. Can be a {@code Provider} as well.
     */
    void setPrefix(Object p) {
        ccso.stringTools().updateStringProperty(this.prefix, p)
    }

    protected AbstractIacBackend(String name, Project tempProjectRef) {
        this.name = name
        this.ccso = ConfigCacheSafeOperations.from(tempProjectRef)
        this.configuredTokens = tempProjectRef.objects.mapProperty(String, TokenValue)
        this.prefix = tempProjectRef.objects.property(String)
        this.hclTransformer = new HclTransformer(this.ccso)
        this.securityVars = tempProjectRef.objects.newInstance(DefaultSecretVariables)
        this.objectFactory = tempProjectRef.objects
    }

    /**
     * Configuration cache safe operations that can be used by backend implementations.
     */
    protected final ConfigCacheSafeOperations ccso

    private final MapProperty<String, TokenValue> configuredTokens
    private final Property<String> prefix
    private final ObjectFactory objectFactory

    @Delegate(interfaces = true)
    private final SecretVariables securityVars
}
