/**
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.iac.base.tf.config.multilevel;

import org.gradle.api.provider.Provider;
import org.gradle.api.tasks.InputFiles;

import java.io.File;
import java.util.List;

/**
 * Task interface that indicates the task requires variables.
 *
 * @author Schalk W. Cronjé
 * @since 2.0
 */
public interface UsesVariables {

    /**
     * The location of the variables files.
     *
     * @return Location provider
     */
    @InputFiles
    Provider<List<File>> getVariablesFiles();

    /**
     * Sets the location of the variables file.
     *
     * @param file Location provider.
     */
    void setVariablesFiles(Provider<List<File>> file);
}
