/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.iac.base.tf

import groovy.transform.CompileStatic
import org.gradle.api.Project
import org.gradle.api.provider.MapProperty
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.ysb33r.gradle.iac.base.hcl.HclTransformer
import org.ysb33r.grolifant5.api.core.ConfigCacheSafeOperations
import org.ysb33r.grolifant5.api.core.OperatingSystem

/**
 * Extension that describes a {@code terraformrc} or {@code opentofurc} file.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class AbstractGlobalConfigExtension implements PluginCache, TfConfig {

    /** Disable checkpoint.
     *
     * When set to true, disables upgrade and security bulletin checks that require reaching out to
     * HashiCorp-provided network services.
     *
     * Default is {@code true}.
     *
     * @param flag Turn check on or off.
     */
    void setDisableCheckPoint(boolean flag) {
        this.config.put('disable_checkpoint', flag.toString())
    }

    /**
     * Disable checkpoint signature.
     *
     * When set to true, allows the upgrade and security bulletin checks described above but disables the use of an
     * anonymous id used to de-duplicate warning messages.
     *
     * Default is {@code false}.
     *
     * @param flag Turn check on or off.
     */
    void setDisableCheckPointSignature(boolean flag) {
        this.config.put('disable_checkpoint_signature', flag.toString())
    }

    /**
     * Plugin cache may break dependency lock file.
     *
     * Setting this option gives Terraform/OpenTofu CLI permission to create an incomplete dependency
     * lock file entry for a provider if that would allow Terraform/OpenTofu to use the cache to install that provider.
     *
     * In that situation the dependency lock file will be valid for use on the current system but may not be
     * valid for use on another computer with a different operating system or CPU architecture, because it
     * will include only a checksum of the package in the global cache.
     *
     * Default is {@code false}.
     *
     * @param flag Turn check on or off.
     */
    void setPluginCacheMayBreakDependencyLockFile(boolean flag) {
        this.config.put('plugin_cache_may_break_dependency_lock_file', flag.toString())
    }

    /**
     * Select source of configuration.
     *
     * When set to {@code true} use global configuration rather than the Gradle root project configuration.
     *
     * Default is {@code false}.
     *
     * @param flag Whether to use global config
     */
    void setUseGlobalConfig(boolean flag) {
        final os = OperatingSystem.current()
        if (flag) {
            if (os.windows) {
                this.homeDir.map { new File(it, rcFileName.replaceFirst(~/^\./, '')) }
            } else {
                this.rcFile.set(this.homeDir.map { new File(it, rcFileName) })
            }
        } else {
            this.rcFile.set(ccso.fsOperations().provideProjectCacheDir().map { new File(it, rcFileName) })
        }
    }

    /**
     * Sets the location of the plugin cache directory
     *
     * @param dir Anything that is convertible using {@code project.file}.
     */
    void setPluginCacheDir(Object dir) {
        this.ccso.fsOperations().updateFileProperty(
            this.pluginCacheDir,
            this.ccso.fsOperations().provideFile(dir)
        )
    }

    /**
     * Sets the timeout for updating the plugin cache dir.
     *
     * @param timeout Timeout in milliseconds.
     *   Set to 0 or less for no lock.
     */
    void setPluginCacheLockTimeout(Integer timeout) {
        this.pluginCacheDirTimeout.set(timeout)
    }

    /**
     * Location of plugin cache directory.
     *
     * @return Location of cache directory as a file provider.
     */
    @Override
    Provider<File> getPluginCacheDir() {
        this.pluginCacheDir
    }

    @Override
    Provider<Integer> getUseLockWithTimeout() {
        this.pluginCacheDirTimeout
    }

    /**
     * Location of the {@code terraformrc/opentofurc} file if it should be written by the project.
     *
     * @return Location of {@code terraformrc/opentofurc} file. Never {@code null}.
     */
    @Override
    Provider<File> getConfigFile() {
        this.rcFile
    }

    /**
     * Adds a credential set to the configuration file.
     *
     * @param key Remote Terraform system name
     * @param token Token for remote Terraform system.
     */
    void credentials(String key, String token) {
        this.credentials.put(key, token)
    }

    /**
     * Get all configuration tokens.
     *
     * @return Provider to a map of tokens.
     */
    Provider<Map<String, String>> getConfigTokens() {
        this.config
    }

    /**
     * Get all credentials tokens.
     *
     * @return Provider to a map of credential tokens.
     */
    Provider<Map<String, String>> getCredentialTokens() {
        this.credentials
    }

    protected AbstractGlobalConfigExtension(
        Project project,
        String cacheDirName,
        String rcFileName
    ) {
        final os = OperatingSystem.current()
        this.ccso = ConfigCacheSafeOperations.from(project)
        this.rcFile = project.objects.property(File)
        this.pluginCacheDirTimeout = project.objects.property(Integer).convention(300000)
        this.rcFileName = rcFileName
        this.homeDir = ccso.fsOperations().provideFile(ccso.providerTools().environmentVariable(os.homeVar))
        this.pluginCacheDir = project.objects.property(File)
        this.hclTransformer = new HclTransformer(this.ccso)
        this.config = project.objects.mapProperty(String, String)
        this.credentials = project.objects.mapProperty(String, String)

        ccso.fsOperations().updateFileProperty(
            this.pluginCacheDir,
            ccso.fsOperations().gradleUserHomeDir.map { new File(it, "caches/${cacheDirName}.d") }
        )

        this.config.put('plugin_cache_dir', pluginCacheDir.map {
            "\"${hclTransformer.escapeFilePath(it)}\"".toString()
        })

        pluginCacheMayBreakDependencyLockFile = false
        disableCheckPointSignature = false
        disableCheckPoint = true
        useGlobalConfig = false
    }

    protected final MapProperty<String, String> config

    private final Property<File> pluginCacheDir
    private final Property<Integer> pluginCacheDirTimeout
    private final Property<File> rcFile
    private final MapProperty<String, String> credentials
    private final ConfigCacheSafeOperations ccso
    private final HclTransformer hclTransformer
    private final String rcFileName
    private final Provider<File> homeDir
}
