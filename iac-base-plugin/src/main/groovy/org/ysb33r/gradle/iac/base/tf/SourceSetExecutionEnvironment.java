/**
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.iac.base.tf;

import org.gradle.api.provider.Provider;
import org.ysb33r.gradle.iac.base.tf.config.multilevel.ExecutionConfiguration;
import org.ysb33r.gradle.iac.base.tf.config.multilevel.ExecutionOptions;
import org.ysb33r.gradle.iac.base.tf.config.multilevel.ResolvedExecutionsOptions;

import java.io.File;
import java.util.Map;

/**
 * Provide thew default executable, environment and secrets associated with a source set.
 *
 * @author Schalk W. Cronjé
 * @since 2.0
 */
public interface SourceSetExecutionEnvironment extends SecretVariablesProvider {
    /**
     * Location of the executable that will used for this source set
     *
     * @return Provider to the executable
     */
    Provider<File> getExecutableLocation();

    /**
     * Version of the executable that will used for this source set
     *
     * @return Provider to the version.
     */
    Provider<String> getExecutableVersion();

    /**
     * Provider to the full running environment.
     *
     * @return Provider to the environment map.
     */
    Provider<Map<String, String>> getEnvironment();

    /**
     * The execution options configured on the toolchain.
     *
     * @return Execution options.
     */
    Provider<ResolvedExecutionsOptions> executionOptionsFor(Iterable<Class<? extends ExecutionConfiguration>> types);
}
