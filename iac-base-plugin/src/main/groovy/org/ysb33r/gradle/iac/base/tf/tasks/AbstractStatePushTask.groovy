/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.iac.base.tf.tasks

import groovy.transform.CompileStatic
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.gradle.api.tasks.Internal
import org.gradle.api.tasks.options.Option
import org.ysb33r.gradle.iac.base.errors.IacConfigurationException
import org.ysb33r.gradle.iac.base.tf.execspec.IacTfExecSpec

import static java.util.Collections.EMPTY_LIST

/**
 * The {@code state push} command.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class AbstractStatePushTask extends AbstractStateTask {
    @Option(option = 'state-file', description = 'local state file to push (relative to tf directory)')
    void setStateFile(String path) {
        this.stateFileProvider.set(sourceDir.map { new File(it, path) })
    }

    @Option(
        option = 'force',
        description = 'Write the state even if lineages don\'t match or the remote serial is higher'
    )

    void setForce(boolean flag) {
        this.force.set(flag)
    }

    @Internal
    Provider<File> getStateInputFile() {
        stateFileProvider
    }

    @Override
    void exec() {
        if (!stateInputFile.present) {
            throw new IacConfigurationException('No destination state file specified. Use --state-file.')
        }
        execSpec.cmd {
            addCommandLineArgumentProviders(finalParams)
        }
        super.exec()
    }

    protected AbstractStatePushTask(
        String workspaceName,
        Class<? extends IacTfExecSpec> execSpecClass
    ) {
        super('push', workspaceName, execSpecClass)
        stateFileProvider = project.objects.property(File)
        this.force = project.objects.property(Boolean)
        this.force.set(false)
        this.finalParams = stateFileProvider.zip(this.force) { file, forceFlag ->
            (forceFlag ? ['-force'] : EMPTY_LIST) + [file.absolutePath]
        }
        alwaysOutOfDate()
    }

    private final Property<File> stateFileProvider
    private final Property<Boolean> force
    private final Provider<List<String>> finalParams
}
