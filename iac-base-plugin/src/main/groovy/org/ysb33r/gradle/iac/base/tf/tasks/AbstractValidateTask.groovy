/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.iac.base.tf.tasks

import groovy.transform.CompileStatic
import org.gradle.api.provider.Property
import org.gradle.api.tasks.options.Option
import org.ysb33r.gradle.iac.base.tf.execspec.DefaultArguments
import org.ysb33r.gradle.iac.base.tf.execspec.IacTfExecSpec

import javax.inject.Inject

import static java.util.Collections.EMPTY_LIST
import static org.ysb33r.gradle.iac.base.internal.tf.TaskUtils.JSON_FORMAT

/**
 * Equivalent of {@code validate}.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class AbstractValidateTask extends AbstractTfStandardTask {

    @Inject
    AbstractValidateTask(
        String workspaceName,
        Class<? extends IacTfExecSpec> execSpecClass
    ) {
        super(
            'validate',
            workspaceName,
            execSpecClass,
            DefaultArguments.builder().supportsColor()
        )

        this.json = project.objects.property(Boolean)
        this.json.set(false)

        execSpec.cmd {
            commandLineArgumentProviders.add(json.map { it ? [JSON_FORMAT] : EMPTY_LIST })
        }
    }

    /**
     * Whether output should be in JSON.
     *
     * This option can be set from the command-line with {@code --json}.
     */
    @Option(option = 'json', description = 'Force output to be in JSON format')
    void setJson(boolean flag) {
        this.json.set(flag)
    }

    private final Property<Boolean> json
}
