/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.iac.base.secrets

import groovy.transform.CompileStatic
import org.gradle.api.Project
import org.ysb33r.grolifant5.api.core.OperatingSystem

import javax.inject.Inject

/**
 * Holds AWS secrets as environment variables.
 *
 * @author Schalk W. Cronjé
 *
 * @since
 */
@CompileStatic
class AwsSecrets extends IacSecrets {

    public static final String AWS_KEY = 'AWS_ACCESS_KEY_ID'
    public static final String AWS_SECRET = 'AWS_SECRET_ACCESS_KEY'
    public static final String AWS_TOKEN = 'AWS_SESSION_TOKEN'
    public static final String AWS_PROFILE = 'AWS_PROFILE'
    public static final String AWS_CONFIG_FILE = 'AWS_CONFIG_FILE'
    public static final String AWS_CREDENTIALS_FILE = 'AWS_SHARED_CREDENTIALS_FILE'

    @Inject
    AwsSecrets(String name, Project tempProjectRef) {
        super(name, tempProjectRef)
    }

    void useAccessKeyId(Object key) {
        secretVariable(AWS_KEY, key)
    }

    void useSecretAccessKey(Object key) {
        secretVariable(AWS_SECRET, key)
    }

    void useSharedCredentialsFile() {
        final os = OperatingSystem.current()
        final homeVar = ccso.providerTools().environmentVariable(os.homeVar)
        final subpath = '.aws/credentials'
        final path = homeVar.map { "${it}/${subpath}" }
        useSharedCredentialsFile(path)
    }

    void useSharedCredentialsFile(Object file) {
        secretVariable(
            AWS_CREDENTIALS_FILE,
            ccso.fsOperations().provideFile(file).map { it.absolutePath }
        )
    }

    void useConfigFile() {
        final os = OperatingSystem.current()
        final homeVar = ccso.providerTools().environmentVariable(os.homeVar)
        final subpath = '.aws/config'
        final path = homeVar.map { "${it}/${subpath}" }
        useSharedCredentialsFile(path)
    }

    void useConfigFile(Object file) {
        secretVariable(
            AWS_CONFIG_FILE,
            ccso.fsOperations().provideFile(file).map { it.absolutePath }
        )
    }

    void useProfile(Object profileName) {
        secretVariable(AWS_PROFILE, profileName)
    }

    /**
     * Sets the default to be to use AWS credentials from the environment and pass as-is to the appropriate tool.
     *
     */
    void useAwsCredentialsFromEnvironment() {
        AWS_ENV.findAll { it in AWS_ENV_NEEDED }.each {
            secretVariable(it, System.getenv(it))
        }
    }

    private static final Set<String> AWS_ENV = System.getenv().keySet()
        .findAll { k -> k.startsWith('AWS_') }.toSet()

    private static final Set<String> AWS_ENV_NEEDED = [
        AWS_KEY, AWS_SECRET, AWS_TOKEN, AWS_PROFILE, AWS_CONFIG_FILE, AWS_CREDENTIALS_FILE
    ].toSet()
}
