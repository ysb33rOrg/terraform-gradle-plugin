/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.iac.base.tf.tasks

import groovy.transform.CompileStatic
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.gradle.api.tasks.Input
import org.ysb33r.grolifant5.api.core.wrappers.AbstractWrapperCacheBinaryTask

@CompileStatic
abstract class AbstractIacCacheBinaryTask extends  AbstractWrapperCacheBinaryTask {

    final String propertiesDescription

    void setBinaryLocationProvider(Provider<String> provider) {
        this.binaryLocation.set(provider)
    }

    void setBinaryVersionProvider(Provider<String> provider) {
        this.binaryVersion.set(provider)
    }

    void setConfigLocation(Provider<String> location) {
        this.configLocationAsPath.set(location)
    }

    /**
     * Location of the configuration file.
     *
     * @return Provider to the location.
     */
    @Input
    Provider<String> getConfigLocation() {
        this.configLocationAsPath
    }

    protected AbstractIacCacheBinaryTask(
        String localPropertiesDefault,
        String toolName
    ) {
        super(localPropertiesDefault)
        this.propertiesDescription = "Describes the ${toolName.capitalize()} usage for the ${project.name} project"
        this.configLocationAsPath = project.objects.property(String)
        this.binaryVersion = project.objects.property(String)
        this.binaryLocation = project.objects.property(String)
    }

    @Override
    protected Provider<String> getBinaryLocationProvider() {
        this.binaryLocation
    }

    @Override
    protected Provider<String> getBinaryVersionProvider() {
        this.binaryVersion
    }

    @Override
    protected Map<String, String> getAdditionalProperties() {
        final map = super.additionalProperties
        map.putAll(
            APP_VERSION: binaryVersion.get(),
            APP_LOCATION: binaryLocation.get(),
            CONFIG_LOCATION: configLocationAsPath.get()
        )
        map
    }

    private final Property<String> binaryLocation
    private final Property<String> binaryVersion
    private final Property<String> configLocationAsPath
}
