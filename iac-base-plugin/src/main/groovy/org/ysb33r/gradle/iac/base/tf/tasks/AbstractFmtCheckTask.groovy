/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.iac.base.tf.tasks

import groovy.transform.CompileStatic
import org.ysb33r.gradle.iac.base.tf.execspec.IacTfExecSpec

import static java.util.Collections.EMPTY_LIST

/**
 * The {@code fmt -check} command.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class AbstractFmtCheckTask extends AbstractTfStandardTask {

    protected AbstractFmtCheckTask(Class<? extends IacTfExecSpec> execSpecClass) {
        super('fmt', null, execSpecClass)
        final fixedArgs = ['-check'] +
            (logger.infoEnabled ? ['-diff'] : EMPTY_LIST) +
            (logger.quietEnabled ? ['-list=true'] : EMPTY_LIST)

        execSpec.cmd {
            args(fixedArgs)
        }
    }
}
