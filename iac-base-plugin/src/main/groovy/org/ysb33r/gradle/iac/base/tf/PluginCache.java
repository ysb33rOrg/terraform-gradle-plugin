/**
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.iac.base.tf;

import org.gradle.api.provider.Provider;

import java.io.File;

/**
 * Plugin Cache methods.
 *
 * @author Schalk W. Cronjé
 * @since 2.0
 */
public interface PluginCache {
    /**
     * Location of plugin cache directory.
     *
     * @return Location of cache directory as a file provider.
     */
    Provider<File> getPluginCacheDir();

    /**
     * Indicates whether access to the plugin cache dir should be locked between operations that writes to the
     * cache directory
     *
     * @return Returns a timeout in millseconds.
     *   If the provider is empty or the values is less than 1, no locking will be performed.
     */
    Provider<Integer> getUseLockWithTimeout();
}
