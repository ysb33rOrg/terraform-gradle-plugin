/**
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.iac.base.tf.backends;

import org.gradle.api.model.ObjectFactory;
import org.ysb33r.gradle.iac.base.internal.tf.tokentypes.TokenHelpers;
import org.ysb33r.grolifant5.api.core.ConfigCacheSafeOperations;
import org.ysb33r.grolifant5.api.core.ProviderTools;

import java.io.Serializable;

/**
 * Describes a token value for a backend.
 *
 * @author Schalk W. Cronjé
 * @since 2.0
 */
public interface TokenValue extends Serializable {

    /**
     * Creates an instance from an arbitrary value.
     *
     * @param value Object that needs to be converted to a value.
     * @param objects An {@link ObjectFactory}.
     * @return An instance of something that implements {@link TokenValue}.
     */
    static TokenValue of(final Object value, final ObjectFactory objects) {
        return TokenHelpers.create(value, objects);
    }

    /**
     * Creates an instance from an arbitrary value.
     *
     * @param value Object that needs to be converted to a value.
     * @param pt An instance of {@link ProviderTools}.
     * @return An instance of something that implements {@link TokenValue}.
     */
    static TokenValue of(final Object value, final ProviderTools pt) {
        return TokenHelpers.create(value, pt);
    }

    /**
     * Creates an instance from an arbitrary value.
     *
     * @param value Object that needs to be converted to a value.
     * @param ccso An instancer of {@link ProviderTools}.
     * @return An instance of something that implements {@link TokenValue}.
     */
    static TokenValue of(final Object value, final ConfigCacheSafeOperations ccso) {
        return TokenHelpers.create(value, ccso.providerTools());
    }

    String getValue();
}
