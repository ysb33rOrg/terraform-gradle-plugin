/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.iac.base.tf.tasks

import groovy.transform.CompileStatic
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.gradle.api.tasks.Internal
import org.gradle.api.tasks.options.Option
import org.ysb33r.gradle.iac.base.errors.IacConfigurationException
import org.ysb33r.gradle.iac.base.tf.execspec.IacTfExecSpec

/**
 * The {@code state pull} command.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class AbstractStatePullTask extends AbstractStateTask {

    @Option(option = 'state-file', description = 'where to write local state file (relative to tf directory)')
    void setStateFile(String path) {
        this.stateFileProvider.set(sourceDir.map { new File(it, path) })
    }

    @Internal
    Provider<File> getStateOutputFile() {
        stateFileProvider
    }

    @Override
    void exec() {
        if (!stateOutputFile.present) {
            throw new IacConfigurationException('No destination state file specified. Use --state-file.')
        }
        execSpec.cmd {
            addCommandLineArgumentProviders(stateFileProvider.map { [it.absolutePath] })
        }
        super.exec()
        URI fileLocation = stringTools().urize(stateOutputFile)
        logger.lifecycle("\nState file has been written to ${fileLocation}\n")
    }

    protected AbstractStatePullTask(
        String workspaceName,
        Class<? extends IacTfExecSpec> execSpecClass
    ) {
        super('pull', workspaceName, execSpecClass)
        stateFileProvider = project.objects.property(File)
        captureStdOutTo(stateOutputFile)
        alwaysOutOfDate()
    }

    private final Property<File> stateFileProvider
}
