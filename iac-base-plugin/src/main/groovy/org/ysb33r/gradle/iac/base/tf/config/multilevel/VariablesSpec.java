/**
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.iac.base.tf.config.multilevel;

import groovy.lang.Closure;
import groovy.lang.DelegatesTo;
import org.gradle.api.Action;
import org.gradle.api.provider.Provider;
import org.ysb33r.gradle.iac.base.tf.RemoteStateVarProvider;

import java.util.Map;

/**
 * Describes variables for Terraform and OpenTofu.
 */
public interface VariablesSpec {
    /**
     * Adds one variable.
     *
     * <p> This will replace any previous entry by the same name.
     *
     * @param name  Name of variable.
     * @param value Lazy-evaluated form of variable. Anything resolvable via
     *              {@link org.ysb33r.grolifant5.api.core.StringTools#stringize(Object)}
     *              is accepted.
     */
    void var(final String name, final Object value);

    /**
     * Adds a map as a variable.
     *
     * <p> This will replace any previous entry by the same name.
     *
     * @param name Name of variable.
     * @param map  Lazy-evaluated forms of variable.
     *             Anything resolvable via {@link org.ysb33r.grolifant5.api.core.StringTools#stringizeValues}
     *             is accepted.
     */
    void map(Map<String, ?> map, final String name);

    /**
     * Adds a map provider as a variable.
     *
     * <p> This will replace any previous map by the same name.
     *
     * @param name        Name of map
     * @param mapProvider Provider to map
     */
    void map(final String name, Provider<Map<String, ?>> mapProvider);

    /**
     * Adds a list as a variable.
     *
     * <p> This will replace any previous entry by the same name.
     *
     * @param name Name of variable.
     * @param val1 First
     * @param vals Lazy-evaluated forms of variable. Anything resolvable via
     *             {@link org.ysb33r.grolifant5.api.core.StringTools#stringize} is accepted.
     */
    void list(final String name, Object val1, Object... vals);

    /**
     * Adds a list as a variable.
     *
     * <p> This will replace any previous entry by the same name.
     *
     * @param name Name of variable.
     * @param vals Lazy-evaluated forms of variable. Anything resolvable via
     *             {@link org.ysb33r.grolifant5.api.core.StringTools#stringize} is accepted.
     */
    void list(final String name, Iterable<?> vals);

    /**
     * Adds a name of a file containing {@code terraform} variables.
     *
     * @param fileName Files that can be converted via
     *                 {@link org.ysb33r.grolifant5.api.core.StringTools#stringize} and resolved relative to
     *                 the appropriate source set.
     *
     * @deprecated USe {@link #files} instead.
     */
    @Deprecated
    default void file(final Object fileName) {
        files(fileName);
    }

    /**
     * Adds one or more file names containing {@code terraform} variables.
     *
     * @param fileNames Files that can be converted via
     *                 {@link org.ysb33r.grolifant5.api.core.StringTools#stringize} and resolved relative to
     *                 the appropriate source set.
     */
    void files(final Object... fileNames);

    /**
     * Adds additional actions which can add variables. These will be called first when evaluating a final escaped
     * variable map.
     *
     * @param additionalVariables Action that can be called to provide additional variables.
     */
    void provider(Action<VariablesSpec> additionalVariables);

    /**
     * Configure the injection of a map for {@code terraform_remote_state} usage.
     *
     * @param configurator Configuration action.
     */
    void remoteStateMap(Action<RemoteStateVarProvider> configurator);

    /**
     * Configure the injection of a map for {@code terraform_remote_state} usage.
     *
     * @param configurator Configuration closure.
     */
    void remoteStateMap(@DelegatesTo(RemoteStateVarProvider.class) Closure<?> configurator);

    /**
     * Provide access to a remote state configuration.
     *
     * @return Instance implemting {@link RemoteStateVarProvider}.
     */
    RemoteStateVarProvider getRemoteStateMap();
}
