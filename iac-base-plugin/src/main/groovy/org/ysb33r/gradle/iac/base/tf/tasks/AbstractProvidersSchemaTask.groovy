/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.iac.base.tf.tasks

import groovy.transform.CompileStatic
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.gradle.api.tasks.OutputFile
import org.ysb33r.gradle.iac.base.internal.tf.TaskUtils
import org.ysb33r.gradle.iac.base.tf.UsesSourceSetName
import org.ysb33r.gradle.iac.base.tf.execspec.IacTfExecSpec

/**
 * Equivalent of {@code providers schema}.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class AbstractProvidersSchemaTask extends AbstractProviderTask implements UsesSourceSetName {

    protected AbstractProvidersSchemaTask(
        Class<? extends IacTfExecSpec> execSpecClass
    ) {
        super('schema', execSpecClass)
        this.outputFile = project.objects.property(File)
        this.sourceSetName = project.objects.property(String)
        this.outputFile.set(reportsDir.zip(sourceSetName) { dir, ssName ->
            new File(dir, "${ssName}.schema.json")
        })
        captureStdOutTo(this.outputFile)
        cmdArgs(TaskUtils.JSON_FORMAT)
    }

    /**
     * Sets the source set name.
     *
     * @param ssName Source set name.
     */
    @Override
    void setSourceSetName(String ssName) {
        this.sourceSetName.set(ssName)
    }

    /**
     * Get the location where the report file needs to be generated.
     *
     * @return File provider
     */
    @OutputFile
    Provider<File> getSchemaOutputFile() {
        this.outputFile
    }

    @Override
    void exec() {
        super.exec()
        URI fileLocation = stringTools().urize(this.outputFile)
        logger.lifecycle(
            "The textual representation of the plan file has been generated into ${fileLocation}"
        )
    }

    private final Property<File> outputFile
    private final Property<String> sourceSetName
}
