/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.iac.base.tf.execspec

import groovy.transform.CompileStatic
import org.gradle.api.logging.configuration.ConsoleOutput
import org.ysb33r.grolifant5.api.core.ConfigCacheSafeOperations

import java.util.function.Function

import static java.util.Collections.EMPTY_LIST

/**
 * Build default arguments for specific commands.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class DefaultArguments {

    /** Command-line parameter for no colour.
     *
     */
    public static final String NO_COLOR = '-no-color'

    /** Command-line parameter for JSON output.
     *
     */
    public static final String JSON_FORMAT = '-json'

    static Builder builder() {
        new Builder()
    }

    static class Builder {
        List<String> build(ConfigCacheSafeOperations ccso) {
            options.collectMany { functor ->
                functor.apply(ccso)
            }.asImmutable()
        }

        /**
         * The command supports {@code input}.
         */
        Builder supportsInputs() {
            options.add({ ['-input=false'] } as Function<ConfigCacheSafeOperations, List<String>>)
            this
        }

        /**
         * The command supports {@code auto-approve}
         */
        Builder supportsAutoApprove(boolean auto = true) {
            if (auto) {
                options.add({ ['-auto-approve'] } as Function<ConfigCacheSafeOperations, List<String>>)
            }
            this
        }

        /**
         * The command supports {@code yes}
         */
        Builder supportsYes() {
            options.add({ ['-yes'] } as Function<ConfigCacheSafeOperations, List<String>>)
            this
        }

        /**
         * The command supports {@code force}
         */
        Builder supportsForce() {
            options.add({ ConfigCacheSafeOperations ccso ->
                ccso.projectTools().rerunTasks ? ['-force'] : EMPTY_LIST
            } as Function<ConfigCacheSafeOperations, List<String>>)
            this
        }

        /**
         * To be called from tasks where the command supports {@code no-color}.
         *
         * <p> Will get set if {@code --console=plain was provided to Gradle}
         *
         * @param withColor If set to {@code false}, the task will always run without color output.
         */
        Builder supportsColor(boolean withColor = true) {
            options.add({ ConfigCacheSafeOperations ccso ->
                final mode = ccso.projectTools().consoleOutput
                if (mode == ConsoleOutput.Plain ||
                    mode == ConsoleOutput.Auto && System.getenv('TERM') == 'dumb' ||
                    !withColor
                ) {
                    [NO_COLOR]
                } else {
                    EMPTY_LIST
                }
            } as Function<ConfigCacheSafeOperations, List<String>>)
            this
        }

        /**
         * With arbitrary additional parameters.
         *
         * @param args Additional command-line parameters
         */
        void withAdditionalParams(String... args) {
            options.add({ args.toList() } as Function<ConfigCacheSafeOperations, List<String>>)
            this
        }

        private Builder() {
        }

        private final List<Function<ConfigCacheSafeOperations, List<String>>> options = []
    }
}
