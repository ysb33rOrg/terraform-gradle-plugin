/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.iac.base.tf.tasks

import groovy.transform.CompileStatic
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.gradle.api.tasks.TaskAction
import org.ysb33r.grolifant5.api.core.runnable.GrolifantDefaultTask

import java.time.LocalDateTime

/**
 * Generates repository details to a file.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class GlobalConfigDetailsGenerator extends GrolifantDefaultTask {

    public static final String RC_ENTRY = 'rc'
    public static final String PLUGIN_DIR_ENTRY = 'plugin-dir'
    public static final String PLUGIN_DIR_TIMEOUT_ENTRY = 'plugin-dir-timeout-ms'

    GlobalConfigDetailsGenerator() {
        this.outputFile = project.objects.property(File)
        this.location = project.objects.property(String)
        this.pluginCacheDir = project.objects.property(String)
        this.pluginCacheDirTimeout = project.objects.property(Integer)
        inputs.property('location', this.location)
        inputs.property('plugins', this.pluginCacheDir)
        inputs.property('timeout', this.pluginCacheDirTimeout)
        outputs.file(this.outputFile)
    }

    void setOutputFile(Provider<File> out) {
        this.outputFile.set(out)
    }

    void setLocation(Provider<File> location) {
        this.location.set(location.map { it.absolutePath })
    }

    void setPluginCacheDir(Provider<File> dir) {
        this.pluginCacheDir.set(dir.map { it.absolutePath })
    }

    void setPluginCacheTimeout(Provider<Integer> value) {
        this.pluginCacheDirTimeout.set(value)
    }

    @TaskAction
    void exec() {
        final props = new Properties()
        props[RC_ENTRY] = location.get()
        props[PLUGIN_DIR_ENTRY] = pluginCacheDir.get()
        props[PLUGIN_DIR_TIMEOUT_ENTRY] = pluginCacheDirTimeout.get().toString()
        outputFile.get().withWriter { w -> props.store(w, LocalDateTime.now().toString()) }
    }

    private final Property<File> outputFile
    private final Property<String> location
    private final Property<String> pluginCacheDir
    private final Property<Integer> pluginCacheDirTimeout
}
