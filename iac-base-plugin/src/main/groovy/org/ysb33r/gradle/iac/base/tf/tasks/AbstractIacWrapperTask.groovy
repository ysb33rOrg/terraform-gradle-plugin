/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.iac.base.tf.tasks

import groovy.transform.CompileStatic
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.gradle.api.tasks.TaskProvider
import org.ysb33r.grolifant5.api.core.wrappers.AbstractWrapperGeneratorTask

/**
 * Script wrapper generator task.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class AbstractIacWrapperTask extends AbstractWrapperGeneratorTask {
    void associateCacheTask(TaskProvider<? extends AbstractIacCacheBinaryTask> ct) {
        inputs.files(ct)
        this.locationPropertiesFile.set(ct.flatMap { it.locationPropertiesFile })
        this.cacheTaskName.set(ct.map { it.name })
    }

    @Override
    protected String getBeginToken() {
        TEMPLATE_TOKEN_DELIMITER
    }

    @Override
    protected String getEndToken() {
        TEMPLATE_TOKEN_DELIMITER
    }

    @SuppressWarnings('UnnecessaryCast')
    protected AbstractIacWrapperTask(
        String appBaseName,
        String templateResourcePath,
        final Map<String, String> templateMapping
    ) {
        useWrapperTemplatesInResources(templateResourcePath, templateMapping)

        this.locationPropertiesFile = project.objects.property(File)
        this.cacheTaskName = project.objects.property(String)

        final root = fsOperations().projectRootDir
        this.tokenValues = locationPropertiesFile.zip(cacheTaskName) { lpf, cacheTaskName ->
            [
                APP_BASE_NAME               : appBaseName,
                GRADLE_WRAPPER_RELATIVE_PATH: fsOperations().relativePathNotEmpty(root),
                DOT_GRADLE_RELATIVE_PATH    : fsOperations().relativePath(lpf.parentFile),
                APP_LOCATION_FILE           : lpf.name,
                CACHE_TASK_NAME             : cacheTaskName
            ] as Map<String, String>
        }
    }

    @Override
    protected Map<String, String> getTokenValuesAsMap() {
        this.tokenValues.get()
    }

    private final Provider<Map<String, String>> tokenValues
    private final Property<File> locationPropertiesFile
    private final Property<String> cacheTaskName

    private static final String TEMPLATE_TOKEN_DELIMITER = '~~'
}
