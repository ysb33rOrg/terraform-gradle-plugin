/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.iac.base.tf.config.multilevel

import groovy.transform.CompileStatic
import org.gradle.api.Project
import org.gradle.api.provider.Provider

import javax.inject.Inject

/**
 * Configure formatting of code.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class CodeFormatting implements ExecutionConfiguration {
    /**
     * Whether recursive formatting should be enabled
     */
    boolean recursive = false

    final Provider<List<String>> commandLineArgs
    final Provider<List<String>> tfVars
    final Provider<String> inputProperty

    @Inject
    CodeFormatting(Project project) {
        this.commandLineArgs = project.provider { ->
            recursive ? ['-recursive'] : Collections.EMPTY_LIST
        }
        this.tfVars = project.provider { -> Collections.EMPTY_LIST }
        this.inputProperty = project.provider { -> recursive.toString() }
    }
}
