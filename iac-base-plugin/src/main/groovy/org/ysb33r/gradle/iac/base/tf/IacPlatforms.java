/**
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.iac.base.tf;

import org.gradle.api.provider.Provider;

import java.util.Set;

/**
 * Defines the platforms that the tool is supported on.
 *
 * @author Schalk W. Cronjé
 * @since 2.0
 */
public interface IacPlatforms {

    /**
     * Standard set of supported platforms.
     *
     * @return The set of provider platforms supported at the time the plugin was released.
     */
    Set<String> getAllPlatforms();

    /**
     * Add one or more platforms to support for providers.
     *
     * Use {@link #getAllPlatforms} to add all platforms supported bu this plugin.
     *
     * @param reqPlatforms Platforms to add.
     */
    void platforms(Iterable<String> reqPlatforms);

    /**
     * Add one or more platforms to support for providers.
     *
     * Use {@link #getAllPlatforms} to add all platforms supported bu this plugin.
     *
     * @param reqPlatforms Platforms to add.
     */
    void platforms(String... reqPlatforms);

    /**
     * Provide the list of platforms that need to be supported.
     *
     * If empty, only the current platform will be supported.
     *
     * @return List of supported platforms.
     */
    Provider<Set<String>> getPlatforms();
}
