/**
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.iac.base.tf;

import org.gradle.api.provider.Provider;

/**
 * Manages the injection of a variable containing variables related to remote state, so that they can be used by
 * {@code terraform_remote_state} data sources.
 *
 * @author Schalk W. Cronjé
 * @since 2.0
 */
public interface RemoteStateVarProvider {

    /**
     * Name of the variable.
     *
     * <p>The default is {@code remote_state}</p>
     *
     * @param varName Override name.
     */
    void setVarName(String varName);

    /**
     * Whether tokens should be bundled in a map variable.
     *
     * @param asVar {@code true} Whether to inject the variable
     */
    void setInjectVar(boolean asVar);

    /**
     * A provider to indicate whether remote state tokens should be added as a map variable.
     *
     * @return Decision provider.
     */
    Provider<Boolean> getInjectVar();
}
