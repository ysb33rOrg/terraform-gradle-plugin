/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.iac.base.tf.tasks

import groovy.transform.CompileStatic
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.gradle.api.tasks.options.Option
import org.ysb33r.gradle.iac.base.errors.IacConfigurationException
import org.ysb33r.gradle.iac.base.internal.tf.DefaultUsesVariables
import org.ysb33r.gradle.iac.base.tf.config.multilevel.UsesVariables
import org.ysb33r.gradle.iac.base.tf.execspec.DefaultArguments
import org.ysb33r.gradle.iac.base.tf.execspec.IacTfExecSpec

/**
 * Equivalent of {@code terraform import}.
 *
 * Should be used with command-line arguments {@code --type}, {@code --name} and {@code --id}.
 *
 * @since 0.1
 */
@CompileStatic
class AbstractImportTask extends AbstractTfStandardTask implements UsesVariables {

    protected AbstractImportTask(
        String workspaceName,
        Class<? extends IacTfExecSpec> execSpecClass
    ) {
        super(
            'import',
            workspaceName,
            execSpecClass,
            DefaultArguments.builder().supportsColor().supportsInputs()
        )

        alwaysOutOfDate()
        this.varsFile = new DefaultUsesVariables(execSpec, this)

        this.resourceId = project.objects.property(String)
        this.resourcePath = project.objects.property(String)
        this.resources = this.resourcePath.zip(this.resourceId) { x, y -> [x, y] }
    }

    @Option(option = 'id', description = 'Identifier of resource to import')
    void setResourceIdentifier(String id) {
        this.resourceId.set(id)
    }

    @Option(option = 'path', description = 'Resource path to import')
    void setResourcePath(String id) {
        this.resourcePath.set(id)
    }

    @Override
    void exec() {
        if (!resourceId.present || !resourcePath.present) {
            throw new IacConfigurationException('import requires both --path and --id')
        }
        execSpec.cmd {
            addCommandLineArgumentProviders(resources)
        }
        super.exec()
    }

    private final Property<String> resourcePath
    private final Property<String> resourceId
    private final Provider<List<String>> resources

    @Delegate(includes = ['getVariablesFiles', 'setVariablesFiles'])
    private final UsesVariables varsFile
}
