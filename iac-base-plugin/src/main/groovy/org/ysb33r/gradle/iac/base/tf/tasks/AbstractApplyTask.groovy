/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.iac.base.tf.tasks

import groovy.transform.CompileStatic
import org.gradle.api.Task
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.gradle.api.specs.Spec
import org.gradle.api.tasks.options.Option
import org.ysb33r.gradle.iac.base.tf.ResourceFilter
import org.ysb33r.gradle.iac.base.tf.execspec.DefaultArguments
import org.ysb33r.gradle.iac.base.tf.execspec.IacTfExecSpec
import org.ysb33r.grolifant5.api.core.StringTools

import java.time.LocalDateTime

import static java.util.Collections.EMPTY_LIST
import static org.ysb33r.gradle.iac.base.internal.tf.TaskUtils.JSON_FORMAT

/**
 * Equivalent of {@code apply}.
 *
 * @since 2.0
 */
@CompileStatic
class AbstractApplyTask extends AbstractTfStandardTask implements ResourceFilter {

    /**
     * Select specific resources.
     *
     * @param resourceNames List of resources to target.
     */
    @Option(option = 'target', description = 'List of resources to target')
    @Override
    void setTargets(List<String> resourceNames) {
        project.tasks.named(planTask, AbstractPlanTask).configure { it.targets = resourceNames }
    }

    /**
     * Mark resources to be replaced.
     *
     * @param resourceNames List of resources to target.
     */
    @Option(option = 'replace', description = 'List of resources to replace')
    @Override
    void setReplacements(List<String> resourceNames) {
        project.tasks.named(planTask, AbstractPlanTask).configure { it.replacements = resourceNames }
    }

    /**
     * Output progress in json as per https://www.terraform.io/docs/internals/machine-readable-ui.html
     *
     * @param state Set to {@code true} to output in JSON.
     */
    @Option(option = 'json', description = 'Output progress in JSON')
    void setJson(boolean state) {
        this.useJson.set(state)
    }

    protected AbstractApplyTask(
        String planTaskName,
        String workspaceName,
        String defaultWorkspaceName,
        Class<? extends IacTfExecSpec> execSpecClass,
        boolean isDestroy
    ) {
        super(
            'apply',
            workspaceName,
            execSpecClass,
            DefaultArguments.builder().supportsColor().supportsInputs()
        )
        final linkedTask = project.tasks.named(planTaskName, AbstractPlanTask)
        final ws = workspaceName == defaultWorkspaceName ? '' : ".${workspaceName}"
        this.planTask = planTaskName
        this.useJson = project.objects.property(Boolean)
        this.useJson.convention(false)
        this.approved = project.objects.property(Boolean)
        this.approved.convention(!isDestroy)
        this.tracker = dataDir.map { new File(it, ".applied-plan${ws}.sha256") }
        this.applyStateFile = dataDir.map { new File(it, isDestroy ? ".destroy${ws}.txt" : ".apply${ws}.txt") }
        this.planFile = linkedTask.flatMap { it.planOutputFile }

        if (isDestroy) {
            cmdArgs('-destroy')
        }

        execSpec.cmd {
            addCommandLineArgumentProviders(
                this.useJson.map { it ? [JSON_FORMAT] : EMPTY_LIST },
                this.approved.map { it ? ['-auto-approve'] : EMPTY_LIST }
            )
        }

        inputs.property('tracker', this.tracker.map {
            if (it.exists()) {
                it.text
            } else {
                final pf = planFile.get()
                if (pf.exists()) {
                    pf.text.sha256()
                } else {
                    StringTools.EMPTY
                }
            }
        })

        inputs.files(linkedTask)
        inputs.file(this.planFile)
        outputs.file(this.applyStateFile)
        dependsOn(planTaskName)

        outputs.upToDateWhen( new Spec<Task>() {
            @Override
            boolean isSatisfiedBy(Task task) {
                !hasTaintedResources()
            }
        })
    }

    /**
     * Ensures that the plan file will be last on the command-line.
     */
    @Override
    protected void exec() {
        execSpec.cmd {
            addCommandLineArgumentProviders(planFile.map { [it.absolutePath] })
        }
        super.exec()

        final trackerStamp = tracker.get()
        trackerStamp.parentFile.mkdirs()
        trackerStamp.text = planFile.get().text.sha256()
        applyStateFile.get().text = "${LocalDateTime.now()}"
        removeAllTaintedResources()
    }

    /**
     * Explicitly approve the change.
     *
     * This can be called by a derived task to approve the change.
     */
    protected void approveChange() {
        this.approved.set(true)
    }

    private final String planTask
    private final Property<Boolean> approved
    private final Property<Boolean> useJson
    private final Provider<File> planFile
    private final Provider<File> tracker
    private final Provider<File> applyStateFile
}

