/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.iac.base.tf.config.multilevel

import groovy.transform.CompileStatic
import org.gradle.api.Project
import org.gradle.api.provider.Provider
import org.ysb33r.grolifant5.api.core.StringTools

import javax.inject.Inject

/**
 * Allows for lock configurations on a task.
 *
 */
@CompileStatic
class Lock implements ExecutionConfiguration {
    /**
     * Whether locking should be enabled
     */
    boolean enabled = true

    /**
     * Timeout in seconds of the lock.
     */
    Integer timeout = 0

    final Provider<List<String>> commandLineArgs
    final Provider<List<String>> tfVars
    final Provider<String> inputProperty

    @Inject
    Lock(Project project) {
        this.commandLineArgs = project.provider { ->
            ["-lock=${enabled}", "-lock-timeout=${timeout}s"]*.toString()
        }
        this.tfVars = project.provider { -> Collections.EMPTY_LIST }
        this.inputProperty = project.provider { -> StringTools.EMPTY }
    }
}
