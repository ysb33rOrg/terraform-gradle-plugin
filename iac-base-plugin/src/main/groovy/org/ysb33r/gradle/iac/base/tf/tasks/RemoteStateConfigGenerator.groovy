/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.iac.base.tf.tasks

import groovy.transform.CompileStatic
import org.gradle.api.provider.MapProperty
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.gradle.api.tasks.CacheableTask
import org.gradle.api.tasks.Internal
import org.gradle.api.tasks.OutputFile
import org.gradle.api.tasks.TaskAction
import org.ysb33r.gradle.iac.base.tf.backends.TokenValue
import org.ysb33r.grolifant5.api.core.runnable.GrolifantDefaultTask

import static java.util.Collections.EMPTY_MAP

/**
 * Generates a remote state file containing partial configuration for backend.
 *
 * @author Schalk W. Cronjé
 *
 */
@CompileStatic
@CacheableTask
class RemoteStateConfigGenerator extends GrolifantDefaultTask {

    RemoteStateConfigGenerator() {
        this.destDir = project.objects.property(File)
        this.tokens = project.objects.mapProperty(String, String)
        this.outputFile = destDir.map { new File(it, 'terraform-backend-config.tf') }
        this.hasTokens = this.tokens.map { (Boolean) it.isEmpty() }
        inputs.property('token', this.tokens).optional(true)
    }

    /**
     * Override the output directory.
     *
     * @param dir Anything convertible to a file path.
     */
    void setDestinationDir(Object dir) {
        fsOperations().updateFileProperty(this.destDir, dir)
    }

    /** The output directory for the configuration file
     *
     * @return
     */
    @Internal
    Provider<File> getDestinationDir() {
        this.destDir
    }

    /**
     * The location of the backend configuration file.
     *
     * @return Configuration file.
     */
    @OutputFile
    Provider<File> getBackendConfigFile() {
        this.outputFile
    }

    /**
     * Adds a provider of tokens.
     *
     * These providers are processed before any of the customisations on the backend provider.
     *
     * @param tp Addition provider of tokens
     */
    void setTokenProvider(Provider<Map<String, TokenValue>> tp) {
        this.tokens.set(tp.map { stringTools().stringizeValues(it) })
    }

    @TaskAction
    void exec() {
        final entries = this.tokens.getOrElse(EMPTY_MAP)
        Integer maxLength = entries*.key*.toString()*.size().max()
        outputFile.get().withWriter { w ->
            entries.each { k, v ->
                w.println("${k.padRight(maxLength)} = ${v}")
            }
        }
    }

    private final Property<File> destDir
    private final Provider<File> outputFile
    private final Provider<Boolean> hasTokens
    private final MapProperty<String, String> tokens
}
