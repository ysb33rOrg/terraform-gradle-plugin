/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.iac.base.internal.tf

import groovy.transform.CompileStatic
import org.gradle.api.Task
import org.gradle.api.provider.ListProperty
import org.gradle.api.provider.Provider
import org.ysb33r.gradle.iac.base.tf.config.multilevel.UsesVariables
import org.ysb33r.gradle.iac.base.tf.execspec.IacTfExecSpec

/**
 * A proxy class for implementing the {@link UsesVariables}.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class DefaultUsesVariables implements UsesVariables, Serializable {

    DefaultUsesVariables(IacTfExecSpec spec, Task task) {
        this.varsFile = task.project.objects.listProperty(File)

        spec.cmd {
            args(varsFile.map { list ->
                list.collect {
                    "-var-file=${it.absolutePath}"
                }
            })
        }

        task.inputs.files(this.varsFile)
    }

    /**
     * The location of the variables files.
     *
     * @return Location provider
     */
    @Override
    Provider<List<File>> getVariablesFiles() {
        this.varsFile
    }

    /**
     * Sets the location of the variables file.
     *
     * @param file Location provider.
     */
    @Override
    void setVariablesFiles(Provider<List<File>> file) {
        this.varsFile.set(file)
    }

    private final ListProperty<File> varsFile
}
