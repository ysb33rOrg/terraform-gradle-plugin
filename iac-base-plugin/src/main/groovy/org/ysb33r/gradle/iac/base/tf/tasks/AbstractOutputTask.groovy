/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.iac.base.tf.tasks

import groovy.transform.CompileStatic
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.Optional
import org.gradle.api.tasks.OutputFile
import org.gradle.api.tasks.options.Option
import org.ysb33r.gradle.iac.base.errors.IacConfigurationException
import org.ysb33r.gradle.iac.base.tf.UsesSourceSetName
import org.ysb33r.gradle.iac.base.tf.execspec.DefaultArguments
import org.ysb33r.gradle.iac.base.tf.execspec.IacTfExecSpec

import static java.util.Collections.EMPTY_LIST
import static org.ysb33r.gradle.iac.base.internal.tf.TaskUtils.JSON_FORMAT

/**
 * Equivalent of {@code output}.
 *
 * Could be used with command-line arguments {@code --raw} and {@code --json}.
 *
 * @since 0.9.0
 */
@CompileStatic
class AbstractOutputTask extends AbstractTfStandardTask implements UsesSourceSetName {
    /**
     * Whether output should be in JSON
     *
     * This option can be set from the command-line with {@code --json}.
     *
     * Mutually exclusive with {@link #setRaw}.
     *
     * @param flag Whether to display JSON.
     */
    @Option(option = 'json', description = 'Force output to be in JSON format')
    void setJson(boolean flag) {
        this.json.set(flag)
    }

    /**
     * Whether output should contain raw variable content.
     *
     * This option can be set from the command-line with {@code --raw}.
     *
     * Mutually exclusive with {@link #setJson}.
     *
     * @param flag Whether to display raw.
     */
    @Option(option = 'raw', description = 'Use raw string representation')
    void setRaw(boolean flag) {
        this.raw.set(flag)
    }

    /**
     * List of names to retrieve output for.
     *
     * @return Name of variable. Can be {@code null} to retrieve all names.
     */
    @Input
    @Optional
    Provider<String> getOutputName() {
        this.varName
    }

    /**
     * Sets the source set name
     * @param ssName Source set name.
     */
    @Override
    void setSourceSetName(String ssName) {
        this.sourceSetName.set(ssName)
    }

    /**
     * Set the list of output names to retrieve.
     *
     * This option can be set on the command-line with {@code --name}
     *
     * @param outputName Output name to use.
     */
    @Option(option = 'name', description = 'Name of output variable')
    void setName(String outputName) {
        this.varName.set(outputName)
    }

    /** Get the location where the report file needs to be generated.
     *
     * @return File provider
     */
    @OutputFile
    Provider<File> getStatusReportOutputFile() {
        this.outputFile
    }

    @Override
    void exec() {
        // We need to set the var name here to ensure it is the last thing on the cmdline.
        // ????
        if (varName.present) {
            execSpec.cmd {
                addCommandLineArgumentProviders(varName.map { [it] })
            }
        }

        super.exec()
        URI fileLocation = stringTools().urize(statusReportOutputFile)
        logger.lifecycle(
            "The textual representation of the plan file has been generated into ${fileLocation}"
        )
    }

    protected AbstractOutputTask(
        String workspaceName,
        String defaultWorkspaceName,
        Class<? extends IacTfExecSpec> execSpecClass
    ) {
        super(
            'output',
            workspaceName,
            execSpecClass,
            DefaultArguments.builder().supportsColor()
        )

        this.varName = project.objects.property(String)
        this.sourceSetName = project.objects.property(String)
        this.json = project.objects.property(Boolean)
        this.raw = project.objects.property(Boolean)
        this.json.set(false)
        this.raw.set(false)

        final String ws = workspaceName == defaultWorkspaceName ? '' : ".${workspaceName}"
        this.outputFile = sourceSetName.zip(json) { baseName, jsonMode ->
            "${baseName}${ws}.outputs.tf${jsonMode ? '.json' : ''}"
        }.zip(reportsDir) { fileName, dir ->
            new File(dir, fileName)
        }

        captureStdOutTo(outputFile)

        execSpec.cmd {
            commandLineArgumentProviders.add(json.zip(raw) { j, r ->
                if (j && r) {
                    throw new IacConfigurationException('--raw && --json are mutually exclusive')
                }
                r ? ['-raw'] : (j ? [JSON_FORMAT] : EMPTY_LIST)
            })
        }
    }

    private final Property<String> varName
    private final Provider<File> outputFile
    private final Property<String> sourceSetName
    private final Property<Boolean> json
    private final Property<Boolean> raw
}
