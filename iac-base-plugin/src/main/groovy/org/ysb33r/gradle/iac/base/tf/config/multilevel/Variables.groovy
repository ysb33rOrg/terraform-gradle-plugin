/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.iac.base.tf.config.multilevel

import groovy.transform.CompileStatic
import org.gradle.api.Action
import org.gradle.api.Project
import org.gradle.api.provider.Provider
import org.ysb33r.gradle.iac.base.tf.RemoteStateVarProvider
import org.ysb33r.gradle.iac.base.tf.backends.TokenValue
import org.ysb33r.grolifant5.api.core.ClosureUtils
import org.ysb33r.grolifant5.api.core.ConfigCacheSafeOperations

import javax.inject.Inject

/**
 * A configuration building block for tasks that need to pass variables to
 * a {@code terraform/opentofu task}.
 *
 */
@CompileStatic
class Variables implements VariablesSpec {
    /**
     * Attach this configuration block to a Terraform extension or source directory set
     *
     * @param rootFileResolver Root file resolver for file that are referenced.
     * @param backendTokens Provider to backend tokens.
     * @param project Associated project.
     *
     * @since 2.0
     */
    @Inject
    Variables(
        Provider<File> rootFileResolver,
        Provider<Map<String, TokenValue>> backendTokens,
        Project project
    ) {
        this(
            rootFileResolver,
            backendTokens,
            ConfigCacheSafeOperations.from(project)
        )
    }

    /**
     * Attach this configuration block to a Terraform extension or source directory set
     *
     * @param rootFileResolver Root file resolver for file that are referenced.
     * @param backendTokens Provider to backend tokens.
     * @param ccso Configuration cache-safe operations.
     *
     * @since 2.0
     */
    Variables(
        Provider<File> rootFileResolver,
        Provider<Map<String, TokenValue>> backendTokens,
        ConfigCacheSafeOperations ccso
    ) {
        this.rootDirResolver = rootFileResolver
        this.ccso = ccso
        this.varsFilesPair = new VarsFilesPair(this.ccso)
        this.declaredFilesProvider = this.varsFilesPair.resolvedFiles
        this.remoteStateVarProvider = new DefaultRemoteStateVarProvider(backendTokens, ccso)
        this.escapedVarsProvider = this.varsFilesPair.escapedVarsProvider
            .zip(this.remoteStateVarProvider.remoteVarEscaped) { vars, remote ->
                vars + remote
            }
    }

    /**
     * Constructs instance from definition of files and variables
     *
     * @param vfp Definition of files and variables
     * @param rootFileResolver Root file resolver for file that are referenced.
     * @param backendTokens Provider to backend tokens.
     * @param ccso Configuration cache-safe operations.
     *
     * @since 2.0
     */
    Variables(
        VarsFilesPair vfp,
        Provider<File> rootFileResolver,
        Provider<Map<String, TokenValue>> backendTokens,
        ConfigCacheSafeOperations ccso
    ) {
        this(rootFileResolver, backendTokens, ConfigCacheSafeOperations.from(ccso))
        vfp.copyTo(this.varsFilesPair)
    }

    /**
     * Adds one variable.
     *
     * <p> This will replace any previous entry by the same name.
     *
     * @param name Name of variable.
     * @param value Lazy-evaluated form of variable. Anything resolvable via
     * {@link org.ysb33r.grolifant5.api.core.StringTools#stringize(Object)}
     * is accepted.
     */
    @Override
    void var(final String name, final Object value) {
        varsFilesPair.vars.put(name, TokenValue.of(value, ccso))
    }

    /** Adds a map as a variable.
     *
     * <p> This will replace any previous entry by the same name.
     *
     * @param name Name of variable.
     * @param val1 First
     * @param map Lazy-evaluated form of map.
     *  Anything resolvable via {@link org.ysb33r.grolifant5.api.core.StringTools#stringizeValues(Map)}
     * is accepted.
     */
    @Override
    void map(Map<String, ?> map, final String name) {
        varsFilesPair.vars.put(name, TokenValue.of(map, ccso))
    }

    /**
     * Adds a map provider as a variable.
     *
     * <p> This will replace any previous map by the same name.
     *
     * @param name Name of map
     * @param mapProvider Provider to map
     */
    @Override
    void map(String name, Provider<Map<String, ?>> mapProvider) {
        varsFilesPair.vars.put(name, TokenValue.of(mapProvider, ccso))
    }

    /** Adds a list as a variable.
     *
     * <p> This will replace any previous entry by the same name.
     *
     * @param name Name of variable.
     * @param val1 First
     * @param vals Lazy-evaluated forms of variable. Anything resolvable via
     * {@link org.ysb33r.grolifant5.api.core.StringTools#stringize(Iterable <?>)} is accepted.
     */
    @Override
    void list(final String name, Object val1, Object... vals) {
        List<Object> inputs = [val1]
        inputs.addAll(vals)
        varsFilesPair.vars.put(name, TokenValue.of(inputs, ccso))
    }

    /** Adds a list as a variable.
     *
     * <p> This will replace any previous entry by the same name.
     *
     * @param name Name of variable.
     * @param vals Lazy-evaluated forms of variable. Anything resolvable via
     * {@link org.ysb33r.grolifant5.api.core.StringTools#stringize(Iterable <?>)} is accepted.
     */
    @Override
    void list(final String name, Iterable<?> vals) {
        varsFilesPair.vars.put(name, TokenValue.of(vals.toList(), ccso))
    }

    /** Adds a name of a file containing {@code terraform} variables.
     *
     * @param fileNames Files that can be converted.
     */
    @Override
    void files(final Object... fileNames) {
        fileNames.each { fileName -> varsFilesPair.files.add fileName }
    }

    /**
     * Removes all existing variables and file references.
     */
    void clear() {
        varsFilesPair.clear()
    }

    @Override
    String toString() {
        "tfvars: ${this.varsFilesPair}"
    }

    /**
     * Adds additional actions which can add variables. These will be called first when evaluating a final escaped
     * variable map.
     *
     * @param additionalVariables Action that can be called to provide additional variables.
     *
     * @since 0.12
     */
    @Override
    void provider(Action<VariablesSpec> additionalVariables) {
        this.varsFilesPair.additionalVariables.add(additionalVariables)
    }

    /**
     * A list of of escaped variables.
     *
     * @return Provider.
     */
    Provider<List<String>> getTfVars() {
        this.escapedVarsProvider
    }

    /**
     * All provider to a list of files declared to contain variables.
     *
     * @return Provider
     */
    Provider<List<File>> getDeclaredFiles() {
        this.declaredFilesProvider
    }

    /**
     * Configure the injection of a map for {@code terraform_remote_state} usage.
     *
     * @param configurator Configuration action.
     *
     * @since 2.0
     */
    @Override
    void remoteStateMap(Action<RemoteStateVarProvider> configurator) {
        configurator.execute(this.remoteStateVarProvider)
    }

    /**
     * Configure the injection of a map for {@code terraform_remote_state} usage.
     *
     * @param configurator Configuration closure.
     *
     * @since 2.0
     */
    @Override
    @SuppressWarnings('UnnecessaryDotClass')
    void remoteStateMap(@DelegatesTo(RemoteStateVarProvider.class) Closure<?> configurator) {
        ClosureUtils.configureItem(this.remoteStateVarProvider, configurator)
    }

    /**
     * Provide access to a remote state configuration.
     *
     * @return Instance implementing {@link RemoteStateVarProvider}.
     *
     * @since 2.0
     */
    @Override
    RemoteStateVarProvider getRemoteStateMap() {
        this.remoteStateVarProvider
    }

    private final VarsFilesPair varsFilesPair
    private final Provider<File> rootDirResolver
    private final Provider<List<File>> declaredFilesProvider
    private final Provider<List<String>> escapedVarsProvider
    private final ConfigCacheSafeOperations ccso
    private final DefaultRemoteStateVarProvider remoteStateVarProvider
}
