/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.iac.base.internal.tf.tokentypes

import groovy.transform.CompileStatic
import org.gradle.api.model.ObjectFactory
import org.gradle.api.provider.Provider
import org.ysb33r.gradle.iac.base.tf.backends.TokenValue
import org.ysb33r.grolifant5.api.core.ProviderTools

import java.util.concurrent.Callable
import java.util.function.Supplier

import static org.ysb33r.grolifant5.api.core.StringTools.EMPTY

/**
 * Token that is lazy-evaluated.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class LazyEvaluatedToken implements TokenValue {

    LazyEvaluatedToken(Provider<?> lazy, ObjectFactory objects) {
        this.lazyValue = lazy.map {
            TokenHelpers.create(it, objects)
        }
    }

    LazyEvaluatedToken(Optional<?> lazy, ObjectFactory objects) {
        final opt = lazy.map { TokenHelpers.create(it, objects) }
        final provider = objects.property(String)
        provider.set(EMPTY)
        this.lazyValue = provider.map { opt.get() }
    }

    LazyEvaluatedToken(Supplier<?> lazy, ObjectFactory objects) {
        final provider = objects.property(String)
        provider.set(EMPTY)
        this.lazyValue = provider.map { TokenHelpers.create(lazy.get(), objects) }
    }

    LazyEvaluatedToken(Callable<?> lazy, ObjectFactory objects) {
        final provider = objects.property(String)
        provider.set(EMPTY)
        this.lazyValue = provider.map { TokenHelpers.create(lazy.call(), objects) }
    }

    LazyEvaluatedToken(Provider<?> lazy, ProviderTools objects) {
        this.lazyValue = lazy.map {
            TokenHelpers.create(it, objects)
        }
    }

    LazyEvaluatedToken(Optional<?> lazy, ProviderTools objects) {
        final opt = lazy.map { TokenHelpers.create(it, objects) }
        final provider = objects.property(String)
        provider.set(EMPTY)
        this.lazyValue = provider.map { opt.get() }
    }

    LazyEvaluatedToken(Supplier<?> lazy, ProviderTools objects) {
        final provider = objects.property(String)
        provider.set(EMPTY)
        this.lazyValue = provider.map { TokenHelpers.create(lazy.get(), objects) }
    }

    LazyEvaluatedToken(Callable<?> lazy, ProviderTools objects) {
        final provider = objects.property(String)
        provider.set(EMPTY)
        this.lazyValue = provider.map { TokenHelpers.create(lazy.call(), objects) }
    }

    @Override
    String getValue() {
        lazyValue.get().value
    }

    @Override
    String toString() {
        value
    }

    private final Provider<TokenValue> lazyValue
}
