/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.iac.base.internal.tf

import groovy.transform.CompileStatic
import org.gradle.api.provider.Provider
import org.ysb33r.gradle.iac.base.tf.PluginCache
import org.ysb33r.gradle.iac.base.tf.TfConfig
import org.ysb33r.gradle.iac.base.tf.tasks.GlobalConfigDetailsGenerator

/**
 * Helper class for converting task output into {@link TfConfig}.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class SubprojectConfigDetails implements TfConfig, PluginCache {
    SubprojectConfigDetails(Provider<File> location) {
        this.loc = location.map {
            final props = new Properties()
            it.withReader { reader -> props.load(reader) }
            new File(props[GlobalConfigDetailsGenerator.RC_ENTRY].toString())
        }
        this.pluginDir = location.map {
            final props = new Properties()
            it.withReader { reader -> props.load(reader) }
            new File(props[GlobalConfigDetailsGenerator.PLUGIN_DIR_ENTRY].toString())
        }
        this.timeout = location.map {
            final props = new Properties()
            it.withReader { reader -> props.load(reader) }
            props[GlobalConfigDetailsGenerator.PLUGIN_DIR_TIMEOUT_ENTRY] as Integer
        }
    }

    @Override
    Provider<File> getConfigFile() {
        this.loc
    }

    /**
     * Location of plugin cache directory.
     *
     * @return Location of cache directory as a file provider.
     */
    @Override
    Provider<File> getPluginCacheDir() {
       this.pluginDir
    }

    @Override
    Provider<Integer> getUseLockWithTimeout() {
        this.timeout
    }

    private final Provider<File> loc
    private final Provider<File> pluginDir
    private final Provider<Integer> timeout
}
