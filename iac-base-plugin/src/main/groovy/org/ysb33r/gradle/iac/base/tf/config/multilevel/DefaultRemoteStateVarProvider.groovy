/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.iac.base.tf.config.multilevel

import groovy.transform.CompileStatic
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.ysb33r.gradle.iac.base.tf.RemoteStateVarProvider
import org.ysb33r.gradle.iac.base.tf.backends.TokenValue
import org.ysb33r.grolifant5.api.core.ConfigCacheSafeOperations

import static java.util.Collections.EMPTY_LIST
import static java.util.Collections.EMPTY_MAP

/**
 * Implementation of {@linkRemoteStateVarProvider}.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class DefaultRemoteStateVarProvider implements RemoteStateVarProvider {

    final Provider<List<String>> remoteVarEscaped

    DefaultRemoteStateVarProvider(
        Provider<Map<String, TokenValue>> backendTokens,
        ConfigCacheSafeOperations ccso
    ) {
        this.injectVar = ccso.providerTools().property(Boolean).convention(false)
        this.varName = ccso.providerTools().property(String).convention('remote_state')

        final emptyProvider = ccso.providerTools().provider { -> EMPTY_LIST }
        this.remoteVarEscaped = injectVar.flatMap { use ->
            if (use) {
                backendTokens.orElse(EMPTY_MAP).zip(this.varName)  { tokens, name ->
                    final values = tokens.collect { k, v ->
                        "  ${k} = ${v.value}"
                    }*.toString()
                    ["${name} = {\n${values.join('\n')}\n}".toString()]
                }
            } else {
                emptyProvider
            }
        }
    }

    /**
     * Name of the variable.
     *
     * <p>The default is {@code remote_state}</p>
     *
     * @param varName Override name.
     */
    @Override
    void setVarName(String varName) {
        this.varName.set(varName)
    }

    /**
     * Whether tokens should be bundled in a map variable.
     *
     * @param asVar {@code true} Whether to inject the variable
     */
    @Override
    void setInjectVar(boolean asVar) {
        this.injectVar.set(asVar)
    }

    /**
     * A provider to indicate whether remote state tokens should be added as a map variable.
     *
     * @return Decision provider.
     */
    @Override
    Provider<Boolean> getInjectVar() {
        this.injectVar
    }

    private final Property<Boolean> injectVar
    private final Property<String> varName
}
