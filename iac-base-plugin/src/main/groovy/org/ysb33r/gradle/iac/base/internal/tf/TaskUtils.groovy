/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.iac.base.internal.tf

import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import org.gradle.api.Project
import org.gradle.api.attributes.Attribute
import org.gradle.api.provider.Provider
import org.gradle.api.services.BuildServiceSpec
import org.gradle.api.tasks.TaskProvider
import org.ysb33r.gradle.iac.base.tf.AbstractGlobalConfigExtension
import org.ysb33r.gradle.iac.base.tf.AbstractIacToolchain
import org.ysb33r.gradle.iac.base.tf.SourceSetExecutionEnvironment
import org.ysb33r.gradle.iac.base.tf.SourceSetSources
import org.ysb33r.gradle.iac.base.tf.backends.TokenValue
import org.ysb33r.gradle.iac.base.tf.config.multilevel.ExecutionOptions
import org.ysb33r.gradle.iac.base.tf.services.ConcurrencyProtector
import org.ysb33r.gradle.iac.base.tf.tasks.AbstractIacCacheBinaryTask
import org.ysb33r.gradle.iac.base.tf.tasks.AbstractIacWrapperTask
import org.ysb33r.gradle.iac.base.tf.tasks.AbstractTfBaseTask
import org.ysb33r.gradle.iac.base.tf.tasks.AbstractTfStandardTask
import org.ysb33r.gradle.iac.base.tf.tasks.GlobalConfigDetailsGenerator
import org.ysb33r.gradle.iac.base.tf.tasks.GlobalConfigGenerator
import org.ysb33r.gradle.iac.base.tf.tasks.PluginCacheDirGenerator
import org.ysb33r.gradle.iac.base.tf.tasks.RemoteStateConfigGenerator
import org.ysb33r.gradle.iac.base.tf.tasks.VariablesCacheGenerator
import org.ysb33r.grolifant5.api.core.OperatingSystem
import org.ysb33r.grolifant5.api.core.ProjectOperations

import java.nio.file.FileVisitResult
import java.nio.file.FileVisitor
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.attribute.BasicFileAttributes

import static java.nio.file.FileVisitResult.CONTINUE
import static java.nio.file.Files.readSymbolicLink

/**
 * General utilities for working with some tasks
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
@Slf4j
class TaskUtils {
    public static final String DEFAULT_SOURCESET_NAME = 'main'
    public static final String DEFAULT_WORKSPACE = 'default'
    public static final String JSON_FORMAT = '-json'
    public static final String VARIABLE_CACHE_FILE_NAME = '__.gradle.cached.tfvars'
    public static final String WRAPPER_GROUP = 'Build Setup'

    /**
     * Loads a default environment for running {@code terraform/tofu}.
     *
     * @return A map of environment variables.
     */
    @SuppressWarnings('UnnecessaryCast')
    static Map<String, Object> defaultEnvironment() {
        // tag::default-environment[]
        if (OS.windows) {
            [
                TEMP        : System.getenv('TEMP'),
                TMP         : System.getenv('TMP'),
                HOMEDRIVE   : System.getenv('HOMEDRIVE'),
                HOMEPATH    : System.getenv('HOMEPATH'),
                USERPROFILE : System.getenv('USERPROFILE'),
                (OS.pathVar): System.getenv(OS.pathVar)
            ] as Map<String, Object>
        } else {
            [
                HOME        : System.getProperty('user.home'),
                (OS.pathVar): System.getenv(OS.pathVar)
            ] as Map<String, Object>
        }
        // end::default-environment[]
    }

    /**
     * Removes dangling symbolic links from a directory.
     *
     * @param targetDir Directory to work with.
     */
    static void removeDanglingSymlinks(File targetDir) {
        Path pluginDir = targetDir.toPath()
        Files.walkFileTree(
            pluginDir,
            new FileVisitor<Path>() {
                @Override
                FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
                    CONTINUE
                }

                @Override
                FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                    if (attrs.symbolicLink && !Files.exists(readSymbolicLink(file))) {
                        log.debug("Removing dangling plugin symbolic link ${file}")
                        Files.delete(file)
                    }
                    CONTINUE
                }

                @Override
                FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
                    log.debug("Failed to visit: ${file}, because ${exc.message}")
                    CONTINUE
                }

                @Override
                FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                    CONTINUE
                }
            }
        )
    }

    /**
     * Provides a task name
     *
     * @param sourceSetName Name of source set the task will be associated with.
     * @param commandType The OpenTofu command that this task will wrap.
     * @param workspaceName Name of workspace. if {@code null} will act as if workspace-agnostic.
     * @param workspaceAgnostic Whether the task is workspace agnostic.
     * @param taskPrefix The prefix for all tasks in the group.
     *
     * @return Name of task
     */
    static String taskName(
        String sourceSetName,
        String commandType,
        String workspaceName,
        boolean workspaceAgnostic,
        String taskPrefix
    ) {
        boolean agnostic = workspaceName ? workspaceAgnostic : true
        String workspace = workspaceName == DEFAULT_WORKSPACE || agnostic ? '' : workspaceName.capitalize()
        sourceSetName == DEFAULT_SOURCESET_NAME ?
            "${taskPrefix}${commandType.capitalize()}${workspace}" :
            "${taskPrefix}${sourceSetName.capitalize()}${commandType.capitalize()}${workspace}"
    }

    /**
     * The name of the backend configuration task.
     *
     * @param sourceSetName Name of source set.
     * @param taskPrefix The prefix for all tasks in the group.
     * @return Name of task
     */
    static String backendTaskName(
        String sourceSetName,
        String taskPrefix
    ) {
        "create${taskName(sourceSetName, 'backendConfiguration', null, true, taskPrefix).capitalize()}"
    }

    /**
     * The name of the task that prepares a plugin cache directory.
     *
     * @param taskPrefix The prefix for all tasks in the group.
     * @return Name of task
     */
    static String pluginCacheDirTaskName(String taskPrefix) {
        "prepare${taskPrefix.capitalize()}PluginCacheDirectory"
    }

    /**
     * The name of the variables caching task.
     *
     * @param sourceSetName Name of source set.
     * @param taskPrefix The prefix for all tasks in the group.
     * @return Name of task
     */
    static String variablesCacheTaskName(
        String sourceSetName,
        String taskPrefix
    ) {
        "cache${taskName(sourceSetName, 'Variables', null, true, taskPrefix).capitalize()}"
    }

    /**
     * Configure options on a task that extends {@link AbstractTfStandardTask}.
     *
     * @param task Task instance
     * @param sourceSet Source set
     * @param protector The concurrency protector service.
     */
    static void configureStandardTaskOptions(
        AbstractTfStandardTask task,
        SourceSetSources sourceSet,
        Provider<ConcurrencyProtector> protector
    ) {
        task.tap {
            sourceDir = sourceSet.srcDir
            dataDir = sourceSet.dataDir
            reportsDir = sourceSet.reportsDir
            logDir = sourceSet.logDir
            importantInputFiles(sourceSet.asFileTree)
            importantInputFiles(sourceSet.secondarySources)

            concurrencyProtector.set(protector)
            usesService(protector)
        }
    }

    /**
     * Configures options on a task that extends {@link AbstractTfBaseTask}
     * @param task Task instance.
     * @param sourceSet source set
     * @param exeOpts List of classes that will provide additional options.
     */
    static void configureBaseTaskOptions(
        AbstractTfBaseTask task,
        SourceSetExecutionEnvironment sourceSet,
        Iterable<Class<? extends ExecutionOptions>> exeOpts
    ) {
        task.tap {
            executableLocation = sourceSet.executableLocation
            executableVersion = sourceSet.executableVersion
            addEnvironmentProvider(sourceSet.environment)
            secretVariables = sourceSet.secretVariables
            addExecutionOptions(sourceSet.executionOptionsFor(exeOpts).map { it.commandLineArgs })
        }
    }

    /**
     * Registers the task for configuring the backend.
     *
     * @param sourceSetName Name of source set.
     * @param taskName Name of task.
     * @param group Task group.
     * @param backendConfigurationDir Backend configuration directory.
     * @param backendTokenProvider Provider of backend tokens.
     * @param project Associated project.
     */
    @SuppressWarnings('ParameterCount')
    static void registerBackendConfigurationTask(
        String sourceSetName,
        String taskName,
        String group,
        Provider<File> backendConfigurationDir,
        Provider<Map<String, TokenValue>> backendTokenProvider,
        Project project
    ) {
        project.tasks.register(
            taskName,
            RemoteStateConfigGenerator,
        ) { t ->
            t.group = group
            t.description = "Generates the backend configuration for '${sourceSetName}'"
            t.destinationDir = backendConfigurationDir
            t.tokenProvider = backendTokenProvider
        }
    }

    /**
     * Registers a task to create a plugin cache directory
     *
     * @param taskName Task name
     * @param group Task group
     * @param cacheDir Provider to the cache directory
     * @param project Assocaited project
     */
    static void registerPluginCacheDirTask(
        String taskName,
        String group,
        Provider<File> cacheDir,
        Project project
    ) {
        project.tasks.register(taskName, PluginCacheDirGenerator) {
            it.group = group
            it.description = 'Prepares a plugin cache directory'
            it.pluginDir = cacheDir
        }
    }
    /**
     * Registers the task for caching variables.
     *
     * @param taskName Name of task.
     * @param toolName Name of tool.
     * @param group Task group.
     * @param varFile Location of the file for the variables.
     * @param vars Variables to be cached.
     * @param project Associatred project.
     */
    @SuppressWarnings('ParameterCount')
    static void registerVariablesGeneratorTask(
        String taskName,
        String toolName,
        String group,
        Provider<File> varFile,
        Provider<List<String>> vars,
        Project project
    ) {
        project.tasks.register(
            taskName,
            VariablesCacheGenerator,
        ) { t ->
            t.group = group
            t.description = "Caches the variables that are required for certain ${toolName} tasks"
            t.variablesFile = varFile
            t.variables = vars
        }
    }

    @SuppressWarnings('ParameterCount')
    static void registerGlobalConfigOutgoingConfiguration(
        String taskName,
        String taskGroup,
        String toolName,
        String outputRelPath,
        TaskProvider<GlobalConfigGenerator> rcgen,
        Provider<File> pluginCacheDir,
        Provider<Integer> pluginCacheTimeout,
        String outgoingConfigName,
        String outgoingAttrName,
        String outgoingAttrValue,
        Project project
    ) {
        final gcg = project.tasks.register(
            taskName,
            GlobalConfigDetailsGenerator
        ) { t ->
            t.group = taskGroup
            t.description = "Generated metadata about ${toolName} that can be used by subprojects"
            t.inputs.files(rcgen)
            t.outputFile = t.fsOperations().buildDirDescendant(outputRelPath)
            t.location = rcgen.flatMap { it.outputFile }
            t.pluginCacheDir = pluginCacheDir
            t.pluginCacheTimeout = pluginCacheTimeout
        }

        ProjectOperations.find(project).configurations.createSingleOutgoingConfiguration(
            outgoingConfigName,
            false
        ) { attrs ->
            attrs.attribute(
                Attribute.of(outgoingAttrName, String),
                outgoingAttrValue,
            )
        }

        project.configurations.getByName(outgoingConfigName).outgoing { cp ->
            cp.artifact(gcg)
        }
    }

    /**
     * Configures the task that caches a binary on behalf of a wrapper.
     *
     * @param taskProvider Provider of a task that extends {@link AbstractIacCacheBinaryTask}.
     * @param rcExt Extension that extends {@link AbstractGlobalConfigExtension}.
     * @param wrapperExt The wrapper extension that extends {@link AbstractIacToolchain}.
     * @param toolName Name of the tool that will be wrapped.
     */
    static void configureWrapperCacheBinaryTask(
        TaskProvider<? extends AbstractIacCacheBinaryTask> taskProvider,
        AbstractGlobalConfigExtension rcExt,
        AbstractIacToolchain wrapperExt,
        String toolName
    ) {
        taskProvider.configure { t ->
            t.tap {
                group = WRAPPER_GROUP
                description = "Caches ${toolName} binary"
                binaryVersionProvider = wrapperExt.resolvedExecutableVersion()
                binaryLocationProvider = wrapperExt.executable.map { it.absolutePath }
                configLocation = rcExt.configFile.map { it.absolutePath }
            }
        }
    }

    /**
     * Configures the task that creates wrapper scripts.
     *
     * @param taskProvider Provider of the task that creates the wrapper scripts.
     * @param cacheTaskProvider Provider of the task that caches the tool binary.
     * @param toolName Name of the tool.
     */
    static void configureWrapperTask(
        TaskProvider<? extends AbstractIacWrapperTask> taskProvider,
        TaskProvider<? extends AbstractIacCacheBinaryTask> cacheTaskProvider,
        String toolName
    ) {
        taskProvider.configure { t ->
            t.tap {
                group = WRAPPER_GROUP
                description = "Generates ${toolName.capitalize()} wrapper files"
                associateCacheTask(cacheTaskProvider)
            }
        }
    }

    /**
     * Register the concurrent protector for a source set.
     *
     * @param serviceName Soutce set service name.
     * @param project Project where we start from.
     * @return A provider to the service.
     */
    static Provider<ConcurrencyProtector> registerConcurrencyProtectorService(
        String serviceName,
        Project project
    ) {
        BuildServiceSpec
        project.gradle.sharedServices.registerIfAbsent(serviceName, ConcurrencyProtector) {
            it.maxParallelUsages.set(1)
        }
    }

    private static final OperatingSystem OS = OperatingSystem.current()
}
