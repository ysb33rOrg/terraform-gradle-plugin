/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.iac.base.tf.config.multilevel

import groovy.transform.CompileStatic
import org.gradle.api.provider.Provider

/**
 * Allows for options related to walking the state tree.
 *
 * @author Schalk W. Cronjé
 *
 * @since 0.1
 */
@CompileStatic
class StateOptionsConcurrency implements ExecutionConfiguration {
    final Provider<List<String>> commandLineArgs
    final Provider<List<String>> tfVars
    final Provider<String> inputProperty

    StateOptionsConcurrency(StateOptionsFull sof) {
        this.commandLineArgs = sof.commandLineArgs.map { list ->
            list.findAll { it.startsWith('-parallel') }
        }
        this.tfVars = sof.tfVars
        this.inputProperty = sof.inputProperty
    }
}
