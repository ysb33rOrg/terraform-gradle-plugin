/**
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.iac.base.tf;

import org.gradle.api.file.FileTree;
import org.gradle.api.provider.Provider;

import java.io.File;
import java.util.List;

/**
 * Source set directories and file collections.
 *
 * @author Schalk W. Cronjé
 * @since 2.0
 */
public interface SourceSetSources {
    /**
     * Source tree
     */
    FileTree getAsFileTree();

    /**
     * Provide for the root directory of the source set,
     *
     * @return File provider.
     */
    Provider<File> getSrcDir();

    /** Data directory provider.
     *
     * @return File provider.
     */
    Provider<File> getDataDir();


    /** Log directory provider.
     *
     * @return File provider.
     */
    Provider<File> getLogDir();

    /**
     * Reports directory.
     *
     * @return File provider.
     */
    Provider<File> getReportsDir();

    /**
     * Provides a list of secondary sources.
     *
     * @return Provider never returns null, but could return an empty list.
     */
    Provider<List<File>> getSecondarySources();
}
