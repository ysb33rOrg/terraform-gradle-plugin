/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.iac.base.internal.tf.tokentypes

import groovy.transform.CompileStatic
import org.gradle.api.model.ObjectFactory
import org.gradle.api.provider.Provider
import org.ysb33r.gradle.iac.base.tf.backends.TokenValue
import org.ysb33r.grolifant5.api.core.ProviderTools

import java.nio.file.Path
import java.util.concurrent.Callable
import java.util.function.Supplier

/**
 * Helper class for backend tokens.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class TokenHelpers {
    static TokenValue create(Object value, ObjectFactory objects) {
        switch (value) {
            case TokenValue:
                return (TokenValue) value
            case Boolean:
                return new BooleanToken((Boolean) value)
            case Number:
                return new NumberToken((Number) value)
            case Map:
                return objects.newInstance(MapToken, (Map) value)
            case Collection:
                return objects.newInstance(ListToken, (Collection) value)
            case Provider:
                return new LazyEvaluatedToken((Provider) value, objects)
            case Callable:
                return new LazyEvaluatedToken((Callable) value, objects)
            case Supplier:
                return new LazyEvaluatedToken((Supplier) value, objects)
            case Optional:
                return new LazyEvaluatedToken((Optional) value, objects)
            case File:
            case Path:
                return objects.newInstance(FileToken, value)
                // Treat everything else as a string
            default:
                objects.newInstance(StringToken, value)
        }
    }

    static TokenValue create(Object value, ProviderTools pt) {
        switch (value) {
            case TokenValue:
                return (TokenValue) value
            case Boolean:
                return new BooleanToken((Boolean) value)
            case Number:
                return new NumberToken((Number) value)
            case Map:
                return pt.newInstance(MapToken, (Map) value)
            case Collection:
                return pt.newInstance(ListToken, (Collection) value)
            case Provider:
                return new LazyEvaluatedToken((Provider) value, pt)
            case Callable:
                return new LazyEvaluatedToken((Callable) value, pt)
            case Supplier:
                return new LazyEvaluatedToken((Supplier) value, pt)
            case Optional:
                return new LazyEvaluatedToken((Optional) value, pt)
            case File:
            case Path:
                return pt.newInstance(FileToken, value)
                // Treat everything else as a string
            default:
                pt.newInstance(StringToken, value)
        }
    }
}
