/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.iac.base.tf.tasks

import groovy.transform.CompileStatic
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.gradle.api.tasks.options.Option
import org.ysb33r.gradle.iac.base.errors.IacConfigurationException
import org.ysb33r.gradle.iac.base.tf.execspec.IacTfExecSpec

/**
 * The {@code state rm} command.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class AbstractStateRmTask extends AbstractStateTask {
    @Option(option = 'path', description = 'Resource path to remove')
    void setResourcePath(String id) {
        this.path.set(id)
    }

    @Override
    protected void exec() {
        if (!path.present) {
            throw new IacConfigurationException('No resource path specified. Use --path.')
        }
        execSpec.cmd {
            addCommandLineArgumentProviders(finalArgs)
        }
        super.exec()
    }

    protected AbstractStateRmTask(
        String workspaceName,
        Class<? extends IacTfExecSpec> execSpecClass
    ) {
        super('rm', workspaceName, execSpecClass)
        this.path = project.objects.property(String)
        this.finalArgs = this.path.map { [it] }
    }

    private final Property<String> path
    private final Provider<List<String>> finalArgs
}
