/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.iac.base.tf.config.multilevel

import groovy.transform.CompileStatic

/**
 * Resovled execution options for a specific combination.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class ResolvedExecutionsOptions implements Serializable {

    /**
     * Returns the list of Terraform/OpenTofu command-line arguments.
     */
    final List<String> commandLineArgs

    /**
     * Returns the list of variables in the form name=value
     */
    final List<String> tfVars

    /**
     * @return Provider to a special property.
     */
    final String inputProperty

    ResolvedExecutionsOptions(
        final List<String> commandLineArgs,
        final List<String> tfVars,
        final String inputProperty
    ) {
        this.commandLineArgs = commandLineArgs
        this.tfVars = tfVars
        this.inputProperty = inputProperty
    }
}
