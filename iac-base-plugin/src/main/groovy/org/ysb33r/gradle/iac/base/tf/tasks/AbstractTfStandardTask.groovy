/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.iac.base.tf.tasks

import groovy.transform.CompileStatic
import org.gradle.api.file.ConfigurableFileCollection
import org.gradle.api.file.FileCollection
import org.gradle.api.provider.ListProperty
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.gradle.api.tasks.Internal
import org.gradle.api.tasks.options.Option
import org.ysb33r.gradle.iac.base.internal.tf.TaskUtils
import org.ysb33r.gradle.iac.base.tf.execspec.DefaultArguments
import org.ysb33r.gradle.iac.base.tf.execspec.IacTfExecSpec
import org.ysb33r.gradle.iac.base.tf.services.ConcurrencyProtector

import static org.ysb33r.grolifant5.api.core.ExecTools.OutputType.CAPTURE
import static org.ysb33r.grolifant5.api.core.ExecTools.OutputType.FORWARD

/**
 * A base class for all Terraform and OpenTofu tasks.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
abstract class AbstractTfStandardTask extends AbstractTfBaseTask {

    public static final String PLAN_FILE_EXTENSION = 'tf.plan'

    /**
     * For internal use by tasks only.
     */
    @Internal
    final Property<ConcurrencyProtector> concurrencyProtector

    /**
     * Patch in an command-line arguments.
     *
     * <p>
     *     This is meant as a last-resort when a command-line argument is needed for the tool, but the argument is not
     *   yet supported by the plugin.
     *   </p>
     *
     * @param singleArgs Arguments to patch
     *
     * @since 2.0
     */
    @Option(option = 'patch-arg', description = 'Patch a single command-line argument')
    void setSingleArgs(List<String> singleArgs) {
        patchedArgs.addAll(singleArgs)
    }

    /**
     * Patch in an command-line arguments.
     *
     * <p>
     *     This is meant as a last-resort when a command-line argument is needed for the tool, but the argument is not
     *   yet supported by the plugin.
     *   </p>
     *
     * @param cmdlineSnippets Cmdline snippets to patch. These will be split on space boundaries.
     *   If you have command-line parameters that contain spaces, use {@link #setSingleArgs} instead.
     *
     * @since 2.0
     */
    @Option(option = 'patch-args', description = 'Patches a collection of space command-line arguments')
    void setSpaceSeparateArgs(List<String> cmdlineSnippets) {
        final list = cmdlineSnippets.collectMany {
            it.split(/\s+/).toList()
        }
        patchedArgs.addAll(list)
    }

    @Internal
    Provider<File> getSourceDir() {
        this.sourceDir
    }

    @Internal
    Provider<File> getDataDir() {
        this.dataDir
    }

    @Internal
    Provider<File> getLogDir() {
        this.logDir
    }

    @Internal
    Provider<File> getReportsDir() {
        this.reportsDir
    }

    void setSourceDir(Provider<File> dir) {
        this.sourceDir.set(dir)
    }

    void setDataDir(Provider<File> dir) {
        this.dataDir.set(dir)
    }

    void setReportsDir(Provider<File> dir) {
        this.reportsDir.set(dir)
    }

    void setLogDir(Provider<File> dir) {
        this.logDir.set(dir)
    }

    void importantInputFiles(FileCollection tree) {
        this.importantInputFiles.from(tree)
    }

    void importantInputFiles(Provider<List<File>> tree) {
        this.importantInputFiles.from(tree)
    }

    /**
     * Adds an environment provider.
     *
     * @param provider Provider that will add additional environments.
     */
    @Override
    void addEnvironmentProvider(Provider<? extends Map<String, String>> provider) {
        super.addEnvironmentProvider(provider)
        if (workspaceExecSpec) {
            workspaceExecSpec.entrypoint.addEnvironmentProvider(provider)
        }
    }

    protected final String workspaceName

    protected AbstractTfStandardTask(
        String command,
        String workspaceName,
        Class<? extends IacTfExecSpec> execSpecClass
    ) {
        this(command, workspaceName, execSpecClass, DefaultArguments.builder())
    }

    protected AbstractTfStandardTask(
        String command1,
        String workspaceName,
        Class<? extends IacTfExecSpec> execSpecClass,
        DefaultArguments.Builder argsBuilder
    ) {
        super(command1, execSpecClass, argsBuilder)
        this.workspaceName = workspaceName
        this.sourceDir = project.objects.property(File)
        this.dataDir = project.objects.property(File)
        this.logDir = project.objects.property(File)
        this.reportsDir = project.objects.property(File)
        this.importantInputFiles = fsOperations().emptyFileCollection()
        this.workspaceExecSpec = createWorkspace(execSpecClass)
        this.patchedArgs = project.objects.listProperty(String)
        this.taintedResourcesDir = this.dataDir.map { new File(it, '.tainted') }
        this.concurrencyProtector = project.objects.property(ConcurrencyProtector)
        configureExtras()
    }

    /**
     * Creates logging directory, set source directory as working directory, switch to appropriate workspace,
     * then run and handle result.
     */
    @Override
    protected void exec() {
        createLogDir()
        setSourceDirAsWorkingDir()
        switchWorkspace()
        super.exec()
    }

    /**
     * Creates the logging directory
     */
    protected void createLogDir() {
        logDir.get().mkdirs()
    }

    /**
     * Sets the source directory as the working directory.
     */
    protected void setSourceDirAsWorkingDir() {
        execSpec.entrypoint {
            workingDir(sourceDir.get())
        }
    }

    /**
     * Switches workspaces to the correct one if the source set has workspaces and the current workspace is not the
     * correct one. If no additional workspace or the task is workspace agnostic, then it will do-nothing.
     */
    protected void switchWorkspace() {
        if (workspaceExecSpec) {
            final workspaces = listWorkspaces()
            String current = workspaces.find { k, v -> v == true }.key

            if (current != workspaceName) {
                if (workspaces.containsKey(workspaceName)) {
                    runWorkspaceSubcommand('select', workspaceName)
                } else {
                    runWorkspaceSubcommand('new', workspaceName)
                }
            }
        }
    }

    /**
     * Lists the workspaces as currently known to Terraform
     *
     * @return List of workspaces.
     */
    protected Map<String, Boolean> listWorkspaces() {
        runWorkspaceSubcommand('list').readLines().findAll {
            !it.empty
        }.collectEntries {
            if (it.startsWith('*')) {
                [it.substring(1).trim(), true]
            } else {
                [it.trim(), false]
            }
        }
    }

    /**
     * Runs a {@code terraform workspace} subcommand.
     *
     * @param wsCmd Subcommand to run.
     * @return Output from command.
     *
     */
    protected String runWorkspaceSubcommand(String wsCmd, String... cmdArgs) {
        workspaceExecSpec.entrypoint {
            workingDir(sourceDir)
        }
        workspaceExecSpec.cmd {
            args = [wsCmd]
            args(cmdArgs)
        }
        logDir.get().mkdirs()
        final result = execTools().exec(CAPTURE, FORWARD) {
            workspaceExecSpec.copyTo(it)
            it.environment.remove('TF_LOG')
            it.environment(secretVariables.get())
        }
        result.assertNormalExitValue()
        result.standardOutput.asText.get()
    }

    /**
     * Reports whether there are any tainted resources.
     *
     * @return {@code true} If resources were manually tainted after the last apply.
     */
    protected boolean hasTaintedResources() {
        final dir = this.taintedResourcesDir.get()
        dir.exists() && dir.listFiles({ File it ->
            !it.directory
        } as FileFilter).size()
    }

    /**
     * Mark a resource as tainted.
     *
     * @param resource Resource name
     */
    protected addTaintedResource(String resource) {
        final dir = this.taintedResourcesDir.get()
        if (!dir.exists()) {
            dir.mkdirs()
        }
        new File(dir, fsOperations().toSafeFileName(resource)).text = resource
    }

    /**
     * Remove a resource as being tainted.
     *
     * @param resource Resource name
     */
    protected removeTaintedResource(String resource) {
        final dir = this.taintedResourcesDir.get()
        if (dir.exists()) {
            final file = new File(dir, fsOperations().toSafeFileName(resource))
            if (file.exists()) {
                file.delete()
            }
        }
    }

    /**
     * Remove all tainted resources.
     */
    protected removeAllTaintedResources() {
        final dir = this.taintedResourcesDir.get()
        if (dir.exists()) {
            dir.deleteDir()
        }
    }

    /**
     * Calculates the location of a plan file
     *
     * @param sourceSetName Provider to the name of source set.
     * @param workspaceName Name of workspace.
     * @param defaultWorkspaceName Name of default workspace.
     * @param fileExtension File extension.
     *
     * @return Path where plan file should be located.
     */
    protected Provider<File> whatIsPlanOutputFile(
        Provider<String> sourceSetName,
        String workspaceName,
        String defaultWorkspaceName,
        String fileExtension
    ) {
        final ws = workspaceName == defaultWorkspaceName ? '' : ".${workspaceName}"
        dataDir.zip(sourceSetName) { file, ssName ->
            new File(file, "${ssName}${ws}.${fileExtension}")
        }
    }

    private void configureExtras() {
        inputs.files(this.importantInputFiles).skipWhenEmpty().ignoreEmptyDirectories()
        super.addEnvironmentProvider(this.logDir.map { [TF_LOG_PATH: new File(it, "${name}.log").absolutePath] })

        execSpec.cmd {
            addCommandLineArgumentProviders(this.patchedArgs)
        }
    }

    private IacTfExecSpec createWorkspace(Class<? extends IacTfExecSpec> execSpecClass) {
        if (workspaceName) {
            final ws = project.objects.newInstance(execSpecClass).tap {
                entrypoint {
                    environment = TaskUtils.defaultEnvironment()
                    executable(executableLocation)
                }
                cmd {
                    command = 'workspace'
                }
            }
            ws
        } else {
            null
        }
    }

    private final Property<File> sourceDir
    private final Property<File> dataDir
    private final Property<File> logDir
    private final Property<File> reportsDir
    private final Provider<File> taintedResourcesDir
    private final ConfigurableFileCollection importantInputFiles
    private final IacTfExecSpec workspaceExecSpec
    private final ListProperty<String> patchedArgs
}
