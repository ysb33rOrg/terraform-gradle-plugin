/**
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.iac.base.tf;

import org.gradle.api.provider.Provider;
import org.ysb33r.grolifant5.api.core.SimpleSecureString;

import java.util.Map;

/**
 * Methods for adding and accessing secrets.
 *
 * @author Schalk W. Cronjé
 * @since 2.0
 */
public interface SecretVariables extends SecretVariablesProvider {
    /**
     * Sets a secret variable that will be injected into the environment at run time.
     *
     * @param key Key
     * @param value Value to be encrypted.
     *   This is anything that can be converted to a string.
     *   If this is a provider, it will be evaluated at this point.
     */
    void secretVariable(String key, Object value);

    /**
     * Sets a secret variable that will be injected into the environment at run time.
     *
     * @param key Key
     * @param propertyName Name of property
     *   If the property cannot be resolved, it will be set to a blank string.
     *
     */
    void secretPropertyOrBlank(String key, String propertyName);

    /**
     * Sets a secret variable that will be injected into the environment at run time.
     *
     * @param key Key
     * @param propertyName Name of property
     *   If the property cannot be resolved, no action will be taken and a warning will be logged.
     */
    void secretProperty(String key, String propertyName);

    /**
     * Use secrets from another secrets provider.
     *
     * @param secretsProvider Provider of secrets.
     */
    void fromSecretsProvider(Provider<Map<String, SimpleSecureString>> secretsProvider);
}
