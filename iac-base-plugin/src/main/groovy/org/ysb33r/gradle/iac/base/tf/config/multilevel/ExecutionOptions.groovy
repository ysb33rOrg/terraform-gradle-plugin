/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.iac.base.tf.config.multilevel

import groovy.transform.CompileStatic
import org.gradle.api.Action
import org.gradle.api.Project
import org.gradle.api.model.ObjectFactory
import org.gradle.api.provider.Provider
import org.ysb33r.gradle.iac.base.errors.IacConfigurationException
import org.ysb33r.grolifant5.api.core.ClosureUtils
import org.ysb33r.grolifant5.api.core.StringTools

import javax.inject.Inject

/**
 * Configurable execution options for OpenTofu/Terraform.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class ExecutionOptions {
    final Lock lock
    final StateOptionsFull stateOptions
    final CodeFormatting formatting

    @Inject
    ExecutionOptions(Project tempProjectRef) {
        this.objectFactory = tempProjectRef.objects
        this.lock = objectFactory.newInstance(Lock)
        this.stateOptions = objectFactory.newInstance(StateOptionsFull)
        this.stateOptionsConcurrency = new StateOptionsConcurrency(this.stateOptions)
        this.formatting = objectFactory.newInstance(CodeFormatting)
    }

    /**
     * Gets an execution option by type.
     *
     * @param type One of the types that implements {@link ExecutionConfiguration}.
     *
     * @return Instance of the type.
     * @throw IacConfigurationException When the type is not supported.
     */
    ExecutionConfiguration getByType(Class<? extends ExecutionConfiguration> type) {
        switch (type) {
            case Lock:
                return this.lock
            case StateOptionsFull:
                return this.stateOptions
            case StateOptionsConcurrency:
                return this.stateOptionsConcurrency
            case CodeFormatting:
                return this.formatting
            default:
                throw new IacConfigurationException("${type.canonicalName} is not a supported execution option")
        }
    }

    /**
     * Returns the list of Terraform/OpenTofu command-line arguments.
     *
     * @param types The types that needs to be retrieved
     * @return List of arguments to be added. Can be empty, but never {@code null}
     */
    Provider<List<String>> getCommandLineArgs(Iterable<Class<? extends ExecutionConfiguration>> types) {
        final prov = objectFactory.listProperty(String)
        types.each {
            prov.addAll(getByType(it).commandLineArgs)
        }
        prov
    }

    /**
     * Returns a provider that will contain all the resolved options for given combination.
     *
     * @param types Types which to combine.
     * @return Provider
     */
    Provider<ResolvedExecutionsOptions> getResolvedOptionsFor(Iterable<Class<? extends ExecutionConfiguration>> types) {
        final args = objectFactory.listProperty(String)
        final vars = objectFactory.listProperty(String)
        final props = objectFactory.listProperty(String)
        types.each {
            final opt = getByType(it)
            args.addAll(opt.commandLineArgs)
            vars.addAll(opt.tfVars)
            props.add(opt.inputProperty)
        }

        args.zip(vars) { x, y ->
            new ResolvedExecutionsOptions(x, y, props.get().join(StringTools.COMMA_SPACE))
        }
    }

    void lock(Action<Lock> configurator) {
        configurator.execute(this.lock)
    }

    void lock(@DelegatesTo(Lock) Closure<?> configurator) {
        ClosureUtils.configureItem(this.lock, configurator)
    }

    void stateOptions(Action<StateOptionsFull> configurator) {
        configurator.execute(this.stateOptions)
    }

    void stateOptions(@DelegatesTo(StateOptionsFull) Closure<?> configurator) {
        ClosureUtils.configureItem(this.stateOptions, configurator)
    }

    private final ObjectFactory objectFactory
    private final StateOptionsConcurrency stateOptionsConcurrency
}
