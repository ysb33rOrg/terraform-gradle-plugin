/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.iac.base.tf

import groovy.transform.CompileStatic
import org.gradle.api.Project
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.gradle.api.provider.SetProperty
import org.ysb33r.grolifant5.api.core.ConfigCacheSafeOperations
import org.ysb33r.grolifant5.api.core.FileSystemOperations

import javax.inject.Inject

/**
 * Configures sources that are not part of a source set, but which can be included or referenced.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class OtherSources {

    @Inject
    OtherSources(Project tempProjectRef) {
        this.sourceDirs = tempProjectRef.objects.setProperty(File)
        this.recurse = tempProjectRef.objects.property(Boolean)
        this.recurse.convention(false)
        this.fsOperations = ConfigCacheSafeOperations.from(tempProjectRef).fsOperations()
    }

    /**
     * Whether to recursive scan directories below the source directories.
     *
     * @param flag {@code true} to recursive descendant directories.
     */
    void setRecursive(boolean flag) {
        this.recurse.set(flag)
    }

    /**
     * Whether to recurse down the child directories.
     *
     * @return
     */
    Provider<Boolean> getRecursive() {
        this.recurse
    }

    /**
     * The list of toplevel directories to check
     *
     * @return List of directories
     */
    Provider<Set<File>> getSourceDirectories() {
        this.sourceDirs
    }

    /**
     * Replace existing directories with a new set.
     *
     * @param dirs Directories to add. Anything convertible by the like of `project.file` is acceptable.
     */
    void setDirs(Iterable<?> dirs) {
        this.sourceDirs.set([])
        this.sourceDirs.addAll(fsOperations.provideRootDir().map {
            dirs.collect { f -> fsOperations.file(f) }
        })
    }

    /**
     * Add directories to be set.
     *
     * @param d Directories to add. Anything convertible by the like of `project.file` is acceptable.
     */
    void dirs(Object... dirs) {
        this.sourceDirs.addAll(fsOperations.provideRootDir().map {
            dirs.collect { f -> fsOperations.file(f) }
        })
    }

    private final Property<Boolean> recurse
    private final SetProperty<File> sourceDirs
    private final FileSystemOperations fsOperations
}
