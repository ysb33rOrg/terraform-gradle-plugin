/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.iac.base.internal.tf

import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import org.gradle.api.Project
import org.gradle.api.provider.MapProperty
import org.gradle.api.provider.Provider
import org.ysb33r.gradle.iac.base.tf.SecretVariables
import org.ysb33r.grolifant5.api.core.ConfigCacheSafeOperations
import org.ysb33r.grolifant5.api.core.SimpleSecureString
import org.ysb33r.grolifant5.api.core.plugins.GrolifantServicePlugin
import org.ysb33r.grolifant5.api.core.services.GrolifantSecurePropertyService

import javax.inject.Inject

/**
 * Implementation to store secrets.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
@Slf4j
class DefaultSecretVariables implements SecretVariables {

    @Inject
    DefaultSecretVariables(Project tempProjectRef) {
        this.ccso = ConfigCacheSafeOperations.from(tempProjectRef)
        this.securityVars = tempProjectRef.objects.mapProperty(String, SimpleSecureString)
        this.securePropertyServiceProvider = (Provider<GrolifantSecurePropertyService>) tempProjectRef.gradle
            .sharedServices
            .registrations
            .getByName(GrolifantServicePlugin.SECURE_STRING_SERVICE)
            .service
    }

    /**
     * Sets a secret variable that will be injected into the environment at run time.
     *
     * @param key Key
     * @param value Value to be encrypted.
     *   This is anything that can be converted to a string.
     *   If this is a provider, it will be evaluated at this point.
     */
    @Override
    void secretVariable(String key, Object value) {
        this.securityVars.put(
            key,
            securePropertyServiceProvider.get().pack(
                ccso.stringTools().stringize(value))
        )
    }

    /**
     * Sets a secret variable that will be injected into the environment at run time.
     *
     * @param key Key
     * @param propertyName Name of property
     *   If the property cannot be resolved, it will be set to a blank string.
     *
     */
    @Override
    void secretPropertyOrBlank(String key, String propertyName) {
        this.securityVars.put(
            key,
            securePropertyServiceProvider.get().pack(
                ccso.providerTools().resolveProperty(propertyName, ''
                ).get())
        )
    }

    /**
     * Sets a secret variable that will be injected into the environment at run time.
     *
     * @param key Key
     * @param propertyName Name of property
     *   If the property cannot be resolved, no action will be taken and a warning will be logged.
     */
    void secretProperty(String key, String propertyName) {
        final property = ccso.providerTools().resolveProperty(propertyName)
        if (property.present) {
            this.securityVars.put(
                key,
                securePropertyServiceProvider.get().pack(property.get())
            )
        } else {
            log.warn("Secret property '${propertyName}' could not be resolved and will be ignored")
        }
    }

    /**
     * Returns a provider to a set of secret variables.
     *
     * @return Provider.
     */
    @Override
    Provider<Map<String, SimpleSecureString>> getSecretVariables() {
        this.securityVars
    }

    /**
     * Use secrets from another secrets provider.
     *
     * @param secretsProvider Provider of secrets.
     */
    @Override
    void fromSecretsProvider(Provider<Map<String, SimpleSecureString>> secretsProvider) {
        this.securityVars.putAll(secretsProvider)
    }

    private final ConfigCacheSafeOperations ccso
    private final MapProperty<String, SimpleSecureString> securityVars
    private final Provider<GrolifantSecurePropertyService> securePropertyServiceProvider
}
