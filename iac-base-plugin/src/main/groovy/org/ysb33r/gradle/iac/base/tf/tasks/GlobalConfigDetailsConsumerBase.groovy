/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.iac.base.tf.tasks

import groovy.transform.CompileStatic
import org.gradle.api.file.FileCollection
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.gradle.api.tasks.InputFiles
import org.gradle.api.tasks.OutputFile
import org.gradle.api.tasks.PathSensitive
import org.gradle.api.tasks.PathSensitivity
import org.gradle.api.tasks.TaskAction
import org.gradle.api.tasks.util.PatternFilterable
import org.ysb33r.grolifant5.api.core.runnable.GrolifantDefaultTask

/**
 * Generates repository details to a file.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class GlobalConfigDetailsConsumerBase extends GrolifantDefaultTask {

    @InputFiles
    @PathSensitive(PathSensitivity.RELATIVE)
    FileCollection getInputFiles() {
        this.details
    }

    @OutputFile
    Provider<File> getDetailsLocation() {
        this.outputFile
    }

    @TaskAction
    void exec() {
        fsOperations().copy {
            it.from(details)
            it.into(outputFile.get().parentFile)
            it.include("**/${detailsFileName}")
        }
    }

    protected GlobalConfigDetailsConsumerBase(
        String outputFileRelSubPath,
        String detailsFileName,
        String incomingConfigurationName
    ) {
        this.outputFile = project.objects.property(File)
        this.outputFile.set(fsOperations().buildDirDescendant("${outputFileRelSubPath}/${detailsFileName}"))
        this.detailsFileName = detailsFileName
        this.details = project.configurations.getByName(incomingConfigurationName)
            .asFileTree.matching { PatternFilterable p ->
            p.include(detailsFileName)
        }
    }

    private final String detailsFileName
    private final Property<File> outputFile
    private final FileCollection details
}
