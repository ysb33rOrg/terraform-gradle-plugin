/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.iac.base.internal.tf.tokentypes

import groovy.transform.CompileStatic
import org.gradle.api.Project
import org.ysb33r.gradle.iac.base.errors.IacConfigurationException
import org.ysb33r.gradle.iac.base.hcl.HclTransformer
import org.ysb33r.gradle.iac.base.tf.backends.TokenValue
import org.ysb33r.grolifant5.api.core.ConfigCacheSafeOperations

import javax.inject.Inject
import java.nio.file.Path

/**
 * Backend token that is a {@link File} or {@link Path}.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class FileToken implements TokenValue {

    final String value

    @Inject
    FileToken(Object value, Project tempProjectRef) {
        if (value instanceof File || value instanceof Path) {
            try {
                final file = value instanceof File ? (File) value : ((Path) value).toFile()
                final hcl = new HclTransformer(ConfigCacheSafeOperations.from(tempProjectRef))
                this.value = "\"${hcl.escapeFilePath(file)}\""
            } catch (UnsupportedOperationException e) {
                throw new IacConfigurationException('Item needs to be a Path opn the default filesystem', e)
            }
        } else {
            throw new IacConfigurationException('Item needs to be a File or Path instance')
        }
    }

    @Override
    String toString() {
        value
    }
}
