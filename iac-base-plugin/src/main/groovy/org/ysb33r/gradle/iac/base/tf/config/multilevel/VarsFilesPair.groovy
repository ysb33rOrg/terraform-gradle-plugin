/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.iac.base.tf.config.multilevel

import groovy.transform.CompileStatic
import org.gradle.api.Action
import org.gradle.api.provider.Provider
import org.ysb33r.gradle.iac.base.tf.RemoteStateVarProvider
import org.ysb33r.gradle.iac.base.tf.backends.TokenValue
import org.ysb33r.grolifant5.api.core.ConfigCacheSafeOperations

/**
 * Keeps a collection of variables and variable files.
 *
 * @since 0.2
 */
@CompileStatic
class VarsFilesPair {
    /**
     * Variables.
     *
     * Key is the name of the variable.
     *
     * Value can be a map, collection, boolean, number or anything convertible to a string.
     */
    final Map<String, TokenValue> vars = [:]

    /**
     * List of filenames or relative paths to a source set.
     */
    final List<Object> files

    final Provider<List<File>> resolvedFiles
    final Provider<List<String>> escapedVarsProvider

    /**
     * Additional variables from other sources.
     *
     * @since 0.12
     */
    final List<Action<VariablesSpec>> additionalVariables = []

    VarsFilesPair(ConfigCacheSafeOperations po) {
        this.ccso = ConfigCacheSafeOperations.from(po)
        this.files = []
        this.resolvedFiles = ccso.providerTools().provider { ->
            files.collect {
                ccso.fsOperations().file(it)
            }
        }
        this.escapedVarsProvider = ccso.providerTools().provider { ->
            varsInTfFormat
        }
    }

    void clear() {
        this.vars.clear()
        this.files.clear()
        this.additionalVariables.clear()
    }

    /**
     * Copy these settings to another instance.
     *
     * @param target Target instance
     *
     * @since 0.12
     */
    void copyTo(VarsFilesPair target) {
        target.files.addAll(this.files)
        target.vars.putAll(this.vars)
        target.additionalVariables.addAll(this.additionalVariables)
    }

    /**
     * Gets all variables in a Terraform file format.
     *
     * @return List of pairings.
     *
     * @since 0.13
     */
    List<String> getVarsInTfFormat() {
        escapedVars.collect { String k, String v ->
            "$k = $v".toString()
        }
    }

    /**
     * Evaluate all provided and local variables and convert them to Terraform-compliant strings, ready to be
     * passed to command-line.
     *
     * Provided variables will be evaluated first, so that any local definitions can override them.
     *
     * <p> Calling this will resolve all lazy-evaluated entries.
     *
     * @return Map where each key is the name of a variable. Each value is correctly formatted according to
     *   the kind of variable.
     *
     * @since 0.12
     */
    Map<String, String> getEscapedVars() {
        Map<String, String> hclMap = escapeProvidedVars()
        hclMap.putAll(escapeLocalVars())
        hclMap
    }

    /**
     * Returns a string representation of the object.
     *
     * @return a string representation of the object.
     */
    @Override
    String toString() {
        "vars=${vars}, files=${files}, additionalsCount=${additionalVariables.size()}"
    }

    private Map<String, String> escapeProvidedVars() {
        final vars2 = new IntermediateVariableSpec(new VarsFilesPair(this.ccso), this.ccso)
        for (Action<VariablesSpec> additional : this.additionalVariables) {
            additional.execute(vars2)
        }
        vars2.vfp.vars.collectEntries { k, v ->
            [k, v.value]
        }
    }

    private Map<String, String> escapeLocalVars() {
        this.vars.collectEntries { k, v ->
            [k, v.value]
        }
    }

    private final ConfigCacheSafeOperations ccso

    private static class IntermediateVariableSpec implements VariablesSpec {
        final VarsFilesPair vfp
        final RemoteStateVarProvider remoteStateMap

        IntermediateVariableSpec(VarsFilesPair vfp, ConfigCacheSafeOperations ccso) {
            this.vfp = vfp
            this.ccso = ccso
        }

        @Override
        void var(String name, Object value) {
            vfp.vars.put(name, TokenValue.of(value, ccso))
        }

        @Override
        void map(Map<String, ?> map, String name) {
            vfp.vars.put(name, TokenValue.of(map, ccso))
        }

        @Override
        void map(String name, Provider<Map<String, ?>> mapProvider) {
            vfp.vars.put(name, TokenValue.of(mapProvider, ccso))
        }

        @Override
        void list(String name, Object val1, Object... vals) {
            vfp.vars.put(name, TokenValue.of([val1] + vals.toList(), ccso))
        }

        @Override
        void list(String name, Iterable<?> vals) {
            vfp.vars.put(name, TokenValue.of(vals, ccso))
        }

        @Override
        void files(Object... fileNames) {
            fileNames.each { fileName -> vfp.files.add(fileName) }
        }

        @Override
        void provider(Action<VariablesSpec> additionalVariables) {
            vfp.additionalVariables.add(additionalVariables)
        }

        @Override
        void remoteStateMap(Action<RemoteStateVarProvider> configurator) {
        }

        @Override
        @SuppressWarnings('UnnecessaryDotClass')
        void remoteStateMap(@DelegatesTo(RemoteStateVarProvider.class) Closure<?> configurator) {
        }

        private final ConfigCacheSafeOperations ccso
    }
}

