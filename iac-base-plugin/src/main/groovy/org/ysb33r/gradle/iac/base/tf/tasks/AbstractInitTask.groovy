/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.iac.base.tf.tasks

import groovy.transform.CompileStatic
import org.gradle.api.Task
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.gradle.api.specs.Spec
import org.gradle.api.tasks.InputFile
import org.gradle.api.tasks.Internal
import org.gradle.api.tasks.OutputFile
import org.gradle.api.tasks.PathSensitive
import org.gradle.api.tasks.PathSensitivity
import org.gradle.api.tasks.options.Option
import org.ysb33r.gradle.iac.base.internal.tf.TaskUtils
import org.ysb33r.gradle.iac.base.tf.execspec.DefaultArguments
import org.ysb33r.gradle.iac.base.tf.execspec.IacTfExecSpec
import org.ysb33r.grolifant5.api.core.ExclusiveFileAccess

import java.time.LocalDateTime

import static java.util.Collections.EMPTY_LIST

/**
 * Abstract infra init task.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
abstract class AbstractInitTask extends AbstractTfStandardTask {

    /**
     * Whether modules should be upgraded
     *
     * This option can be set from the command-line with {@code --upgrade=true}.
     *
     * @param flag {@code true to turn option on}
     */
    @Option(option = 'upgrade', description = 'Force upgrade of modules and plugins when not offline')
    void setUpgrade(boolean flag) {
        this.upgrade.set(flag)
    }

    /**
     * Skip initialisation of child modules.
     *
     * @param flag {@code true to turn option on}
     */
    void setSkipChildModules(boolean flag) {
        this.skipChildModules.set(flag)
    }

    /**
     * Whether backend configuration should be skipped.
     *
     * This option can be set from the command-line with {@code --no-configure-backends}
     *
     * @param flag {@code true to turn option on}
     *
     * @since 0.6.0
     */
    @Option(option = 'no-configure-backends', description = 'Skip backend configuration')
    void setSkipConfigureBackends(boolean flag) {
        this.skipConfigureBackends.set(flag)
    }

    @Option(option = 'force-copy', description = 'Automatically answer yes to any backend migration questions')
    void setForceCopy(boolean flag) {
        this.forceCopy.set(flag)
    }

    @Option(option = 'reconfigure',
        description = 'Disregard any existing configuration and prevent migration of existing state')
    void setReconfigure(boolean flag) {
        this.reconfigure.set(flag)
    }

    /**
     * The location of the state file
     *
     * @since 0.10
     */
    @OutputFile
    final Provider<File> stateFile

    /**
     * The location of the file that provides details about the last run of this task.
     */
    @OutputFile
    final Provider<File> initStateFile

    /**
     * Configuration of a state backend.
     *
     * See https://www.terraform.io/docs/backends/config.html#partial-configuration
     *
     * @return Location of configuration file. Can be empty if none is required.
     */
    @InputFile
    @PathSensitive(PathSensitivity.RELATIVE)
    Provider<File> getBackendConfigFile() {
        this.backendConfig
    }

    /**
     * Set location of backend configuration file.
     *
     * @param location File location
     */
    void setBackendConfigFile(Provider<File> location) {
        this.backendConfig.set(location)
    }

    /**
     * Sets the plugin directory to use with this task.
     *
     * @param dir Plugin cache directory
     */
    void setPluginDir(Provider<File> dir) {
        this.pluginDir.set(dir)
    }

    /**
     * Set whether the plugin directory should be locked whilst this task is running.
     *
     * <p>This is a Gradle lock and nothing to do with any locks within {@code terraform/tofu}. </p>
     *
     * @param lockTimeout The timeout to be used for the lock.
     *   Pass an empty provider or a provider with a value less than 1 to not use a lock.
     */
    void setLockPluginDirWithTimeout(Provider<Integer> lockTimeout) {
        this.pluginDirLockTimeout.set(lockTimeout)
    }

    @Override
    void exec() {
        final pd = pluginDir.get()
        if (!pd.exists()) {
            pd.mkdirs()
        }
        final lockTimeout = pluginDirLockTimeout.getOrElse(0)
        removeDanglingSymlinks()
        if (lockTimeout > 0) {
            final efa = new ExclusiveFileAccess(lockTimeout, 200)
            efa.access(pd) {
                super.exec()
            }
        } else {
            super.exec()
        }

        initStateFile.get().withWriter { w ->
            w.println(LocalDateTime.now())
            fsOperations().fileTree(pd).files.each {
                if (it.file) {
                    w.println(it.absolutePath)
                }
            }
        }
    }

    protected AbstractInitTask(
        String statefileName,
        Class<? extends IacTfExecSpec> execSpecClass
    ) {
        super(
            'init',
            null,
            execSpecClass,
            DefaultArguments.builder().supportsInputs().supportsColor()
        )

        this.backendConfig = project.objects.property(File)
        this.pluginDir = project.objects.property(File)
        this.pluginDirLockTimeout = project.objects.property(Integer)
        this.upgrade = project.objects.property(Boolean)
        this.skipChildModules = project.objects.property(Boolean)
        this.skipConfigureBackends = project.objects.property(Boolean)
        this.forceCopy = project.objects.property(Boolean)
        this.reconfigure = project.objects.property(Boolean)

        this.upgrade.convention(false)
        this.skipChildModules.convention(false)
        this.skipConfigureBackends.convention(false)
        this.forceCopy.convention(false)
        this.reconfigure.convention(false)

        this.stateFile = dataDir.map { new File(it, statefileName) }
        this.initStateFile = dataDir.map { new File(it, '.init.txt') }

        configureFinalOptions()

        outputs.files(initStateFile, this.stateFile)
        notUpToDateOnCommandLineOptions()
        notUpToDateIfTheProvidersLockFileDoesNotExist()
    }

    @Internal
    protected Provider<File> getPluginDirectory() {
        this.pluginDir
    }

    // Written this way as the upToDateWhen(Closure) fails on CC
    private notUpToDateOnCommandLineOptions() {
        outputs.upToDateWhen(new Spec<Task>() {
            @Override
            boolean isSatisfiedBy(Task task) {
                final result = !(forceCopy.get() || upgrade.get() || reconfigure.get())
                if (!result) {
                    logger.info 'Out of date because one of --reconfigure, --upgrade or --force-copy was requested'
                }
                result
            }
        })
    }

    private notUpToDateIfTheProvidersLockFileDoesNotExist() {
        final lockFileExists = sourceDir.map { new File(it, '.terraform.lock.hcl').exists() }
        outputs.upToDateWhen(new Spec<Task>() {
            @Override
            boolean isSatisfiedBy(Task task) {
                final result = lockFileExists.get()
                if (!result) {
                    logger.info 'Out of date because providers lock file does not exist'
                }
                result
            }
        })
    }

    private void configureFinalOptions() {
        execSpec.cmd {
            addCommandLineArgumentProviders(
                upgrade.zip(skipChildModules) { upg, scm -> [upg, scm] }
                    .zip(projectTools().offlineProvider) { flags, offline ->
                        if (offline) {
                            logger.warn(
                                'Gradle is running in offline mode. ' +
                                    (flags[0] ? 'Upgrade will not be attempted. ' : '') +
                                    (flags[1] ? '' : 'Modules will not be retrieved. ')
                            )
                            ['-get=false']
                        } else {
                            ["-get=${!flags[1]}".toString()] + (flags[0] ? ['-upgrade'] : EMPTY_LIST)
                        }
                    }
            )

            addCommandLineArgumentProviders(
                skipConfigureBackends.map { ["-backend=${!it}".toString()] },
                backendConfig.map {
                    ["-backend-config=${it.absolutePath}".toString()]
                }.orElse(EMPTY_LIST),
                forceCopy.map { it ? ['-force-copy'] : EMPTY_LIST },
                reconfigure.map { it ? ['-reconfigure'] : EMPTY_LIST }
            )
        }
    }

    private void removeDanglingSymlinks() {
        TaskUtils.removeDanglingSymlinks(new File(dataDir.get(), PLUGIN_SUBDIR))
    }

    private final Property<File> backendConfig
    private final Property<File> pluginDir
    private final Property<Boolean> upgrade
    private final Property<Boolean> skipChildModules
    private final Property<Boolean> skipConfigureBackends
    private final Property<Boolean> forceCopy
    private final Property<Boolean> reconfigure
    private final Property<Integer> pluginDirLockTimeout
    private static final String PLUGIN_SUBDIR = 'plugins'
}
