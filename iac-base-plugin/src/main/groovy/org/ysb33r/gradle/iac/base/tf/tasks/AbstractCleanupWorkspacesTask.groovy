/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.iac.base.tf.tasks

import groovy.transform.CompileStatic
import org.gradle.api.provider.ListProperty
import org.gradle.api.provider.Provider
import org.gradle.api.tasks.Internal
import org.gradle.api.tasks.options.Option
import org.ysb33r.gradle.iac.base.tf.execspec.IacTfExecSpec

/**
 * The {@code workspace delete} command.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class AbstractCleanupWorkspacesTask extends AbstractTfStandardTask {

    protected AbstractCleanupWorkspacesTask(
        String defaultWorkspaceName,
        Class<? extends IacTfExecSpec> execSpecClass
    ) {
        super(
            'workspace',
            defaultWorkspaceName,
            execSpecClass
        )

        this.workspaceNames = project.objects.listProperty(String)
        this.defaultWorkspaceName = defaultWorkspaceName
    }

    @Option(option = 'force', description = 'Forces local removal of workspaces')
    @Internal
    boolean forceRemoval = false

    void setWorkspaces(Provider<List<String>> ws) {
        this.workspaceNames.set(ws)
    }

    @Override
    @SuppressWarnings('UnnecessaryGetter')
    void exec() {
        List<String> expectedWorkspaces = this.workspaceNames.get()

        final currentWorkspaces = listWorkspaces()
        final currentWorkspace = currentWorkspaces.find { k, v -> v }.key
        Set<String> removeWorkspaces = currentWorkspaces.keySet().findAll { it != defaultWorkspaceName }
        removeWorkspaces.removeAll(expectedWorkspaces)
        logger.info "Required additional workspaces are: ${expectedWorkspaces}"
        if (removeWorkspaces.empty) {
            logger.info 'Found no workspaces to remove'
        } else {
            logger.info "Will remove old workspaces: ${removeWorkspaces.join(', ')}"

            if (removeWorkspaces.contains(currentWorkspace)) {
                logger.info "Switching to '${defaultWorkspaceName}' workspace first"
                runWorkspaceSubcommand('select', defaultWorkspaceName)
            }
        }
        removeWorkspaces.forEach { String ws ->
            if (forceRemoval) {
                runWorkspaceSubcommand(DELETE_CMD, '-force', ws)
            } else {
                runWorkspaceSubcommand(DELETE_CMD, ws)
            }
        }
    }

    private final ListProperty<String> workspaceNames
    private final String defaultWorkspaceName
    private static final String DELETE_CMD = 'delete'
}
