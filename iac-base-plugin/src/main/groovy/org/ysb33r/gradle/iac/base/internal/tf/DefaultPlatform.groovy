/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.iac.base.internal.tf

import groovy.transform.CompileStatic
import org.gradle.api.model.ObjectFactory
import org.gradle.api.provider.Provider
import org.gradle.api.provider.SetProperty
import org.ysb33r.gradle.iac.base.tf.IacPlatforms

import javax.inject.Inject

/**
 * Base class for implementing platforms
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class DefaultPlatform implements IacPlatforms {

    final Set<String> allPlatforms

    @Inject
    DefaultPlatform(Set<String> platformSet, ObjectFactory objectFactory) {
        this.platforms = objectFactory.setProperty(String)
        this.allPlatforms = platformSet.asImmutable()
    }

    /**
     * Add one or more platforms to support for providers.
     *
     * Use {@link #getAllPlatforms} to add all platforms supported bu this plugin.
     *
     * @param reqPlatforms Platforms to add.
     */
    @Override
    void platforms(Iterable<String> reqPlatforms) {
        this.platforms.addAll(reqPlatforms)
    }

    /**
     * Add one or more platforms to support for providers.
     *
     * Use {@link #getAllPlatforms} to add all platforms supported by this plugin.
     *
     * @param reqPlatforms Platforms to add.
     */
    @Override
    void platforms(String... reqPlatforms) {
        this.platforms.addAll(reqPlatforms)
    }

    /**
     * Provide the list of platforms that need to be supported.
     *
     * If empty, only the current platform will be supported.
     *
     * @return List of supported platforms.
     */
    @Override
    Provider<Set<String>> getPlatforms() {
        this.platforms
    }

    private final SetProperty<String> platforms
}
