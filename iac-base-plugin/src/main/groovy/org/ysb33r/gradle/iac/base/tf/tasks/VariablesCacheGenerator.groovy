/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.iac.base.tf.tasks

import groovy.transform.CompileStatic
import org.gradle.api.provider.ListProperty
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.gradle.api.tasks.CacheableTask
import org.gradle.api.tasks.OutputFile
import org.gradle.api.tasks.TaskAction
import org.ysb33r.grolifant5.api.core.runnable.GrolifantDefaultTask

/**
 * Generates the file that stores source set variables
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
@CacheableTask
class VariablesCacheGenerator extends GrolifantDefaultTask {

    VariablesCacheGenerator() {
        this.varsFile = project.objects.property(File)
        this.vars = project.objects.listProperty(String)
        this.vars.convention([])
        inputs.property('vars', this.vars).optional(true)
    }

    /**
     * The location of the variables file.
     *
     * @return Location provider
     */
    @OutputFile
    Provider<File> getVariableFile() {
        this.varsFile
    }

    /**
     * Sets the location of the variables file.
     * @param file Location provider.
     */
    void setVariablesFile(Provider<File> file) {
        this.varsFile.set(file)
    }

    /**
     * Sets the variables to be cached.
     *
     * @param vars Provider of variables.
     */
    void setVariables(Provider<List<String>> vars) {
        this.vars.set(vars)
    }

    @TaskAction
    void exec() {
        final allVars = this.vars.get().flatten()
        varsFile.get().withWriter { writer ->
            allVars.each { writer.println(it) }
        }
    }

    private final Property<File> varsFile
    private final ListProperty<String> vars
}
