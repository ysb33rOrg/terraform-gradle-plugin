/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.iac.base.tf.tasks

import groovy.transform.CompileStatic
import org.gradle.api.provider.ListProperty
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.ysb33r.gradle.iac.base.tf.execspec.DefaultArguments
import org.ysb33r.gradle.iac.base.tf.execspec.IacTfExecSpec
import org.ysb33r.grolifant5.api.core.runnable.ExecOutput

import static java.util.Collections.EMPTY_LIST

/**
 * Base class for custom formatting tasks.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
abstract class AbstractCustomFmtBaseTask extends AbstractTfBaseTask {

    void setRecursive(Provider<Boolean> flag) {
        this.recursive.set(flag)
    }

    void setGlobalConfigFile(Provider<File> file) {
        this.configFile.set(file)
    }

    void setSourceDirectories(Provider<Set<File>> dirs) {
        this.sourceDirectories.set(dirs)
    }

    void setPluginDir(Provider<File> dir) {
        this.pluginDir.set(dir)
    }

    @Override
    void exec() {
        for (File target : sourceDirectories.get()) {
            final result = execSpec.submitAsExec { spec ->
                spec.workingDir(target)
                spec.ignoreExitValue = true
            }
            handleExecResult(result)
        }
    }

    protected AbstractCustomFmtBaseTask(Class<? extends IacTfExecSpec> execSpecClass) {
        super('fmt', execSpecClass, DefaultArguments.builder())
        this.dataDir = fsOperations().buildDirDescendant("tf/${name}")
        this.configFile = project.objects.property(File)
        this.recursive = project.objects.property(Boolean)
        this.sourceDirectories = project.objects.listProperty(File)
        this.pluginDir = project.objects.property(File)

        inputs.files('dirs', this.sourceDirectories).ignoreEmptyDirectories().skipWhenEmpty()

        execSpec.entrypoint.executable(this.executableLocation)
        execSpec.process {
            addEnvironmentProvider(
                this.dataDir.zip(this.configFile) { dir, cfg ->
                    [
                        TF_DATA_DIR       : dir.absolutePath,
                        TF_CLI_CONFIG_FILE: cfg.absolutePath
                    ]
                }.zip(this.pluginDir) { map, dir ->
                    map + [TF_PLUGIN_CACHE_DIR: dir.absolutePath]
                }
            )
        }

        execSpec.cmd {
            addCommandLineArgumentProviders(this.recursive.map { it ? ['-recursive'] : EMPTY_LIST })
        }
    }

    abstract protected void handleExecResult(ExecOutput result)

    private final Provider<File> dataDir
    private final Property<File> configFile
    private final Property<Boolean> recursive
    private final ListProperty<File> sourceDirectories
    private final Property<File> pluginDir
}
