/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.iac.base.tf.tasks

import groovy.transform.CompileStatic
import org.gradle.api.provider.Provider
import org.ysb33r.gradle.iac.base.tf.execspec.IacTfExecSpec
import org.ysb33r.grolifant5.api.core.StringTools

/** Equivalent of {@code providers lock}.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class AbstractProvidersLockTask extends AbstractProviderTask {
    AbstractProvidersLockTask(
        String lockFileName,
        Class<? extends IacTfExecSpec> execSpecClass
    ) {
        super('lock', execSpecClass)
        this.providerLockFile = sourceDir.map { new File(it, lockFileName) }
        skipIfOffline()

        execSpec.cmd {
            commandLineArgumentProviders.add(
                requiredPlatforms.map { list ->
                    list.collect {
                        "-platform=${it}".toString()
                    }
                }
            )
        }

        // TODO: -net-mirror and -fs-mirror. See https://gitlab.com/ysb33rOrg/terraform-gradle-plugin/-/issues/93
    }

    @Override
    protected void exec() {
        final file = providerLockFile.get()
        final checksum1 = file.exists() ? file.text.sha256() : StringTools.EMPTY
        super.exec()
        final checksum2 = file.exists() ? file.text.sha256() : StringTools.EMPTY
        didWork = checksum1 != checksum2
    }

    private final Provider<File> providerLockFile
}
