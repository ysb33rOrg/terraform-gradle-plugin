/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.iac.base.internal.tf

import groovy.transform.CompileStatic
import org.gradle.api.ExtensiblePolymorphicDomainObjectContainer
import org.gradle.api.model.ObjectFactory
import org.ysb33r.gradle.iac.base.secrets.AwsSecrets
import org.ysb33r.gradle.iac.base.secrets.GenericSecrets
import org.ysb33r.gradle.iac.base.secrets.GitlabSecrets
import org.ysb33r.gradle.iac.base.secrets.IacSecrets

/**
 * Utilities for dealing with secrets
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class SecretUtils {
    static void registerIacSecretFactories(
        ExtensiblePolymorphicDomainObjectContainer<? extends IacSecrets> container,
        ObjectFactory objects
    ) {
        container.registerFactory(AwsSecrets) { name -> objects.newInstance(AwsSecrets, name) }
        container.registerFactory(GitlabSecrets) { name -> objects.newInstance(GitlabSecrets, name) }
        container.registerFactory(GenericSecrets) { name -> objects.newInstance(GenericSecrets, name) }
    }
}
