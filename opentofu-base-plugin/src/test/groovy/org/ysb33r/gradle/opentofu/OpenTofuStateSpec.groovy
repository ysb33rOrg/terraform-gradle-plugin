/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.opentofu

import org.ysb33r.gradle.opentofu.internal.OpenTofuModel
import org.ysb33r.gradle.opentofu.testfixtures.IntegrationSpecification

import static org.gradle.testkit.runner.TaskOutcome.SUCCESS

class OpenTofuStateSpec extends IntegrationSpecification {

    public static final String RESOURCE_NAME = 'test'
    public static final String PREFIX = OpenTofuModel.PREFIX

    void setup() {
        writeBasicBuildFile()
        writeHashiRandomProvidersFile()
        writeMainTf()
    }

    void 'Can move a resource'() {
        setup:
        final taskName = "${PREFIX}StateMv"
        final newResourceName = 'test2'
        writeMainTf()
        applyInit()

        when:
        writeMainTf(newResourceName)
        final result = getGradleRunner(IS_GROOVY_DSL, [
            '-i',
            taskName,
            '--from-path', "random_uuid.${RESOURCE_NAME}",
            '--to-path', "random_uuid.${newResourceName}"
        ]*.toString()).build()

        then:
        result.task(":${taskName}").outcome == SUCCESS
    }

    void 'Can remove a resource'() {
        final taskName = "${PREFIX}StateRm"
        writeMainTf()
        applyInit()

        when:
        final result = getGradleRunner(IS_GROOVY_DSL, [
            '-i',
            taskName,
            '--path', "random_uuid.${RESOURCE_NAME}"
        ]*.toString()).build()

        then:
        result.task(":${taskName}").outcome == SUCCESS
    }

    void 'Can pull and push a state file'() {
        final pullTask = "${PREFIX}StatePull"
        final pushTask = "${PREFIX}StatePush"
        final stateFile = 'backup.tfstate'
        writeMainTf()
        applyInit()

        when:
        final resultPull = getGradleRunner(IS_GROOVY_DSL, [
            pullTask,
            '--state-file', stateFile
        ]*.toString()).build()

        new File(buildDir, 'tf').deleteDir()
        getGradleRunner(IS_GROOVY_DSL, ["${PREFIX}Init".toString()]).build()

        final resultPush = getGradleRunner(IS_GROOVY_DSL, [
            '-i',
            pushTask,
            '--state-file', stateFile
        ]*.toString()).build()

        then:
        resultPull.task(":${pullTask}").outcome == SUCCESS
        resultPush.task(":${pushTask}").outcome == SUCCESS
    }

    private void applyInit() {
        getGradleRunner(IS_GROOVY_DSL, ["${PREFIX}Init", "${PREFIX}Apply"]*.toString()).build()
    }

    private void writeMainTf(String resourceName = RESOURCE_NAME) {
        new File(srcDir, 'main.tf').text = """
        resource "random_uuid" "${resourceName}" {
        }
        """.stripIndent()
    }
}