/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.opentofu

import org.ysb33r.gradle.opentofu.extensions.OpenTofuExtension
import org.ysb33r.gradle.opentofu.plugins.OpenTofuBasePlugin
import org.ysb33r.gradle.opentofu.testfixtures.UnitTestSpecification

class OpenTofuToolchainSpec extends UnitTestSpecification {

    OpenTofuExtension opentofu

    void setup() {
        project.pluginManager.apply(OpenTofuBasePlugin)

        opentofu = project.extensions.getByType(OpenTofuExtension)
    }

    void 'When the plugin is applied a default toolchain will be added'() {
        expect:
        opentofu.toolchains.getByName('standard')
    }

    void 'Can add a 2nd plugin'() {
        setup:
        opentofu.toolchains {
            extra {
                executableByVersion(OPENTOFU_TEST_VERSIONS[0])
            }
        }

        expect:
        opentofu.toolchains.getByName('extra').runExecutableAndReturnVersion() == OPENTOFU_TEST_VERSIONS[0]
        true
    }
}