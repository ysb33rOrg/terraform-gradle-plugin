/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.opentofu

import org.gradle.testkit.runner.GradleRunner
import org.ysb33r.gradle.opentofu.internal.OpenTofuModel
import org.ysb33r.gradle.opentofu.testfixtures.IntegrationSpecification

import static org.gradle.testkit.runner.TaskOutcome.SUCCESS

class OpenTofuProvidersSpec extends IntegrationSpecification {

    File reportsDir

    void setup() {
        writeBasicBuildFile()
        createTF()
        reportsDir = new File(buildDir, 'reports/tf/main')
    }

    void 'tfProvidersShow describes the providers on the console'() {
        when:
        final result1 = nextRunner('tofuProvidersShow').build()

        then:
        result1.task(':tofuProvidersShow').outcome == SUCCESS
        result1.output.contains('provider[registry.opentofu.org/hashicorp/random]')
    }

    void 'tofuProvidersLock will lock more than the current platform'() {
        when: 'tofuProvidersLock is run the first time'
        final result1 = nextRunner('tofuProvidersLock').build()
        final lockFile = new File(srcDir, OpenTofuModel.PROVIDER_LOCK_FILENAME)

        then: 'the task is executed'
        result1.task(':tofuProvidersLock').outcome == SUCCESS
        result1.output.contains('darwin_arm64')
        result1.output.contains('windows_386')
        lockFile.exists()
    }

    void 'tofuProvidersSchema puts schema in an output file'() {
        when:
        final result1 = nextRunner('tofuProvidersSchema').build()
        final schemaFile = new File(reportsDir, 'main.schema.json')

        then:
        result1.task(':tofuInit').outcome == SUCCESS
        result1.task(':tofuProvidersSchema').outcome == SUCCESS
        schemaFile.exists()
    }

    GradleRunner nextRunner(String taskName) {
        getGradleRunner(IS_GROOVY_DSL, ['tofuInit', taskName])
    }

    void createTF() {
        writeHashiRandomProvidersFile()

        buildFile << '''
        opentofu {
            platforms (allPlatforms.findAll { !it.startsWith('freebsd') })
        }
        '''
    }
}