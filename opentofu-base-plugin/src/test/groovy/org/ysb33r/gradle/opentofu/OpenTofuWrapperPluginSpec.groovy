/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.opentofu

import org.ysb33r.gradle.opentofu.internal.OpenTofuModel
import org.ysb33r.gradle.opentofu.plugins.OpenTofuWrapperPlugin
import org.ysb33r.gradle.opentofu.tasks.OpenTofuCacheBinary
import org.ysb33r.gradle.opentofu.testfixtures.IntegrationSpecification

import static org.gradle.testkit.runner.TaskOutcome.SUCCESS
import static org.ysb33r.gradle.opentofu.internal.plugins.OpenTofuGlobalConfigForSingleProjectPlugin.RC_GENERATOR

class OpenTofuWrapperPluginSpec extends IntegrationSpecification {

    String wrapperExtensionName = OpenTofuWrapperPlugin.WRAPPER_EXTENSION_NAME
    String wrapperTaskName = OpenTofuWrapperPlugin.WRAPPER_TASK_NAME
    String cacheTaskName = OpenTofuWrapperPlugin.CACHE_BINARY_TASK_NAME
    String toolName = OpenTofuModel.EXE_BASE_NAME

    void 'Can cache a binary on behalf of a wrapper'() {
        setup:
        final propsFile = new File(projectDir, ".gradle/${OpenTofuCacheBinary.LOCATION_PROPERTIES_DEFAULT}")
        writeRootBuildFile()

        when:
        final result = getGradleRunner(IS_GROOVY_DSL, [cacheTaskName]).build()

        then:
        result.task(":${cacheTaskName}").outcome == SUCCESS
        result.task(":${RC_GENERATOR}").outcome == SUCCESS
        propsFile.exists()
    }

    void 'Can generate wrappers'() {
        setup:
        writeRootBuildFile()
        final scripts = ["${toolName}w", "${toolName}w.bat"].collect {
            new File(projectDir, it)
        }
        when:
        final result = getGradleRunner(IS_GROOVY_DSL, [wrapperTaskName]).build()

        then:
        result.task(":${wrapperTaskName}").outcome == SUCCESS
        result.task(":${RC_GENERATOR}").outcome == SUCCESS
        scripts.every { !it.text.contains('~~') }
    }

    private void writeRootBuildFile() {
        buildFile.text = """
        plugins {
            id 'org.ysb33r.${EXTENSION_NAME}.wrapper'
        }
        
        ${wrapperExtensionName} {
        }
        
        """.stripIndent()
    }
}