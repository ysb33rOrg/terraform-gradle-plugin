/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.opentofu

import org.ysb33r.gradle.opentofu.internal.OpenTofuModel
import org.ysb33r.gradle.opentofu.testfixtures.IntegrationSpecification

import static org.gradle.testkit.runner.TaskOutcome.SUCCESS
import static org.gradle.testkit.runner.TaskOutcome.UP_TO_DATE
import static org.ysb33r.gradle.opentofu.internal.OpenTofuModel.EXTENSION_NAME

class OpenTofuOutputVariablesProviderSpec extends IntegrationSpecification {

    public static final String PREFIX = OpenTofuModel.PREFIX
    public static final String RESOURCE_NAME = 'test'
    public static final String TASKNAME = 'foo'

    File mainTF

    void setup() {
        mainTF = new File(srcDir, 'main.tf')
        writeBasicBuildFile()
        writeHashiRandomProvidersFile()
        addTaskToReadValues()
        createTF()
    }

    void 'Can cache values from outputs and use as provider'() {
        when:
        final result = getGradleRunner(IS_GROOVY_DSL, [TASKNAME, "${TASKNAME}2"]*.toString()).build()

        then:
        result.task(":${TASKNAME}").outcome == SUCCESS
        result.task(":${PREFIX}CacheOutputVariables").outcome == SUCCESS
        result.task(":${PREFIX}Apply").outcome == SUCCESS
        result.output.contains('UUID =')

        when:
        final result2 = getGradleRunner(IS_GROOVY_DSL, [TASKNAME]).build()

        then:
        result2.task(":${PREFIX}CacheOutputVariables").outcome == SUCCESS
        result2.task(":${PREFIX}Apply").outcome == UP_TO_DATE
    }

    void addTaskToReadValues() {
        buildFile << """
        tasks.register('${TASKNAME}') {
          final p = ${EXTENSION_NAME}.sourceSets.main.rawOutputVariable('${RESOURCE_NAME}')
          final m = ${EXTENSION_NAME}.sourceSets.main.rawOutputVariable('in_a_map')
          inputs.property('p',p)
          inputs.property('m',m)
          doLast {
            println "UUID = \${p.get()}"
            println "UUID = \${m.get()}"
          }
        }
        
        tasks.register('${TASKNAME}2') {
          final p = ${EXTENSION_NAME}.sourceSets.main.rawOutputVariable('${RESOURCE_NAME}')
          final m = ${EXTENSION_NAME}.sourceSets.main.rawOutputVariable('in_a_map')
          inputs.property('p',p)
          inputs.property('m',m)
          doLast {
            println "UUID = \${p.get()}"
            println "UUID = \${m.get()}"
          }
        }
        """.stripIndent()
    }

    void createTF() {
        mainTF.text = """
        resource "random_uuid" "${RESOURCE_NAME}" {
        }
        
        output "${RESOURCE_NAME}" {
            value = random_uuid.${RESOURCE_NAME}.result
        }
        
        output "in_a_map" {
            value = { uuid = random_uuid.${RESOURCE_NAME}.result }
        }
        """.stripIndent()
    }
}