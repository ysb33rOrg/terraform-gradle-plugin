/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.opentofu

import org.gradle.testkit.runner.GradleRunner
import org.ysb33r.gradle.opentofu.internal.OpenTofuUtils
import org.ysb33r.gradle.opentofu.testfixtures.IntegrationSpecification

import static org.gradle.testkit.runner.TaskOutcome.NO_SOURCE
import static org.gradle.testkit.runner.TaskOutcome.SUCCESS
import static org.gradle.testkit.runner.TaskOutcome.UP_TO_DATE

class OpenTofuInitSpec extends IntegrationSpecification {

    String taskName = 'tofuInit'
    String providerVersion = '2.70.0'

    void setup() {
        writeBasicBuildFile()
    }

    void 'Run init on a clean project directory'() {
        when:
        final result = getGradleRunner(IS_GROOVY_DSL, [taskName]).build()

        then:
        result.task(":${taskName}").outcome == NO_SOURCE
    }

    void 'Run init on a project with a single plugin'() {
        setup:
        File pluginDir = new File(
            testKitDir,
            "caches/opentofu.d/registry.opentofu.org/hashicorp/aws/${providerVersion}/${OpenTofuUtils.osArch()}"
        )
        writeTfInit()

        when:
        final result = getGradleRunner(IS_GROOVY_DSL, [taskName]).build()

        then:
        result.task(":${taskName}").outcome == SUCCESS
        new File(testKitDir, 'caches/opentofu.d').exists()
        pluginDir.exists()
        pluginDir.listFiles().find { it.name.startsWith('terraform-') }
    }

    void 'Run init on a project with a single plugin and a custom plugin cache directory'() {
        setup:
        File pluginDir = new File(
            projectDir,
            ".gradle/.${PREFIX}.d/test-project.main/registry.opentofu.org/hashicorp/aws/${providerVersion}/${OpenTofuUtils.osArch()}"
        )
        writeBuildFile()
        writeTfInit()

        buildFile << """
        ${EXTENSION_NAME} {
          sourceSets {
            main {
              useCustomPluginCache()
            }
          }
        }
        """.stripIndent()

        when:
        final result = getGradleRunner(IS_GROOVY_DSL, [taskName]).build()

        then:
        result.task(":${taskName}").outcome == SUCCESS
        pluginDir.exists()
        pluginDir.listFiles().find { it.name.startsWith('terraform-') }
    }

    void '--reconfigure will cause out of date condition'() {
        setup:
        final gradleRunner = getGradleRunnerConfigCache(IS_GROOVY_DSL, [taskName])
        writeTfInit()

        when: 'tfInit is run the first time'
        final result1 = gradleRunner.build()

        then: 'the task is executed'
        result1.task(":${taskName}").outcome == SUCCESS

        when: 'tfInit is run the second time'
        final result2 = gradleRunner.withArguments(taskName, '-i').build()

        then: 'the task is not executed'
        result2.task(":${taskName}").outcome == UP_TO_DATE

        when: 'tfInit is run with --reconfigure'
        final result3 = nextCacheRunner('--reconfigure').build()

        then: 'the task is executed'
        result3.task(":${taskName}").outcome == SUCCESS

        when: 'tfInit is run with --force-copy'
        final result4 = nextCacheRunner('--force-copy').build()

        then: 'the task is executed'
        result4.task(":${taskName}").outcome == SUCCESS

        when: 'tfInit is run with --upgrade'
        final result5 = nextCacheRunner('--upgrade').build()

        then: 'the task is executed'
        result5.task(":${taskName}").outcome == SUCCESS

        when: 'tfInit is then run again without parameters'
        final result6 = nextCacheRunner().build()

        then: 'the task is not executed'
        result6.task(":${taskName}").outcome == UP_TO_DATE
    }

    void 'Can initialise a subproject'() {
        setup:
        final subprojectName = 'infra'
        final subproject = new File(projectDir, subprojectName)
        subproject.mkdirs()
        writeBuildFile(subproject)
        writeTfInit(subproject)

        when:
        final result = getGradleRunner(IS_GROOVY_DSL, [taskName]).build()

        then:
        result.task(":${subprojectName}:${taskName}").outcome == SUCCESS
        new File(testKitDir, 'caches/opentofu.d').exists()
    }

    GradleRunner nextCacheRunner(String... args) {
        List<String> args1 = [
            taskName,
            '-s', '-i'
        ]
        args1.addAll(args)

        getGradleRunnerConfigCache(IS_GROOVY_DSL, args1)
    }

    void writeBuildFile(File subProjectDir = null) {
        final target = subProjectDir ? new File(subProjectDir, 'build.gradle') : buildFile
        target.text = '''
        import org.ysb33r.gradle.opentofu.backends.LocalBackend
        plugins {
            id 'org.ysb33r.opentofu.base'
        }
        
        opentofu {
          backends {
            local(LocalBackend) {
                path = '../terraform.tfstate'
            }
          }
          sourceSets {
            main {
              useBackend('local')
            }
          }
        }
        '''.stripIndent()

        if (subProjectDir) {
            buildFile.text = '''
            plugins {
                id 'org.ysb33r.opentofu.rc'
                id 'org.ysb33r.opentofu.base' apply false
            }
            '''.stripIndent()

            settingsFile << """
            include '${subProjectDir.name}'
            """.stripIndent()
        }
    }

    void writeTfInit(File subProjectDir = null) {
        final target = subProjectDir ? new File(subProjectDir, 'src/tf/main/init.tf') : new File(srcDir, 'init.tf')
        target.parentFile.mkdirs()
        target.text = """
        terraform {
            backend "local" {
            }
            required_providers {
                aws = {
                  version = "= ${providerVersion}"
                  source = "hashicorp/aws"
                }
            }
        }

        provider "aws" {
          region  = "us-east-1"
        }
        """.stripIndent()
    }
}