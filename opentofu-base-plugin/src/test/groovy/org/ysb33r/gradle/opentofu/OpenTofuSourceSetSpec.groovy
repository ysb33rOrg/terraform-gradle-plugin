/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.opentofu

import org.gradle.api.Task
import org.gradle.api.tasks.TaskProvider
import org.ysb33r.gradle.opentofu.extensions.OpenTofuExtension
import org.ysb33r.gradle.opentofu.internal.OpenTofuModel
import org.ysb33r.gradle.opentofu.internal.OpenTofuTasks
import org.ysb33r.gradle.opentofu.plugins.OpenTofuBasePlugin
import org.ysb33r.gradle.opentofu.testfixtures.UnitTestSpecification
import spock.lang.Issue

import static org.ysb33r.gradle.opentofu.extensions.OpenTofuExtension.DEFAULT_TOOLCHAIN

class OpenTofuSourceSetSpec extends UnitTestSpecification {

    OpenTofuExtension opentofu
    OpenTofuSourceSet main

    void setup() {
        project.pluginManager.apply(OpenTofuBasePlugin)

        opentofu = project.extensions.getByType(OpenTofuExtension)
        main = opentofu.sourceSets.getByName(OpenTofuModel.DEFAULT_SOURCESET_NAME)
    }

    void 'Variables are present in 2nd sourceset even without a defined backend'() {
        setup:
        opentofu.sourceSets {
            extra {
            }
        }

        expect:
        opentofu.sourceSets.getByName('extra').escapedVariables.present
    }

    @Issue('https://gitlab.com/ysb33rOrg/gradle/terraform-gradle-plugin/-/issues/109')
    void 'Plan/Apply tasks in another source set is not dependent on Init task in another source set #setup between source sets'() {
        setup:
        opentofu.sourceSets {
            extra {
                if (mra) {
                    mustRunAfter('main')
                }
            }
        }

        when:
        final extraPlanTaskDependsOn = dependsOnTaskNames("${PREFIX}ExtraPlan")
        final extraApplyTaskDependsOn = dependsOnTaskNames("${PREFIX}ExtraApply")

        then:
        !extraPlanTaskDependsOn.find { it.startsWith("${PREFIX}") && !it.startsWith("${PREFIX}Extra") }
        !extraApplyTaskDependsOn.find { it.startsWith("${PREFIX}") && !it.startsWith("${PREFIX}Extra") }

        where:
        setup                           | mra
        'when there is no relationship' | false
        'when there is a relationship'  | true
    }

    @Issue('https://gitlab.com/ysb33rOrg/gradle/terraform-gradle-plugin/-/issues/109')
    void 'Plan/Apply tasks in another source set #link on Plan/Apply tasks in another source set #setup between source sets'() {
        setup:
        opentofu.sourceSets {
            extra {
            }
        }

        if (mra) {
            opentofu.sourceSets.extra.mustRunAfter('main')
        }

        when:
        final planTaskMRA = mraTaskNames("${PREFIX}Plan")
        final applyTaskMRA = mraTaskNames("${PREFIX}Apply")
        final extraPlanTaskMRA = mraTaskNames("${PREFIX}ExtraPlan")
        final extraApplyTaskMRA = mraTaskNames("${PREFIX}ExtraApply")

        then:
        !planTaskMRA.find { it.startsWith("${PREFIX}Extra") }
        !applyTaskMRA.find { it.startsWith("${PREFIX}Extra") }
        extraPlanTaskMRA.containsAll(["${PREFIX}Plan", "${PREFIX}Apply"]*.toString()) == mra
        extraApplyTaskMRA.containsAll(["${PREFIX}Plan", "${PREFIX}Apply"]*.toString()) == mra

        where:
        setup                           | link               | mra
        'when there is no relationship' | 'is not dependent' | false
        'when there is a relationship'  | 'must run after'   | true
    }

    @Issue('https://gitlab.com/ysb33rOrg/gradle/terraform-gradle-plugin/-/issues/109')
    void 'Plan/Apply tasks in a source set if not dependent on the Init task outputs #setup between source sets'() {
        setup:
        opentofu.sourceSets {
            extra {
            }
        }

        if (mra) {
            opentofu.sourceSets.extra.mustRunAfter('main')
        }

        when:
        final initOutputs = project.tasks.getByName("${PREFIX}Init").outputs.files.files
        final planInputs = project.tasks.getByName("${PREFIX}ExtraPlan").inputs.files.files
        final applyInputs = project.tasks.getByName("${PREFIX}ExtraApply").inputs.files.files

        then:
        !planInputs.any { it in initOutputs }
        !applyInputs.any { it in initOutputs }

        where:
        setup                           | mra
        'when there is no relationship' | false
        'when there is a relationship'  | true
    }

    void 'Uses the default toolchain unless otherwise configured'() {
        expect:
        main.executableLocation.get() == opentofu.toolchains.getByName(DEFAULT_TOOLCHAIN).executable.get()
    }

    void 'Has output directories'() {
        expect:
        main.logDir.get() == new File(projectDir, 'build/tf/main/logs')
        main.dataDir.get() == new File(projectDir, 'build/tf/main')
        main.reportsDir.get() == new File(projectDir, 'build/reports/tf/main')
    }

    void 'Has all tasks for default workspace'() {
        final prefix = OpenTofuModel.PREFIX
        final tasks = OpenTofuTasks.values()
            .collect { "${prefix}${it.command.capitalize()}" }

        expect:
        hasTask('createTofuBackendConfiguration')
        tasks.every { hasTask(it) }
    }

    void 'Has all tasks for a defined workspace'() {
        setup:
        final prefix = OpenTofuModel.PREFIX
        final wsName = 'staging'
        final tasks = OpenTofuTasks.values().findAll { !it.workspaceAgnostic }
            .collect { "${prefix}${it.command.capitalize()}${wsName.capitalize()}" }

        when:
        main.workspaces(wsName)

        then:
        tasks.every { hasTask(it) }
    }

    private Collection<String> dependsOnTaskNames(String taskName) {
        extractTaskNames(project.tasks.getByName(taskName).dependsOn)
    }

    private Collection<String> mraTaskNames(String taskName) {
        final task = project.tasks.getByName(taskName)
        task.mustRunAfter.getDependencies(task)
        extractTaskNames(task.mustRunAfter.getDependencies(task))
    }

    private Collection<String> extractTaskNames(Set<Object> taskies) {
        taskies.collect { t ->
            switch (t) {
                case Task:
                    return ((Task) t).name
                case TaskProvider:
                    return ((TaskProvider) t).name
                default:
                    projectOperations.stringTools.stringize(t)
            }
        }
    }

    private boolean hasTask(String name) {
        project.tasks.getByName(name)
    }
}