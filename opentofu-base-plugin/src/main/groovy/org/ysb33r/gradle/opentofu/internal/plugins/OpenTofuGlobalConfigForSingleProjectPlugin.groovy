/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.opentofu.internal.plugins

import groovy.transform.CompileStatic
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.ysb33r.gradle.iac.base.tf.tasks.GlobalConfigGenerator
import org.ysb33r.gradle.opentofu.extensions.OpenTofuGlobalConfigExtension
import org.ysb33r.gradle.opentofu.tasks.OpenTofuCustomFmtApply
import org.ysb33r.gradle.opentofu.tasks.OpenTofuInit

/**
 * Used internally for single projects.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class OpenTofuGlobalConfigForSingleProjectPlugin implements Plugin<Project> {
    public static final String RC_GENERATOR = 'createTofuConfig'

    @Override
    void apply(Project project) {
        final rc = project.extensions.create(
            OpenTofuGlobalConfigExtension.NAME,
            OpenTofuGlobalConfigExtension,
            project
        )

        project.tasks.create(RC_GENERATOR, GlobalConfigGenerator) {
            it.outputFile = rc.configFile
            it.authTokens = rc.credentialTokens
            it.configTokens = rc.configTokens
        }

        project.tasks.withType(OpenTofuInit).configureEach {
            it.dependsOn(RC_GENERATOR)
        }
        project.tasks.withType(OpenTofuCustomFmtApply).configureEach {
            it.dependsOn(RC_GENERATOR)
        }
    }
}
