/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.opentofu

import groovy.transform.CompileStatic
import org.gradle.api.Project
import org.ysb33r.gradle.iac.base.tf.AbstractIacToolchain
import org.ysb33r.gradle.opentofu.internal.Downloader
import org.ysb33r.grolifant5.api.core.ProjectOperations
import org.ysb33r.grolifant5.api.core.downloader.ExecutableDownloader
import org.ysb33r.grolifant5.api.errors.ConfigurationException

import javax.inject.Inject

import static org.ysb33r.grolifant5.api.core.StringTools.EMPTY

/**
 * Describes a toolchain for OpenTofu.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class OpenTofuToolchain extends AbstractIacToolchain {

    @Inject
    OpenTofuToolchain(String name, Project project) {
        super(name, project)

        final internalDownloader = new Downloader(ProjectOperations.find(project))
        this.executableDownloader = new ExecutableDownloader() {
            @Override
            File getByVersion(String version) {
                internalDownloader.getTofuExecutablePath(version).get()
            }
        }
    }

    @Override
    String toString() {
        "${this.class.name} '${name}'"
    }

    @Override
    protected ExecutableDownloader getDownloader() {
        this.executableDownloader
    }

    @Override
    @SuppressWarnings('CatchRuntimeException')
    protected String runExecutableAndReturnVersion() throws ConfigurationException {
        try {
            projectOperations.execTools.parseVersionFromOutput(
                ['version'],
                resolveExecutable()) { String output ->
                output.readLines()[0].replaceFirst('OpenTofu v', EMPTY)
            }
        } catch (RuntimeException e) {
            throw new ConfigurationException('Cannot determine version', e)
        }
    }

    private final ExecutableDownloader executableDownloader
}
