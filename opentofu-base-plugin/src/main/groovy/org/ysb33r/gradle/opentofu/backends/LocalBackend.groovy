/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.opentofu.backends

import groovy.transform.CompileStatic
import org.gradle.api.Project
import org.ysb33r.gradle.opentofu.extensions.OpenTofuExtension

import javax.inject.Inject

/**
 * OpenTofu local backend specification.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class LocalBackend extends OpenTofuBackend {

    public static final String DEFAULT_NAME = 'local'

    @Inject
    LocalBackend(String name, OpenTofuExtension parent, Project tempProjectRef) {
        super(name, parent, tempProjectRef)
    }

    /**
     * Sets a path where local state will be stored.
     *
     * @param p Path that can be evaluated to a file.
     */
    void setPath(Object p) {
        fileToken('path', p)
    }
}
