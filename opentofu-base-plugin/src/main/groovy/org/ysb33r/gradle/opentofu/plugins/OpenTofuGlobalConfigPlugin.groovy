/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.opentofu.plugins

import groovy.transform.CompileStatic
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.ysb33r.gradle.iac.base.IacBasePlugin
import org.ysb33r.gradle.opentofu.internal.plugins.OpenTofuGlobalConfigForMultiProjectRootPlugin
import org.ysb33r.gradle.opentofu.internal.plugins.OpenTofuGlobalConfigForMultiProjectSubProjectPlugin
import org.ysb33r.gradle.opentofu.internal.plugins.OpenTofuGlobalConfigForSingleProjectPlugin
import org.ysb33r.grolifant5.api.core.ProjectOperations

/**
 * Handles global OpenTofu configuration.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class OpenTofuGlobalConfigPlugin implements Plugin<Project> {
    @Override
    void apply(Project project) {
        project.pluginManager.apply(IacBasePlugin)
        project.pluginManager.apply(internalPluginClass(project))
    }

    Class<? extends Plugin<Project>> internalPluginClass(Project project) {
        final po = ProjectOperations.find(project).projectTools
        if (po.multiProject) {
            if (po.rootProject) {
                OpenTofuGlobalConfigForMultiProjectRootPlugin
            } else {
                OpenTofuGlobalConfigForMultiProjectSubProjectPlugin
            }
        } else {
            OpenTofuGlobalConfigForSingleProjectPlugin
        }
    }
}
