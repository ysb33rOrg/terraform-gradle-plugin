/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.opentofu.extensions

import groovy.transform.CompileStatic
import org.gradle.api.Project
import org.ysb33r.gradle.opentofu.OpenTofuToolchain
import org.ysb33r.gradle.opentofu.internal.OpenTofuModel
import org.ysb33r.gradle.opentofu.plugins.OpenTofuBasePlugin
import org.ysb33r.gradle.opentofu.plugins.OpenTofuWrapperPlugin
import org.ysb33r.grolifant5.api.core.ProjectOperations

import static org.ysb33r.gradle.opentofu.internal.Downloader.downloadSupported

/**
 * An extension to configure the opentofu wrapper.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class OpenTofuWrapperExtension extends OpenTofuToolchain {
    public static final String NAME = OpenTofuWrapperPlugin.WRAPPER_EXTENSION_NAME

    OpenTofuWrapperExtension(Project project) {
        super(NAME, project)

        final versions = ProjectOperations.find(project).fsOperations.loadPropertiesFromResource(
            OpenTofuBasePlugin.VERSION_RESOURCE_PATH,
            this.class.classLoader
        )

        if (downloadSupported) {
            executableByVersion(versions['opentofu-version'])
        } else {
            executableBySearchPath(OpenTofuModel.EXE_BASE_NAME)
        }
    }
}
