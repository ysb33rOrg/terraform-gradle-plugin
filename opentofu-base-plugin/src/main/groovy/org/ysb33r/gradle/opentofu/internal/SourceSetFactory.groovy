/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.opentofu.internal

import groovy.transform.CompileStatic
import org.gradle.api.NamedDomainObjectFactory
import org.gradle.api.Project
import org.gradle.api.model.ObjectFactory
import org.ysb33r.gradle.opentofu.OpenTofuSourceSet
import org.ysb33r.gradle.opentofu.extensions.OpenTofuExtension

/**
 * OpenTofu source set factory.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class SourceSetFactory implements NamedDomainObjectFactory<OpenTofuSourceSet> {
    SourceSetFactory(OpenTofuExtension parent, Project project) {
        this.objectFactory = project.objects
        this.parent = parent
    }

    @Override
    OpenTofuSourceSet create(String name) {
        objectFactory.newInstance(OpenTofuSourceSet, name, parent).tap {
            useToolchain(OpenTofuExtension.DEFAULT_TOOLCHAIN)
        }
    }

    private final OpenTofuExtension parent
    private final ObjectFactory objectFactory
}
