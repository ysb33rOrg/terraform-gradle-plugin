/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.opentofu.tasks

import groovy.transform.CompileStatic
import org.ysb33r.gradle.iac.base.internal.tf.DefaultUsesVariables
import org.ysb33r.gradle.iac.base.tf.config.multilevel.UsesVariables
import org.ysb33r.gradle.iac.base.tf.tasks.AbstractStateRmTask
import org.ysb33r.gradle.opentofu.OpenTofuExecSpec

import javax.inject.Inject

/**
 * Equivalent of {@code tofu state rm}.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class OpenTofuStateRm extends AbstractStateRmTask implements UsesVariables {
    @Inject
    OpenTofuStateRm(String workspaceName) {
        super(workspaceName, OpenTofuExecSpec)
        this.varsFile = new DefaultUsesVariables(execSpec, this)
    }

    @Delegate(includes = ['getVariablesFiles', 'setVariablesFiles'])
    private final UsesVariables varsFile
}
