/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.opentofu.tasks

import groovy.transform.CompileStatic
import org.ysb33r.gradle.iac.base.tf.tasks.AbstractIacWrapperTask
import org.ysb33r.gradle.opentofu.internal.OpenTofuModel

/**
 * openTofu script wrapper generator task.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class OpenTofuWrapper extends AbstractIacWrapperTask {

    OpenTofuWrapper() {
        super(
            OpenTofuModel.TOOL_NAME.uncapitalize(),
            TEMPLATE_RESOURCE_PATH,
            TEMPLATE_MAPPING
        )
    }

    private static final String TEMPLATE_RESOURCE_PATH = '/opentofu-wrapper'
    private static final Map<String, String> TEMPLATE_MAPPING = [
        'wrapper-template.sh' : 'tofuw',
        'wrapper-template.bat': 'tofuw.bat',
//        'wrapper-template.ps': 'terraformw.ps',
    ]
}
