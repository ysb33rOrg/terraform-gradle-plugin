/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.opentofu

import groovy.transform.CompileStatic
import org.gradle.api.NamedDomainObjectContainer
import org.gradle.api.Project
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.gradle.api.tasks.TaskContainer
import org.gradle.api.tasks.TaskProvider
import org.ysb33r.gradle.iac.base.tf.SourceSetBase
import org.ysb33r.gradle.iac.base.tf.tasks.AbstractOutputJsonTask
import org.ysb33r.gradle.opentofu.extensions.OpenTofuExtension
import org.ysb33r.gradle.opentofu.internal.OpenTofuModel

import javax.inject.Inject

import static org.ysb33r.gradle.opentofu.internal.OpenTofuTasks.APPLY
import static org.ysb33r.gradle.opentofu.internal.OpenTofuTasks.OUTPUT_JSON
import static org.ysb33r.gradle.opentofu.internal.OpenTofuTasks.PLAN
import static org.ysb33r.gradle.opentofu.plugins.OpenTofuBasePlugin.VERSION_RESOURCE_PATH

/**
 * Describes an OpenTofu source set.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class OpenTofuSourceSet extends SourceSetBase {
    public static final String DEFAULT_WORKSPACE = OpenTofuModel.DEFAULT_WORKSPACE

    @Inject
    OpenTofuSourceSet(String name, OpenTofuExtension parent, Project project) {
        super(
            name,
            DEFAULT_WORKSPACE,
            OpenTofuModel.PREFIX,
            project
        )

        final props = ccso.fsOperations().loadPropertiesFromResource(VERSION_RESOURCE_PATH, this.class.classLoader)
        this.executableLocation = project.objects.property(File)
        this.executableVersion = project.objects.property(String)
        this.pluginDir = project.objects.property(File)
        this.pluginDirTimeout = project.objects.property(Integer)
        this.parent = parent
        this.taskContainer = project.tasks

        addEnvironmentProvider(parent.globalConfigFile.map {
            [
                TF_CLI_CONFIG_FILE  : it.absolutePath,
                TF_LOG              : 'trace',
                TF_IN_AUTOMATION    : '1',
                TF_APPEND_USER_AGENT: "opentofu-gradle-plugin/${props['plugin-version']}".toString()
            ]
        }.zip(dataDir) { map, dir ->
            map + [TF_DATA_DIR: dir.absolutePath]
        }.zip(pluginDir) { map, dir ->
            map + [TF_PLUGIN_CACHE_DIR: dir.absolutePath]
        })

        include('**/*.tofu')

        useConfiguredPluginCache()
        addRelationshipTasks(DEFAULT_WORKSPACE)
    }

    /**
     * Cache directory for plugins / providers.
     *
     * @return Location of plugin directory.
     */
    @Override
    Provider<File> getPluginDir() {
        this.pluginDir
    }

    /**
     * Local timeout for the cache directory for plugins / providers.
     *
     * @return Timeout in milliseconds. If less than 1, then no lock will be performed
     */
    @Override
    Provider<Integer> getPluginDirTimeout() {
        this.pluginDirTimeout
    }

    /**
     * Use the plugin cache directory that is configured in the global configuration of the root project.
     *
     */
    @Override
    void useConfiguredPluginCache() {
        useThisCustomCacheDir(parent.pluginCacheDir, parent.pluginCacheDirTimeout)
    }

    /**
     * Looks up a backend by name and then set this sourceset to use that backend.
     *
     * @param name Name of backend.
     */
    @Override
    void useBackend(String name) {
        backendByInstance(this.parent.backends.named(name))
    }

    /**
     * Which toolchain to use.
     *
     * <p>
     * If not called, it will always used a default toolchain.
     * </p>
     *
     * @param name Name of toolchain.
     */
    @Override
    void useToolchain(String name) {
        final tc = parent.toolchains.named(name)
        this.executableLocation.set(tc.flatMap { it.executable })
        this.executableVersion.set(tc.flatMap { it.resolvedExecutableVersion() })
    }

    /**
     * Location of the executable that will used for this source set
     *
     * @return Provider to the executable
     */
    @Override
    Provider<File> getExecutableLocation() {
        this.executableLocation
    }

    /**
     * Version of the executable that will used for this source set
     *
     * @return Provider to the version.
     */
    @Override
    Provider<String> getExecutableVersion() {
        this.executableVersion
    }

    /**
     * Invoked when a workspace is added.
     *
     * @param workspaceName Name of workspace,
     */
    @Override
    protected void createWorkspaceTasks(String workspaceName) {
        objectFactory.newInstance(OpenTofuModel.WorkspaceGenerator)
            .create(this, workspaceName)
    }

    @Override
    protected TaskProvider<AbstractOutputJsonTask> getOutputTask(String workspaceName) {
        taskContainer.named(
            OpenTofuModel.taskName(name, OUTPUT_JSON.command, workspaceName),
            AbstractOutputJsonTask
        )
    }

    /**
     * Use this custom cache directory.
     *
     * @param dir Location of cache.
     * @param timeout Timeout in milliseconds.
     */
    @Override
    protected void useThisCustomCacheDir(Provider<File> dir, Provider<Integer> timeout) {
        this.pluginDir.set(dir)
        this.pluginDirTimeout.set(timeout)
    }

    /**
     * Names of tasks that are important in inter-source set relationships.
     *
     * @param ws Name of workspace.
     *
     * @return List of task names.
     */
    @Override
    protected List<String> getRelationshipTaskNames(String ws) {
        [PLAN, APPLY, OUTPUT_JSON].collect { OpenTofuModel.taskName(name, it.command, ws) }
    }

    /**
     * Get access to the container this source set belongs too.
     *
     * @return Container. Cannot be {@code null}.
     */
    @Override
    protected NamedDomainObjectContainer<? extends SourceSetBase> getSourceSetContainer() {
        parent.sourceSets
    }

    private final OpenTofuExtension parent
    private final TaskContainer taskContainer
    private final Property<File> executableLocation
    private final Property<String> executableVersion
    private final Property<File> pluginDir
    private final Property<Integer> pluginDirTimeout
}
