/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.opentofu.internal.plugins

import groovy.transform.CompileStatic
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.tasks.TaskProvider
import org.ysb33r.gradle.iac.base.internal.tf.TaskUtils
import org.ysb33r.gradle.iac.base.tf.tasks.GlobalConfigGenerator
import org.ysb33r.gradle.opentofu.extensions.OpenTofuGlobalConfigExtension
import org.ysb33r.gradle.opentofu.internal.OpenTofuModel

/**
 * Used internally for single projects.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class OpenTofuGlobalConfigForMultiProjectRootPlugin implements Plugin<Project> {
    public static final String GLOBAL_ATTRIBUTE_NAME = 'org.ysb33r.opentofu.global.metadata'
    public static final String GLOBAL_ATTRIBUTE_VALUE = 'metadata'
    public static final String GLOBAL_CONFIG_BASENAME = '$$$OpenTofuRc'
    public static final String GLOBAL_CONFIG_INCOMING = GLOBAL_CONFIG_BASENAME + 'Declaration$$$'
    public static final String GLOBAL_CONFIG_RESOLVE = GLOBAL_CONFIG_BASENAME + 'Resolver$$$'
    public static final String GLOBAL_CONFIG_OUTGOING = GLOBAL_CONFIG_BASENAME + 'Outgoing$$$'
    public static final String DETAILS_FILENAME = 'details.properties'
    public static final String DETAILS_SUBDIR = '.opentofurc'
    public static final String RC_GENERATOR = OpenTofuGlobalConfigForSingleProjectPlugin.RC_GENERATOR
    public static final String DETAILS_GENERATOR = "${RC_GENERATOR}Details"

    @Override
    void apply(Project project) {
        final rc = project.extensions.create(
            OpenTofuGlobalConfigExtension.NAME,
            OpenTofuGlobalConfigExtension,
            project
        )

        final rcgen = project.tasks.register(RC_GENERATOR, GlobalConfigGenerator) {
            it.outputFile = rc.configFile
            it.authTokens = rc.credentialTokens
            it.configTokens = rc.configTokens
        }

        registerOutgoingConfiguration(project, rcgen, rc)
    }

    void registerOutgoingConfiguration(
        Project project,
        TaskProvider<GlobalConfigGenerator> rcgen,
        OpenTofuGlobalConfigExtension rc
    ) {
        TaskUtils.registerGlobalConfigOutgoingConfiguration(
            DETAILS_GENERATOR,
            OpenTofuModel.TASK_GROUP,
            OpenTofuModel.TOOL_NAME,
            "${DETAILS_SUBDIR}/${DETAILS_FILENAME}",
            rcgen,
            rc.pluginCacheDir,
            rc.useLockWithTimeout,
            GLOBAL_CONFIG_OUTGOING,
            GLOBAL_ATTRIBUTE_NAME,
            GLOBAL_ATTRIBUTE_VALUE,
            project
        )
    }
}
