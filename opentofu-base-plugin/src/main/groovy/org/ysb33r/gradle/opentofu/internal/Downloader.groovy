/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.opentofu.internal

import groovy.transform.CompileStatic
import org.gradle.api.provider.Provider
import org.ysb33r.grolifant5.api.core.ConfigCacheSafeOperations
import org.ysb33r.grolifant5.api.core.OperatingSystem
import org.ysb33r.grolifant5.api.core.ProjectOperations
import org.ysb33r.grolifant5.api.core.downloader.AbstractDistributionInstaller

import static org.ysb33r.grolifant5.api.core.OperatingSystem.Arch.ARM64
import static org.ysb33r.grolifant5.api.core.OperatingSystem.Arch.X86
import static org.ysb33r.grolifant5.api.core.OperatingSystem.Arch.X86_64

/**
 * Downloads specific versions of {@code OpenTofu}.
 *
 * <p> Currently limited to Windows (x86, x86_64), MacOS, Linux (x86, x86_64), Solaris (x86_64) and
 * FreeBSD (x86, x86_64).
 *
 * <p> There are more binary packages are available from the OpenTofu site, but currently these are not being tested
 * not implemented. This includes:
 *
 * <ul>
 *    <li> linux_arm.zip
 *    <li> freebsd_arm.zip
 *    <li> openbsd_386, openbsd_amd64
 * </ul>
 * <p> (Patches welcome!)
 */
@CompileStatic
class Downloader extends AbstractDistributionInstaller {
    public static final OperatingSystem OS = OperatingSystem.current()
    public static final String TOOL_IDENTIFIER = 'opentofu'
    public static final String BASEURI = System.getProperty(
        'org.ysb33r.gradle.opentofu.releases.uri',
        'https://github.com/opentofu/opentofu/releases/download'
    )

    /** Creates a downloader
     *
     * @param version Version of {@code OpenTofu}.
     * @param projectOperations Project this is associated with.
     */
    Downloader(final ProjectOperations projectOperations) {
        super(
            TOOL_IDENTIFIER,
            "native-binaries/${TOOL_IDENTIFIER}",
            ConfigCacheSafeOperations.from(projectOperations)
        )
    }

    /**
     * Tells the system whether downloading can be supported.
     *
     * @return {@b true} for supported platforms,
     */
    static boolean isDownloadSupported() {
        (OS.windows || OS.linux || OS.macOsX || OS.freeBSD) &&
            (OS.arch == X86 || OS.arch == X86_64 || OS.arch == ARM64)
    }

    /** Provides an appropriate URI to download a specific version of OpenTofu.
     *
     * @param ver Version of OpenTofu to download
     * @return URI for a supported platform; {@code null} otherwise.
     */
    @Override
    URI uriFromVersion(final String ver) {
        final String arch = osArch()
        arch ? "${BASEURI}/v${ver}/tofu_${ver}_${arch}.zip".toURI() : null
    }

    /**
     * Returns the path to the {@code opentofu} executable.
     * Will force a download if not already downloaded.
     *
     * @return Location of {@code opentofu} or null if not a supported operating system.
     */
    Provider<File> getTofuExecutablePath(String version) {
        getDistributionRoot(version).map { new File(it, exeName) }
    }

    /**
     * Validates an {@code opentofu} download.
     *
     * @return {@code distDit}
     */
    @Override
    protected File verifyDistributionRoot(File distDir) {
        distDir
    }

    private String getExeName() {
        OpenTofuUtils.exeName
    }

    private String osArch() {
        OpenTofuUtils.osArch()
    }
}

