/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.opentofu.tasks

import groovy.transform.CompileStatic
import org.ysb33r.gradle.iac.base.tf.tasks.AbstractOutputJsonTask
import org.ysb33r.gradle.opentofu.OpenTofuExecSpec
import org.ysb33r.gradle.opentofu.internal.OpenTofuModel

import javax.inject.Inject

/**
 * Equivalent of {@code tofu output -json}.
 *
 * <p>Used for caching variables for reuse within the project</p>
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class OpenTofuOutputJson extends AbstractOutputJsonTask {
    @Inject
    OpenTofuOutputJson(String workspaceName) {
        super(workspaceName, OpenTofuModel.DEFAULT_WORKSPACE, OpenTofuExecSpec)
    }
}
