/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.opentofu.internal

import groovy.transform.CompileStatic
import org.ysb33r.grolifant5.api.core.OperatingSystem

import static org.ysb33r.grolifant5.api.core.OperatingSystem.Arch.ARM64
import static org.ysb33r.grolifant5.api.core.OperatingSystem.Arch.X86
import static org.ysb33r.grolifant5.api.core.OperatingSystem.Arch.X86_64

/**
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class OpenTofuUtils {
    public static final OperatingSystem OS = OperatingSystem.current()
    public static final OperatingSystem.Arch ARCH = OS.arch

    static String getExeName() {
        OS.windows ? 'tofu.exe' : 'tofu'
    }

    static String osArch() {
        String variant
        String osname
        if (OS.windows) {
            osname = 'windows'
            variant = (OS.arch == X86) ? '386' : 'amd64'
        } else if (OS.linux) {
            osname = 'linux'
            switch (ARCH) {
                case X86_64:
                    variant = VARIANT_64BIT
                    break
                case X86:
                    variant = VARIANT_32BIT
                    break
                case ARM64:
                    variant = VARIANT_ARM64
                    break
            }
        } else if (OS.macOsX) {
            osname = 'darwin'
            switch (ARCH) {
                case X86_64:
                    variant = VARIANT_64BIT
                    break
                case ARM64:
                    variant = VARIANT_ARM64
                    break
            }
        } else if (OS.solaris) {
            osname = 'solaris'
            variant = VARIANT_64BIT
        } else if (OS.freeBSD) {
            osname = 'freebsd'
            switch (ARCH) {
                case X86_64:
                    variant = VARIANT_64BIT
                    break
                case X86:
                    variant = VARIANT_32BIT
                    break
            }
        }
        variant ? "${osname}_${variant}" : null
    }

    private final static String VARIANT_32BIT = '386'
    private final static String VARIANT_64BIT = 'amd64'
    private final static String VARIANT_ARM64 = 'arm64'
}
