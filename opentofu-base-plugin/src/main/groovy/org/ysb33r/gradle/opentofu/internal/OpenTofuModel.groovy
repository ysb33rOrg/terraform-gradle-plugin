/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.opentofu.internal

import groovy.transform.CompileStatic
import org.gradle.api.Project
import org.ysb33r.gradle.iac.base.internal.tf.InitRelationship
import org.ysb33r.gradle.iac.base.internal.tf.TaskUtils
import org.ysb33r.gradle.iac.base.tf.config.multilevel.ExecutionOptions
import org.ysb33r.gradle.iac.base.tf.tasks.AbstractCustomFmtBaseTask
import org.ysb33r.gradle.iac.base.tf.tasks.AbstractTfBaseTask
import org.ysb33r.gradle.iac.base.tf.tasks.AbstractTfStandardTask
import org.ysb33r.gradle.iac.base.tf.tasks.RemoteStateConfigGenerator
import org.ysb33r.gradle.opentofu.OpenTofuSourceSet
import org.ysb33r.gradle.opentofu.extensions.OpenTofuExtension
import org.ysb33r.gradle.opentofu.tasks.OpenTofuApply
import org.ysb33r.gradle.opentofu.tasks.OpenTofuCleanupWorkspaces
import org.ysb33r.gradle.opentofu.tasks.OpenTofuCustomFmtApply
import org.ysb33r.gradle.opentofu.tasks.OpenTofuCustomFmtCheck
import org.ysb33r.gradle.opentofu.tasks.OpenTofuDestroy
import org.ysb33r.gradle.opentofu.tasks.OpenTofuDestroyPlan
import org.ysb33r.gradle.opentofu.tasks.OpenTofuFmtApply
import org.ysb33r.gradle.opentofu.tasks.OpenTofuFmtCheck
import org.ysb33r.gradle.opentofu.tasks.OpenTofuImport
import org.ysb33r.gradle.opentofu.tasks.OpenTofuInit
import org.ysb33r.gradle.opentofu.tasks.OpenTofuOutput
import org.ysb33r.gradle.opentofu.tasks.OpenTofuOutputJson
import org.ysb33r.gradle.opentofu.tasks.OpenTofuPlan
import org.ysb33r.gradle.opentofu.tasks.OpenTofuProvidersLock
import org.ysb33r.gradle.opentofu.tasks.OpenTofuProvidersSchema
import org.ysb33r.gradle.opentofu.tasks.OpenTofuProvidersShow
import org.ysb33r.gradle.opentofu.tasks.OpenTofuShowState
import org.ysb33r.gradle.opentofu.tasks.OpenTofuStateMv
import org.ysb33r.gradle.opentofu.tasks.OpenTofuStatePull
import org.ysb33r.gradle.opentofu.tasks.OpenTofuStatePush
import org.ysb33r.gradle.opentofu.tasks.OpenTofuStateRm
import org.ysb33r.gradle.opentofu.tasks.OpenTofuTaint
import org.ysb33r.gradle.opentofu.tasks.OpenTofuUntaint
import org.ysb33r.gradle.opentofu.tasks.OpenTofuValidate

import javax.inject.Inject

import static org.gradle.language.base.plugins.LifecycleBasePlugin.CHECK_TASK_NAME
import static org.ysb33r.gradle.iac.base.internal.tf.TaskUtils.configureStandardTaskOptions
import static org.ysb33r.gradle.iac.base.internal.tf.TaskUtils.registerConcurrencyProtectorService
import static org.ysb33r.gradle.iac.base.internal.tf.TaskUtils.registerVariablesGeneratorTask
import static org.ysb33r.gradle.iac.base.internal.tf.TaskUtils.variablesCacheTaskName
import static org.ysb33r.gradle.opentofu.internal.OpenTofuTasks.APPLY
import static org.ysb33r.gradle.opentofu.internal.OpenTofuTasks.DESTROY_PLAN
import static org.ysb33r.gradle.opentofu.internal.OpenTofuTasks.INIT
import static org.ysb33r.gradle.opentofu.internal.OpenTofuTasks.PLAN

/**
 * Utilities for building the tasks around a named OpenTofu source set.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@SuppressWarnings('MethodCount')
@CompileStatic
class OpenTofuModel {
    public static final String DEFAULT_SOURCESET_NAME = TaskUtils.DEFAULT_SOURCESET_NAME
    public static final String DEFAULT_WORKSPACE = TaskUtils.DEFAULT_WORKSPACE
    public static final String TASK_GROUP = 'OpenTofu'
    public static final String TOOL_NAME = PREFIX
    public static final String EXTENSION_NAME = OpenTofuExtension.NAME
    public static final String EXE_BASE_NAME = PREFIX
    public static final String PREFIX = 'tofu'
    public static final String PROVIDER_LOCK_FILENAME = '.terraform.lock.hcl'
    public static final String CUSTOM_FMT_CHECK = "${PREFIX}FmtCheckCustomDirectories"
    public static final String CUSTOM_FMT_APPLY = "${PREFIX}FmtApplyCustomDirectories"
    public static final String FORMAT_ALL = "${PREFIX}FormatAll"

    public static final Set<String> PLATFORMS = [
        'darwin_amd64', 'darwin_arm64',
        'windows_amd64', 'windows_386',
        'linux_386', 'linux_amd64', 'linux_arm', 'linux_arm64',
        'freebsd_386', 'freebsd_amd64', 'freebsd_arm'
    ].toSet().asImmutable()

    /**
     * Used by source set to create workspace tasks
     */
    static class WorkspaceGenerator {
        @Inject
        WorkspaceGenerator(Project p) {
            this.project = p
        }

        void create(OpenTofuSourceSet sourceSet, String wsName) {
            createTasksFromModel(sourceSet, wsName, project)
        }
        private final Project project
    }

    /**
     * Provides a task name
     *
     * @param sourceSetName Name of source set the task will be associated with.
     * @param commandType The OpenTofu command that this task will wrap.
     * @param workspaceName Name of workspace. if {@code null} will act as if workspace-agnostic
     * @return Name of task
     */
    static String taskName(String sourceSetName, String commandType, String workspaceName) {
        TaskUtils.taskName(
            sourceSetName,
            commandType,
            workspaceName,
            OpenTofuTasks.byCommand(commandType).workspaceAgnostic,
            PREFIX
        )
    }

    /**
     * The name of the backend configuration task.
     *
     * @param sourceSetName Name of source set.
     *
     * @return Name of task
     */
    static String backendTaskName(String sourceSetName) {
        TaskUtils.backendTaskName(sourceSetName, PREFIX)
    }

    /**
     * The name of the plugin dir configuration task.
     *
     * @return Name of task
     */
    static String pluginCacheDirTaskName() {
        TaskUtils.pluginCacheDirTaskName(PREFIX)
    }

    /**
     * Creates all of the required tasks for a source set
     * @param sourceSet OpenTofu sourceset
     * @param workspaceName Name of the workspace. Use {@link #DEFAULT_WORKSPACE} for the default workspace.
     * @param project Associated project.
     */
    static void createTasksFromModel(OpenTofuSourceSet sourceSet, String workspaceName, Project project) {
        if (workspaceName == DEFAULT_WORKSPACE) {
            registerBackendConfigurationTask(sourceSet, project)

            registerVariablesGeneratorTask(
                variablesCacheTaskName(sourceSet.name, PREFIX),
                TOOL_NAME,
                TASK_GROUP,
                sourceSet.variablesFile,
                sourceSet.escapedVariables,
                project
            )
        }
        OpenTofuTasks.ordered().each { taskDefinition ->
            if (workspaceName == DEFAULT_WORKSPACE ||
                workspaceName != DEFAULT_WORKSPACE && !taskDefinition.workspaceAgnostic
            ) {
                String newTaskName = taskName(sourceSet.name, taskDefinition.command, workspaceName)
                registerTask(sourceSet, project, workspaceName, taskDefinition, newTaskName)
                taskDefinition.configure(sourceSet, newTaskName, workspaceName, project)
                sourceSet.executionOptions.getCommandLineArgs(taskDefinition.executionConfigurations)
            }
        }
    }

    /**
     * Create format tasks that are not related to specific source sets.
     *
     * @param project Contextual project
     */
    static void createCustomFormatTasks(Project project) {
        final modelExt = project.extensions.getByType(OpenTofuExtension)
        project.tasks.register(FORMAT_ALL) {
            it.group = TASK_GROUP
            it.description = 'Formats all terraform source'
            it.dependsOn(project.tasks.withType(OpenTofuFmtApply))
            it.dependsOn(CUSTOM_FMT_APPLY)
        }

        final checkProvider = project.tasks.register(CUSTOM_FMT_CHECK, OpenTofuCustomFmtCheck)
        final applyProvider = project.tasks.register(CUSTOM_FMT_APPLY, OpenTofuCustomFmtApply)

        [checkProvider, applyProvider].each { t ->
            t.configure { AbstractCustomFmtBaseTask task ->
                task.tap {
                    executableLocation = modelExt.toolchains
                        .named(OpenTofuExtension.DEFAULT_TOOLCHAIN)
                        .flatMap { tc -> tc.executable }
                    recursive = modelExt.otherSources.recursive
                    globalConfigFile = modelExt.globalConfigFile
                    sourceDirectories = modelExt.otherSources.sourceDirectories
                    pluginDir = modelExt.pluginCacheDir
                    dependsOn(pluginCacheDirTaskName())
                }
            }
        }

        project.tasks.named(CHECK_TASK_NAME).configure {
            it.dependsOn(CUSTOM_FMT_CHECK)
        }
    }

    static void configureInit(TaskConfiguration cfg) {
        final backend = cfg.project.tasks.named(
            backendTaskName(cfg.sourceSet.name),
            RemoteStateConfigGenerator
        ).flatMap { it.backendConfigFile }
        cfg.project.tasks.named(cfg.taskName, OpenTofuInit) { OpenTofuInit it ->
            configureStandardTaskOptions(it, cfg.sourceSet)
            configureBaseTaskOptions(it, cfg.sourceSet, cfg.exeOpts)
            it.backendConfigFile = backend
            it.pluginDir = cfg.sourceSet.pluginDir
            it.dependsOn(backendTaskName(cfg.sourceSet.name), pluginCacheDirTaskName())

            // TODO: Could do this differently
            it.mustRunAfter(variablesCacheTaskName(cfg.sourceSet.name, PREFIX))
        }
    }

    static void configurePlan(TaskConfiguration cfg) {
        cfg.project.tasks.named(cfg.taskName, OpenTofuPlan) { OpenTofuPlan it ->
            configureStandardTaskOptions(it, cfg.sourceSet)
            configureBaseTaskOptions(it, cfg.sourceSet, cfg.exeOpts)
            it.sourceSetName = cfg.sourceSet.name
            it.variablesFiles = cfg.sourceSet.allVariablesFiles
            it.dependsOn(variablesCacheTaskName(cfg.sourceSet.name, PREFIX))
        }
    }

    static void configureApply(TaskConfiguration cfg) {
        cfg.project.tasks.named(cfg.taskName, OpenTofuApply) { OpenTofuApply it ->
            configureStandardTaskOptions(it, cfg.sourceSet)
            configureBaseTaskOptions(it, cfg.sourceSet, cfg.exeOpts)
            it.dependsOn(
                taskName(cfg.sourceSet.name, PLAN.command, cfg.workspaceName)
            )
        }
    }

    static void configureDestroyPlan(TaskConfiguration cfg) {
        cfg.project.tasks.named(cfg.taskName, OpenTofuDestroyPlan) { OpenTofuDestroyPlan it ->
            configureStandardTaskOptions(it, cfg.sourceSet)
            configureBaseTaskOptions(it, cfg.sourceSet, cfg.exeOpts)
            it.sourceSetName = cfg.sourceSet.name
            it.variablesFiles = cfg.sourceSet.allVariablesFiles
            it.dependsOn(variablesCacheTaskName(cfg.sourceSet.name, PREFIX))
        }
    }

    static void configureDestroy(TaskConfiguration cfg) {
        cfg.project.tasks.named(cfg.taskName, OpenTofuDestroy) { OpenTofuDestroy it ->
            configureStandardTaskOptions(it, cfg.sourceSet)
            configureBaseTaskOptions(it, cfg.sourceSet, cfg.exeOpts)
            it.dependsOn(
                taskName(cfg.sourceSet.name, DESTROY_PLAN.command, cfg.workspaceName)
            )
        }
    }

    static void configureImport(TaskConfiguration cfg) {
        cfg.project.tasks.named(cfg.taskName, OpenTofuImport) { OpenTofuImport it ->
            configureStandardTaskOptions(it, cfg.sourceSet)
            configureBaseTaskOptions(it, cfg.sourceSet, cfg.exeOpts)
            it.variablesFiles = cfg.sourceSet.allVariablesFiles
            it.dependsOn(variablesCacheTaskName(cfg.sourceSet.name, PREFIX))
            it.variablesFiles = cfg.sourceSet.allVariablesFiles
            it.dependsOn(variablesCacheTaskName(cfg.sourceSet.name, PREFIX))
        }
    }

    static void configureShowState(TaskConfiguration cfg) {
        cfg.project.tasks.named(cfg.taskName, OpenTofuShowState) { OpenTofuShowState it ->
            configureStandardTaskOptions(it, cfg.sourceSet)
            configureBaseTaskOptions(it, cfg.sourceSet, cfg.exeOpts)
            it.variablesFiles = cfg.sourceSet.allVariablesFiles
            it.sourceSetName = cfg.sourceSet.name
            it.dependsOn(variablesCacheTaskName(cfg.sourceSet.name, PREFIX))
        }
    }

    static void configureOutput(TaskConfiguration cfg) {
        cfg.project.tasks.named(cfg.taskName, OpenTofuOutput) { OpenTofuOutput it ->
            configureStandardTaskOptions(it, cfg.sourceSet)
            configureBaseTaskOptions(it, cfg.sourceSet, cfg.exeOpts)
            it.variablesFiles = cfg.sourceSet.allVariablesFiles
            it.sourceSetName = cfg.sourceSet.name
            it.dependsOn(variablesCacheTaskName(cfg.sourceSet.name, PREFIX))
        }
    }

    static void configureCacheOutputVariables(TaskConfiguration cfg) {
        cfg.project.tasks.named(cfg.taskName, OpenTofuOutputJson) { OpenTofuOutputJson it ->
            configureStandardTaskOptions(it, cfg.sourceSet)
            configureBaseTaskOptions(it, cfg.sourceSet, cfg.exeOpts)
            it.sourceSetName = cfg.sourceSet.name
            it.dependsOn(
                taskName(cfg.sourceSet.name, APPLY.command, cfg.workspaceName)
            )
        }
    }

    static void configureValidate(TaskConfiguration cfg) {
        cfg.project.tasks.named(cfg.taskName, OpenTofuValidate) { OpenTofuValidate it ->
            configureStandardTaskOptions(it, cfg.sourceSet)
            configureBaseTaskOptions(it, cfg.sourceSet, cfg.exeOpts)
            it.variablesFiles = cfg.sourceSet.allVariablesFiles
            it.dependsOn(variablesCacheTaskName(cfg.sourceSet.name, PREFIX))
        }
    }

    static void configureCleanupWorkspaces(TaskConfiguration cfg) {
        cfg.project.tasks.named(cfg.taskName, OpenTofuCleanupWorkspaces) { OpenTofuCleanupWorkspaces it ->
            configureStandardTaskOptions(it, cfg.sourceSet)
            configureBaseTaskOptions(it, cfg.sourceSet, cfg.exeOpts)
            it.setWorkspaces(cfg.sourceSet.workspaceNames)
        }
    }

    static void configureProvidersLock(TaskConfiguration cfg) {
        cfg.project.tasks.named(cfg.taskName, OpenTofuProvidersLock) { OpenTofuProvidersLock it ->
            configureStandardTaskOptions(it, cfg.sourceSet)
            configureBaseTaskOptions(it, cfg.sourceSet, cfg.exeOpts)
            it.requiredPlatforms = cfg.ext.platforms
            it.variablesFiles = cfg.sourceSet.allVariablesFiles
            it.dependsOn(variablesCacheTaskName(cfg.sourceSet.name, PREFIX))
        }
    }

    static void configureProvidersSchema(TaskConfiguration cfg) {
        cfg.project.tasks.named(cfg.taskName, OpenTofuProvidersSchema) { OpenTofuProvidersSchema it ->
            configureStandardTaskOptions(it, cfg.sourceSet)
            configureBaseTaskOptions(it, cfg.sourceSet, cfg.exeOpts)
            it.requiredPlatforms = cfg.ext.platforms
            it.sourceSetName = cfg.sourceSet.name
            it.variablesFiles = cfg.sourceSet.allVariablesFiles
            it.dependsOn(variablesCacheTaskName(cfg.sourceSet.name, PREFIX))
        }
    }

    static void configureProvidersShow(TaskConfiguration cfg) {
        cfg.project.tasks.named(cfg.taskName, OpenTofuProvidersShow) { OpenTofuProvidersShow it ->
            configureStandardTaskOptions(it, cfg.sourceSet)
            configureBaseTaskOptions(it, cfg.sourceSet, cfg.exeOpts)
            it.requiredPlatforms = cfg.ext.platforms
            it.variablesFiles = cfg.sourceSet.allVariablesFiles
            it.dependsOn(variablesCacheTaskName(cfg.sourceSet.name, PREFIX))
        }
    }

    static void configureStatePull(TaskConfiguration cfg) {
        cfg.project.tasks.named(cfg.taskName, OpenTofuStatePull) { OpenTofuStatePull it ->
            configureStandardTaskOptions(it, cfg.sourceSet)
            configureBaseTaskOptions(it, cfg.sourceSet, cfg.exeOpts)
            it.variablesFiles = cfg.sourceSet.allVariablesFiles
            it.dependsOn(variablesCacheTaskName(cfg.sourceSet.name, PREFIX))
        }
    }

    static void configureStatePush(TaskConfiguration cfg) {
        cfg.project.tasks.named(cfg.taskName, OpenTofuStatePush) { OpenTofuStatePush it ->
            configureStandardTaskOptions(it, cfg.sourceSet)
            configureBaseTaskOptions(it, cfg.sourceSet, cfg.exeOpts)
            it.variablesFiles = cfg.sourceSet.allVariablesFiles
            it.dependsOn(variablesCacheTaskName(cfg.sourceSet.name, PREFIX))
        }
    }

    static void configureStateMv(TaskConfiguration cfg) {
        cfg.project.tasks.named(cfg.taskName, OpenTofuStateMv) { OpenTofuStateMv it ->
            configureStandardTaskOptions(it, cfg.sourceSet)
            configureBaseTaskOptions(it, cfg.sourceSet, cfg.exeOpts)
            it.variablesFiles = cfg.sourceSet.allVariablesFiles
            it.dependsOn(variablesCacheTaskName(cfg.sourceSet.name, PREFIX))
        }
    }

    static void configureStateRm(TaskConfiguration cfg) {
        cfg.project.tasks.named(cfg.taskName, OpenTofuStateRm) { OpenTofuStateRm it ->
            configureStandardTaskOptions(it, cfg.sourceSet)
            configureBaseTaskOptions(it, cfg.sourceSet, cfg.exeOpts)
            it.variablesFiles = cfg.sourceSet.allVariablesFiles
            it.dependsOn(variablesCacheTaskName(cfg.sourceSet.name, PREFIX))
        }
    }

    static void configureTaint(TaskConfiguration cfg) {
        cfg.project.tasks.named(cfg.taskName, OpenTofuTaint) { OpenTofuTaint it ->
            configureStandardTaskOptions(it, cfg.sourceSet)
            configureBaseTaskOptions(it, cfg.sourceSet, cfg.exeOpts)
            it.sourceSetName = cfg.sourceSet.name
            it.variablesFiles = cfg.sourceSet.allVariablesFiles
            it.dependsOn(variablesCacheTaskName(cfg.sourceSet.name, PREFIX))
        }
    }

    static void configureUntaint(TaskConfiguration cfg) {
        cfg.project.tasks.named(cfg.taskName, OpenTofuUntaint) { OpenTofuUntaint it ->
            configureStandardTaskOptions(it, cfg.sourceSet)
            configureBaseTaskOptions(it, cfg.sourceSet, cfg.exeOpts)
            it.sourceSetName = cfg.sourceSet.name
            it.variablesFiles = cfg.sourceSet.allVariablesFiles
            it.dependsOn(variablesCacheTaskName(cfg.sourceSet.name, PREFIX))
        }
    }

    static void configureFormatApply(TaskConfiguration cfg) {
        cfg.project.tasks.named(cfg.taskName, OpenTofuFmtApply) { OpenTofuFmtApply it ->
            configureStandardTaskOptions(it, cfg.sourceSet)
            configureBaseTaskOptions(it, cfg.sourceSet, cfg.exeOpts)
        }
    }

    static void configureFormatCheck(TaskConfiguration cfg) {
        cfg.project.tasks.named(cfg.taskName, OpenTofuFmtCheck) { OpenTofuFmtCheck it ->
            configureStandardTaskOptions(it, cfg.sourceSet)
            configureBaseTaskOptions(it, cfg.sourceSet, cfg.exeOpts)
        }

        cfg.project.tasks.named(CHECK_TASK_NAME).configure {
            it.dependsOn(cfg.taskName)
        }
    }

    private static void configureStandardTaskOptions(AbstractTfStandardTask task, OpenTofuSourceSet sourceSet) {
        configureStandardTaskOptions(
            task,
            sourceSet,
            registerConcurrencyProtectorService("${EXTENSION_NAME}-concurrency-limiter-${sourceSet.name}", task.project)
        )
    }

    private static void configureBaseTaskOptions(
        AbstractTfBaseTask task,
        OpenTofuSourceSet sourceSet,
        Iterable<Class<? extends ExecutionOptions>> exeOpts
    ) {
        TaskUtils.configureBaseTaskOptions(task, sourceSet, exeOpts)
    }

    private static void registerTask(
        OpenTofuSourceSet sourceSet,
        Project project,
        String workspaceName,
        OpenTofuTasks taskDetails,
        String newTaskName
    ) {
        if (taskDetails.dependsOnProvider) {
            project.tasks.register(
                newTaskName,
                taskDetails.type,
                taskName(sourceSet.name, taskDetails.dependsOnProvider.command, workspaceName),
                workspaceName
            )
        } else if (taskDetails.workspaceAgnostic) {
            project.tasks.register(
                newTaskName,
                taskDetails.type
            )
        } else {
            project.tasks.register(
                newTaskName,
                taskDetails.type,
                workspaceName
            )
        }

        final initName = taskName(sourceSet.name, INIT.command, DEFAULT_WORKSPACE)
        final dependsOnInitFiles = taskDetails.command == INIT.command ?
            null :
            project.tasks.named(initName).map { it.outputs.files }
        final mra = taskDetails.mustRunAfter.collect { tdName ->
            final td = OpenTofuTasks.valueOf(tdName)
            taskName(sourceSet.name, td.command, td.workspaceAgnostic ? DEFAULT_WORKSPACE : workspaceName)
        }
        project.tasks.named(newTaskName) {
            it.group = TASK_GROUP
            it.description = "${taskDetails.description} for '${sourceSet.name}'"
            if (taskDetails.command != INIT.command) {
                switch (taskDetails.initRelationship) {
                    case InitRelationship.MUST_RUN_AFTER:
                        it.mustRunAfter(taskName(sourceSet.name, INIT.command, DEFAULT_WORKSPACE))
                        break
                    case InitRelationship.DEPENDS_ON:
                        it.inputs.files(dependsOnInitFiles)
                        break
                }
            }

            if (mra) {
                it.mustRunAfter(mra)
            }
        }
    }

    private static void registerBackendConfigurationTask(
        OpenTofuSourceSet sourceSet,
        Project project
    ) {
        TaskUtils.registerBackendConfigurationTask(
            sourceSet.name,
            backendTaskName(sourceSet.name),
            TASK_GROUP,
            sourceSet.backendConfigurationDir,
            sourceSet.backendTokenProvider,
            project
        )

        project.tasks.named(backendTaskName(sourceSet.name)) { it.dependsOn(pluginCacheDirTaskName()) }
    }
}
