/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.opentofu.internal

import groovy.transform.CompileStatic
import org.gradle.api.Project
import org.ysb33r.gradle.iac.base.internal.tf.InitRelationship
import org.ysb33r.gradle.iac.base.tf.config.multilevel.CodeFormatting
import org.ysb33r.gradle.iac.base.tf.config.multilevel.ExecutionConfiguration
import org.ysb33r.gradle.iac.base.tf.config.multilevel.Lock
import org.ysb33r.gradle.iac.base.tf.config.multilevel.StateOptionsConcurrency
import org.ysb33r.gradle.iac.base.tf.config.multilevel.StateOptionsFull
import org.ysb33r.gradle.opentofu.OpenTofuSourceSet
import org.ysb33r.gradle.opentofu.extensions.OpenTofuExtension
import org.ysb33r.gradle.opentofu.tasks.OpenTofuApply
import org.ysb33r.gradle.opentofu.tasks.OpenTofuCleanupWorkspaces
import org.ysb33r.gradle.opentofu.tasks.OpenTofuDestroy
import org.ysb33r.gradle.opentofu.tasks.OpenTofuDestroyPlan
import org.ysb33r.gradle.opentofu.tasks.OpenTofuFmtApply
import org.ysb33r.gradle.opentofu.tasks.OpenTofuFmtCheck
import org.ysb33r.gradle.opentofu.tasks.OpenTofuImport
import org.ysb33r.gradle.opentofu.tasks.OpenTofuInit
import org.ysb33r.gradle.opentofu.tasks.OpenTofuOutput
import org.ysb33r.gradle.opentofu.tasks.OpenTofuOutputJson
import org.ysb33r.gradle.opentofu.tasks.OpenTofuPlan
import org.ysb33r.gradle.opentofu.tasks.OpenTofuProvidersLock
import org.ysb33r.gradle.opentofu.tasks.OpenTofuProvidersSchema
import org.ysb33r.gradle.opentofu.tasks.OpenTofuProvidersShow
import org.ysb33r.gradle.opentofu.tasks.OpenTofuShowState
import org.ysb33r.gradle.opentofu.tasks.OpenTofuStateMv
import org.ysb33r.gradle.opentofu.tasks.OpenTofuStatePull
import org.ysb33r.gradle.opentofu.tasks.OpenTofuStatePush
import org.ysb33r.gradle.opentofu.tasks.OpenTofuStateRm
import org.ysb33r.gradle.opentofu.tasks.OpenTofuTaint
import org.ysb33r.gradle.opentofu.tasks.OpenTofuUntaint
import org.ysb33r.gradle.opentofu.tasks.OpenTofuValidate

import static org.ysb33r.gradle.iac.base.internal.tf.InitRelationship.DEPENDS_ON
import static org.ysb33r.gradle.iac.base.internal.tf.InitRelationship.MUST_RUN_AFTER
import static org.ysb33r.gradle.iac.base.internal.tf.InitRelationship.NONE

/**
 * Maps OpenTofu tasks to conventions.
 *
 * @author Schalk W. Cronjé
 */
@CompileStatic
@SuppressWarnings(['LineLength', 'DuplicateListLiteral'])
enum OpenTofuTasks {
    INIT(
        1, 'init', OpenTofuInit, 'Initialises OpenTofu',
        [],
        NONE, true
    ),
    IMPORT(
        2, 'import', OpenTofuImport, 'Imports a resource',
        [Lock, StateOptionsConcurrency],
        MUST_RUN_AFTER, false, ['STATE_RM', 'STATE_PUSH']
    ),
    SHOW(
        3, 'showState', OpenTofuShowState, 'Generates a report on the current state',
        [],
        MUST_RUN_AFTER, false, ['APPLY', 'DESTROY', 'STATE_RM', 'IMPORT', 'STATE_MV', 'STATE_PUSH', 'TAINT', 'UNTAINT']
    ),
    OUTPUT(
        4, 'output', OpenTofuOutput, 'Generates a file of output variables',
        [],
        MUST_RUN_AFTER, false, ['APPLY', 'DESTROY']
    ),
    PLAN(
        10, 'plan', OpenTofuPlan, 'Generates OpenTofu execution plan',
        [Lock, StateOptionsFull],
        DEPENDS_ON, false, ['STATE_MV', 'STATE_RM', 'IMPORT', 'TAINT', 'UNTAINT', 'CLEANUP_WORKSPACES', 'STATE_PUSH']
    ),
    APPLY(
        11, 'apply', OpenTofuApply, 'Builds or changes infrastructure',
        [Lock, StateOptionsFull],
        DEPENDS_ON, false, ['STATE_MV', 'STATE_RM', 'IMPORT', 'TAINT', 'UNTAINT', 'CLEANUP_WORKSPACES', 'STATE_PUSH'], PLAN
    ),
    OUTPUT_JSON(
        12, 'cacheOutputVariables', OpenTofuOutputJson, 'Caches the output variables',
        [],
        MUST_RUN_AFTER, false, ['DESTROY']
    ),
    DESTROY_PLAN(
        14, 'destroyPlan', OpenTofuDestroyPlan, 'Generates OpenTofu destruction plan',
        [Lock, StateOptionsFull],
        DEPENDS_ON, false,
    ),
    DESTROY(
        15, 'destroy', OpenTofuDestroy, 'Destroys infrastructure',
        [Lock, StateOptionsFull],
        DEPENDS_ON, false, ['STATE_MV', 'STATE_RM', 'IMPORT', 'TAINT', 'UNTAINT', 'STATE_PUSH'], DESTROY_PLAN
    ),
    VALIDATE(
        20, 'validate', OpenTofuValidate, 'Validates the OpenTofu configuration',
        [],
        NONE
    ),
    STATE_MV(
        30, 'stateMv', OpenTofuStateMv, 'Moves a resource from one area to another',
        [Lock],
        MUST_RUN_AFTER, false, ['STATE_PUSH', 'IMPORT', 'STATE_RM']
    ),
    STATE_PUSH(
        31, 'statePush', OpenTofuStatePush, 'Pushes local state file to remote',
        [Lock]
    ),
    STATE_PULL(
        32, 'statePull', OpenTofuStatePull, 'Pulls remote state local to local file',
        []
    ),
    STATE_RM(
        33, 'stateRm', OpenTofuStateRm, 'Removes a resource from state',
        [Lock],
        MUST_RUN_AFTER, false, ['STATE_PUSH']
    ),
    TAINT(
        34, 'taint', OpenTofuTaint, 'Taint the status of a resource',
        [Lock],
        MUST_RUN_AFTER, false, ['STATE_PUSH']
    ),
    UNTAINT(
        35, 'untaint', OpenTofuUntaint, 'Remove tainted status from resource',
        [Lock],
        MUST_RUN_AFTER, false, ['STATE_PUSH']
    ),
    FMT_CHECK(
        50,
        'formatCheck',
        OpenTofuFmtCheck,
        'Checks whether files are correctly formatted',
        [CodeFormatting],
        NONE, true
    ),
    FMT_APPLY(
        51, 'formatApply', OpenTofuFmtApply, 'Formats source files in source set',
        [CodeFormatting],
        NONE, true
    ),
    CLEANUP_WORKSPACES(
        60, 'cleanupWorkspaces', OpenTofuCleanupWorkspaces, 'Deletes any dangling workspaces',
        [],
        NONE, true
    ),
    PROVIDER(
        70, 'providersShow', OpenTofuProvidersShow, 'Show provider information',
        [],
        MUST_RUN_AFTER, true
    ),
    PROVIDER_LOCK(
        71, 'providersLock', OpenTofuProvidersLock, 'Lock provider package versions',
        [],
        MUST_RUN_AFTER, true
    ),
    PROVIDER_SCHEMA(
        72, 'providersSchema', OpenTofuProvidersSchema, 'Write detailed provider schemas to a file',
        [],
        MUST_RUN_AFTER, true
    )

    static List<OpenTofuTasks> ordered() {
        OpenTofuTasks.values().sort { a, b -> a.order <=> b.order } as List
    }

    /**
     * Find instance by command name
     *
     * @param cmd Command
     * @return Task metadata
     * @throw {@link IllegalArgumentException} is no match
     */
    static OpenTofuTasks byCommand(String cmd) {
        def task = values().find { cmd == it.command }
        if (!task) {
            throw new IllegalArgumentException("${cmd} is not a valid command alias for a Terraform task")
        }
        task
    }

    final int order
    final String command
    final Class type
    final String description
    final List<Class<? extends ExecutionConfiguration>> executionConfigurations
    final InitRelationship initRelationship
    final boolean workspaceAgnostic
    final OpenTofuTasks dependsOnProvider
    final List<String> mustRunAfter

    void configure(OpenTofuSourceSet sourceSet, String taskName, String workspaceName, Project project) {
        OpenTofuModel.invokeMethod(
            "configure${command.capitalize()}",
            [new TaskConfiguration(
                sourceSet,
                taskName,
                workspaceName,
                project.extensions.getByType(OpenTofuExtension),
                project,
                executionConfigurations
            )] as Object[]
        )
    }

    @SuppressWarnings('ParameterCount')
    private OpenTofuTasks(
        int order,
        String name,
        Class type,
        String description,
        Iterable<Class<? extends ExecutionConfiguration>> configurations,
        InitRelationship init = InitRelationship.MUST_RUN_AFTER,
        boolean workspaceAgnostic = false,
        Iterable<String> mustRunAfter = [],
        OpenTofuTasks dependsOn = null
    ) {
        this.order = order
        this.command = name
        this.type = type
        this.description = description
        this.workspaceAgnostic = workspaceAgnostic
        this.dependsOnProvider = dependsOn
        this.executionConfigurations = configurations.toList().asImmutable()
        this.initRelationship = init
        this.mustRunAfter = mustRunAfter.toList().asImmutable()
    }
}