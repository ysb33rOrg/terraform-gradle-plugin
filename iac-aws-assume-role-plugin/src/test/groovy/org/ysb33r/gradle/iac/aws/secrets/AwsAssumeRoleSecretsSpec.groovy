/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.iac.aws.secrets

import org.gradle.api.Project
import org.gradle.testfixtures.ProjectBuilder
import org.ysb33r.gradle.iac.aws.plugins.AwsAssumeRolePlugin
import org.ysb33r.gradle.iac.base.secrets.AwsSecrets
import spock.lang.Specification

class AwsAssumeRoleSecretsSpec extends Specification {

    Project project

    void setup() {
        project = ProjectBuilder.builder().build()
        project.pluginManager.apply(AwsAssumeRolePlugin)
    }

    void 'Can set and retrieve assume role authentication'() {
        setup:
        final roleSecrets = new AwsAssumeRoleSecrets('foo', project).tap {
            useAccessKeyId('123456')
            useSecretAccessKey('123456')
            useSessionName('testing')
            useRoleArn('fake-arn')
            useRegion('us-west-1')
        }

        when:
        final secrets = roleSecrets.secretVariables.get()

        then:
        secrets.keySet().containsAll([
            AwsSecrets.AWS_TOKEN,
            AwsSecrets.AWS_SECRET,
            AwsSecrets.AWS_KEY
        ])
    }
}