/*
 * Copyright 2017 - 2025 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.iac.aws.secrets

import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import org.gradle.api.Project
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.ysb33r.gradle.iac.base.secrets.AwsSecrets
import org.ysb33r.gradle.iac.base.secrets.IacSecrets
import org.ysb33r.grolifant5.api.core.SimpleSecureString
import org.ysb33r.grolifant5.api.core.plugins.GrolifantServicePlugin
import org.ysb33r.grolifant5.api.core.services.GrolifantSecurePropertyService
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials
import software.amazon.awssdk.auth.credentials.AwsCredentials
import software.amazon.awssdk.auth.credentials.AwsCredentialsProvider
import software.amazon.awssdk.regions.Region
import software.amazon.awssdk.services.sts.StsClient
import software.amazon.awssdk.services.sts.model.AssumeRoleRequest
import software.amazon.awssdk.services.sts.model.AssumeRoleResponse

import javax.inject.Inject

/**
 * Secrets class to handle AWS assume role authentication.
 *
 * <p>
 *     It will add three secrets:
 *
 *     <ul>
 *         <li>AWS_ACCESS_KEY_ID</li>
 *         <li>AWS_SECRET_ACCESS_KEY</li>
 *         <li>AWS_SESSION_TOKEN</li>
 *     </ul>
 *
 *     These session details are only calculated then the provider from {@link #getSecretVariables} is resolved.
 * </p>
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
@Slf4j
class AwsAssumeRoleSecrets extends IacSecrets {

    /**
     * If this system property is defined, then fake session tokens will be returned.
     * This is primarily used for testing
     */
    public static final boolean FAKE_SESSION_TOKENS =
        System.getProperty('org.ysb33r.gradle.iac.aws.integration.tests.fake.session.tokens')

    Integer durationSeconds = 15 * 60

    @Inject
    AwsAssumeRoleSecrets(String name, Project tempProjectRef) {
        super(name, tempProjectRef)

        this.accessKey = tempProjectRef.objects.property(SimpleSecureString)
        this.secretKey = tempProjectRef.objects.property(SimpleSecureString)
        this.roleArn = tempProjectRef.objects.property(String)
        this.region = tempProjectRef.objects.property(String).convention('us-east-1')
        this.sessionName = tempProjectRef.objects.property(String)
            .convention("gradle-aws-iac-plugin-${UUID.randomUUID()}".toString())

        this.securePropertyServiceProvider = (Provider<GrolifantSecurePropertyService>) tempProjectRef.gradle
            .sharedServices
            .registrations
            .getByName(GrolifantServicePlugin.SECURE_STRING_SERVICE)
            .service

        final ourSecretVariables = tempProjectRef.provider { -> loadSessionKeys() }
        this.mergedSecretVariables = super.secretVariables.zip(ourSecretVariables) { x, y ->
            x + y
        }

        this.credentialsChain = new CredentialsProvider(this)
    }

    /**
     * Set the access key to use.
     *
     * @param key Anything that will resolve to a string and can be resolved at configuration time.
     */
    void useAccessKeyId(Object key) {
        accessKey.set(securePropertyServiceProvider.get().pack(ccso.stringTools().stringize(key)))
    }

    /**
     * Set the secret key to use.
     *
     * @param key Anything that will resolve to a string and can be resolved at configuration time.
     */
    void useSecretAccessKey(Object key) {
        secretKey.set(securePropertyServiceProvider.get().pack(ccso.stringTools().stringize(key)))
    }

    /**
     * Set the ARN of the role to assume.
     *
     * @param arn Role ARN. Anything that can be lazily-resolved to a string.
     */
    void useRoleArn(Object arn) {
        this.roleArn.set(ccso.stringTools().provideString(arn))
    }

    /**
     * Set the name of the session.
     *
     * <p>If not set, a random name will be chosen.</p>
     *
     * @param name Session name. Anything that can be lazily-resolved to a string.
     */
    void useSessionName(Object name) {
        this.sessionName.set(ccso.stringTools().provideString(name))
    }

    /**
     * Set the region to use for authentication.
     *
     * <p>If not set, {@code us-east-1} will be assumed.</p>
     *
     * @param region Region to authenticate in. Anything that can be lazily-resolved to a string.
     */
    void useRegion(Object region) {
        this.region.set(ccso.stringTools().provideString(region))
    }

    /**
     * Returns a provider to a set of secret variables.
     *
     * @return A provider to a map of secred property strings.
     */
    @Override
    Provider<Map<String, SimpleSecureString>> getSecretVariables() {
        this.mergedSecretVariables
    }

    private static class CredentialsProvider implements AwsCredentialsProvider {
        CredentialsProvider(AwsAssumeRoleSecrets owner) {
            this.owner = owner
        }

        @Override
        AwsCredentials resolveCredentials() {
            AwsBasicCredentials.create(
                new String(owner.securePropertyServiceProvider.get().unpack(owner.accessKey.get())),
                new String(owner.securePropertyServiceProvider.get().unpack(owner.secretKey.get()))
            )
        }

        private final AwsAssumeRoleSecrets owner
    }

    private Map<String, SimpleSecureString> loadSessionKeys() {
        final spsp = securePropertyServiceProvider.get()
        if (FAKE_SESSION_TOKENS) {
            final String uuid = UUID.randomUUID()
            [
                (AwsSecrets.AWS_KEY)   : spsp.pack("FAKE_ACCESS_${uuid}"),
                (AwsSecrets.AWS_SECRET): spsp.pack("FAKE_SECRET_${uuid}"),
                (AwsSecrets.AWS_TOKEN) : spsp.pack("FAKE_${roleArn.get()}_${region.get()}")
            ]
        } else {
            StsClient.builder()
                .region(Region.of(region.get()))
                .credentialsProvider(credentialsChain)
                .build()
                .withCloseable { StsClient client ->
                    AssumeRoleRequest roleRequest = (AssumeRoleRequest) AssumeRoleRequest.builder()
                        .roleArn(roleArn.get())
                        .roleSessionName(sessionName.get())
                        .durationSeconds(durationSeconds)
                        .build()

                    AssumeRoleResponse roleResponse = client.assumeRole(roleRequest)
                    final myCreds = roleResponse.credentials()
                    if (log.debugEnabled) {
                        final exTime = myCreds.expiration()
                        final tokenInfo = myCreds.sessionToken()
                        log.debug("AWS Session token ${tokenInfo} expires on ${exTime}")
                    }

                    [
                        (AwsSecrets.AWS_KEY)   : spsp.pack(myCreds.accessKeyId()),
                        (AwsSecrets.AWS_SECRET): spsp.pack(myCreds.secretAccessKey()),
                        (AwsSecrets.AWS_TOKEN) : spsp.pack(myCreds.sessionToken())
                    ]
                }
        }
    }

    private final Property<String> roleArn
    private final Property<String> region
    private final Property<String> sessionName
    private final Property<SimpleSecureString> accessKey
    private final Property<SimpleSecureString> secretKey
    private final AwsCredentialsProvider credentialsChain
    private final Provider<Map<String, SimpleSecureString>> mergedSecretVariables
    private final Provider<GrolifantSecurePropertyService> securePropertyServiceProvider
}
