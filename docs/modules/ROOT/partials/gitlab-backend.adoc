//tag::http-backend-warning[]
WARNING: The Gitlab backend *DOES NOT* support workspaces.
  If you need something like workspaces, but want to use Gitlab tfstate storage, consider using multiple source sets and use local modules to share common IAC.
  Then configure a Gitlab backend for each source set and change the address.
//end::http-backend-warning[]

[source,groovy]
----
import org.ysb33r.gradle.IGNORE.backends.GitlabBackend

IGNORE {
// tag::backend-src-block[]
  backends {
    gitlab(GitlabBackend) {
        address = 'https://gitlab.com/api/v4/projects/123456/terraform/state/my-project' // <.>
        username = grolifantOps.providerTools.resolveProperty('my.gitlab.username')  // <.>
        accessToken = grolifantOps.providerTools.resolveProperty('my.gitlab.access.token') // <.>

        retryMax = 2 // <.>
        retryWaitMin= 1 // <.>
        retryWaitMax = 30 // <.>
    }
  }
  sourceSets {
    main {
      usedBackend('gitlab')
    }
  }
// end::backend-src-block[]
}
----
// tag::backend-callouts[]
<.> Set the location of your Gitlab endpoint.
It takes the form `https://gitlab.com/api/v4/projects/<PROJECT_ID>/terraform/state/<STATE-NAME>`, where `PROJECT_ID` is the project state will be stored in, and `STATE-NAME` is a name for the state.
<.> Provide the Gitlab user name.
This user needs to have at least a Gitlab `Maintainer` role to perform updates.
This will be injected into the environment as `TF_USERNAME`
<.> Provide the Gitlab access token.
This will be injected into the environment as `TF_PASSWORD`
<.> The number of HTTP request retries.
Defaults to 2.
<.> The minimum time in seconds to wait between HTTP request attempts.
Defaults to 1.
<.> The maximum time in seconds to wait between HTTP request attempts.
Defaults to 30.
// end::backend-callouts[]

// tag::backend-tf[]
.main.tf
[source,hcl-terraform]
----
terraform {
  backend "http" {}
}
----
// end::backend-tf[]