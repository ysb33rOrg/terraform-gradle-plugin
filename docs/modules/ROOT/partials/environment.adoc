
Gradle execution environment is not passed down to the {tf-name}/{tofu-name} by default.
The exception is specific platform-dependent variables as defined by the following logic:

[source,groovy]
----
include::example$iac-base/TaskUtils.groovy[tags=default-environment,indent=0]
----

Additional, non-sensitive, environment variables are defined on the source set
[source,groovy]
----
sourceSets {
  main {
    environment( foo1: 'bar1', foo2: 'bar2') // <.>
    environment 'foo1', 'bar1' ) // <.>
    addEnvironmentProvider( provider { -> [ foo1: 'bar1', foo2: 'bar2' ]}) // <.>
  }
}
----
<.> A map of environment variables.
<.> A single environment variables passed as key and value.
<.> A provider of additional, non-sensitive, environment variables

// == AWS environmental variables
//
// It is possible to add all AWS-related environment variables to the Terraform runtime environment via short-cuts
//
// [source,groovy]
// ----
// terraform {
//     useAwsEnvironment() // <1>
// }
//
// tfInit {
//     useAwsEnvironment() // <2>
// }
// ----
// <1> Set at project level
// <2> Set at task-specific level