= {gitlab} Secrets

{gitlab} secrets can be managed with xref:project-artifacts:attachment${groovydoc-iac-base}/secrets/GitlabSecrets.html[`GitlabSecrets`]

.Configuring {gitlab} credentials
[source,groovy]
----
import org.ysb33r.gradle.iac.base.secrets.GitlabSecrets

terraform { // <.>
  secrets {
    gitlabMaintainer(GitlabSecrets) { // <.>
      useGitlabToken('1234567890') // <.>
      useGitlabTokenFromEnvironment() // <.>
    }
  }
  sourceSets {
    main {
        fromSecretsProvider(terraform.secrets.gitlabMaintainer) // <.>
    }
  }
}
----
<.> Replace `terraform` with `opentofu` for {tofu-name}.
<.> Declare a set of {gitlab} secrets.
<.> Declare a {gitlab} token.
  Use anything convertible to a string.
  Providers are recommended.
<.> Use the `GITLAB_TOKEN` environment variable of the `gitlab.token` system/Gradle property.
<.> Place the correct environment variables at the time the tool executes.
