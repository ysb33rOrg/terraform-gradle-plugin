= Command-line options for tasks
// TODO: Fix this
A number of task types support command-line options.

[%header,cols=5*]
|===
| Task
| Option
| Type
| Default
| Purpose

.2+<.^| `tfApply`
| `--targets`
| list
| {nbsp}
| Select group of resources to apply.

| `--replace`
| boolean
| `false`
| Select group of resources to replace.

<.^| `tfCleanupWorkspaces`
| `--force`
| boolean
| `false`
| Force removal of dangling workspaces even if state still exists.

.2+<.^| `tfDestroy`
| `--targets`
| list
| {nbsp}
| Select group of resources to apply.

| `--approve`
| boolean
| `false`
| Auto-approve destruction of all mentioned resources.

.2+<.^| `tfDestroyPlan`
| `--targets`
| list
| {nbsp}
| Select group of resources to apply.

| `--json`
| boolean
| `false`
| Write textual plan in JSON format.

.2+<.^| `tfImport`
| `--path`
| string
| {nbsp}
| Resource to import e.g. `aws_ecs_cluster.my_cluster`

| `--id`
| string
| {nbsp}
| Actual identifier to import. Check the Terraform documentation for the specific resource in order to know how to identify this.

.4+<.^| `tfInit`
| `--upgrade`
| boolean
| {nbsp}
| Force upgrade of modules

| `--no-configure-backends`
| boolean
| {nbsp}
| Do not configure backends

| `--force-copy`
| boolean
| {nbsp}
| Automatically answer yes to any backend migration questions

| `--reconfigure`
| boolean
| {nbsp}
| Disregard any existing configuration and prevent migration of any existing state

<.^| `tfOutput`
| `--json`
| boolean
| `false`
| Write validation output in JSON format.

.3+<.^| `tfPlan`
| `--targets`
| list
| {nbsp}
| Select group of resources to apply.

| `--json`
| boolean
| `false`
| Write textual plan in JSON format.

| `--replace`
| boolean
| `false`
| Select group of resources to replace.

<.^| `tfShowState`
| `--json`
| boolean
| `false`
| Write validation output in JSON format.

.2+<.^| `tfStateMv`
| `--from-path`
| string
| {nbsp}
| Source item to move i.e. `packet_device.worker` or `module.app`.

| `--to-path`
| string
| {nbsp}
| Destination item i.e. `packet_device.helper` or `module.parent.module.app`.

<.^| `tfStatePush`
| `--state-file`
| string
| {nbsp}
| Local state file path (relative to Terraform source directory) to push to remote state.

.1+<.^| `tfStateRm`
| `--path`
| string
| {nbsp}
| Resource to remove e.g. `aws_ecs_cluster.my_cluster`

.3+<.^| `tfTaint`
| `--path`
| string
| {nbsp}
| Resource to taint.

| `--allow-missing`
| boolean
| `false`
| Allow task to succeed even if the resource is missing.

| `--ignore-remote-version`
| boolean
| `false`
| Continue if remote and local Terraform versions differ from Terraform Cloud.

.3+<.^| `tfUntaint`
| `--path`
| string
| {nbsp}
| Resource to untaint.

| `--allow-missing`
| boolean
| `false`
| Allow task to succeed even if the resource is missing.

| `--ignore-remote-version`
| boolean
| `false`
| Continue if remote and local Terraform versions differ from Terraform Cloud.

<.^| `tfValidate`
| `--json`
| boolean
| `false`
| Write validation output in JSON format.

|===

In the case that the plugin does not support a specific command-line parameter, it is possible to temporarily inject it using the `--patch-arg` and `--patch-args` parameters.
Both of them can be repeated multiple times.
The first one takes a single parameters, whereas the second one is intended to take a command-line snippet of parameters.
If a parameter contains a space, you must use ``patch-arg` to pass it.

NOTE: Just in case this documentation is out-of-date, always run `./gradlew help --task <taskName>` to get a description of supported command-line options.


