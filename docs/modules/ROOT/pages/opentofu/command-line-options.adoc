= Command-line options for tasks
// TODO: Fix this
A number of task types support command-line options.

[%header,cols=5*]
|===
| Task
| Option
| Type
| Default
| Purpose

.2+<.^| `tofuApply`
| `--targets`
| list
| {nbsp}
| Select group of resources to apply.

| `--replace`
| boolean
| `false`
| Select group of resources to replace.

<.^| `tofuCleanupWorkspaces`
| `--force`
| boolean
| `false`
| Force removal of dangling workspaces even if state still exists.

.2+<.^| `tofuDestroy`
| `--targets`
| list
| {nbsp}
| Select group of resources to apply.

| `--approve`
| boolean
| `false`
| Auto-approve destruction of all mentioned resources.

.2+<.^| `tofuDestroyPlan`
| `--targets`
| list
| {nbsp}
| Select group of resources to apply.

| `--json`
| boolean
| `false`
| Write textual plan in JSON format.

.2+<.^| `tofuImport`
| `--path`
| string
| {nbsp}
| Resource to import e.g. `aws_ecs_cluster.my_cluster`

| `--id`
| string
| {nbsp}
| Actual identifier to import. Check the Terraform documentation for the specific resource in order to know how to identify this.

.4+<.^| `tofuInit`
| `--upgrade`
| boolean
| {nbsp}
| Force upgrade of modules

| `--no-configure-backends`
| boolean
| {nbsp}
| Do not configure backends

| `--force-copy`
| boolean
| {nbsp}
| Automatically answer yes to any backend migration questions

| `--reconfigure`
| boolean
| {nbsp}
| Disregard any existing configuration and prevent migration of any existing state

<.^| `tofuOutput`
| `--json`
| boolean
| `false`
| Write validation output in JSON format.

.3+<.^| `tofuPlan`
| `--targets`
| list
| {nbsp}
| Select group of resources to apply.

| `--json`
| boolean
| `false`
| Write textual plan in JSON format.

| `--replace`
| boolean
| `false`
| Select group of resources to replace.

<.^| `tofuShowState`
| `--json`
| boolean
| `false`
| Write validation output in JSON format.

.2+<.^| `tofuStateMv`
| `--from-path`
| string
| {nbsp}
| Source item to move i.e. `packet_device.worker` or `module.app`.

| `--to-path`
| string
| {nbsp}
| Destination item i.e. `packet_device.helper` or `module.parent.module.app`.

<.^| `tofuStatePush`
| `--state-file`
| string
| {nbsp}
| Local state file path (relative to Terraform source directory) to push to remote state.

.1+<.^| `tofuStateRm`
| `--path`
| string
| {nbsp}
| Resource to remove e.g. `aws_ecs_cluster.my_cluster`

.3+<.^| `tofuTaint`
| `--path`
| string
| {nbsp}
| Resource to taint.

| `--allow-missing`
| boolean
| `false`
| Allow task to succeed even if the resource is missing.

| `--ignore-remote-version`
| boolean
| `false`
| Continue if remote and local Terraform versions differ from Terraform Cloud.

.3+<.^| `tofuUntaint`
| `--path`
| string
| {nbsp}
| Resource to untaint.

| `--allow-missing`
| boolean
| `false`
| Allow task to succeed even if the resource is missing.

| `--ignore-remote-version`
| boolean
| `false`
| Continue if remote and local Terraform versions differ from Terraform Cloud.

<.^| `tofuValidate`
| `--json`
| boolean
| `false`
| Write validation output in JSON format.

|===

NOTE: Just in case this documentation is out-of-date, always run `./gradlew help --task <taskName>` to get a description of supported command-line options.
